# Bilibili
## 简介
Bilibili APP采用Android Compose声明式UI编写而成，主体采用MVVM设计框架，APP涉及到的主要技术包括：Flow、Coroutines、Retrofit、Okhttp、Hilt以及适配了深色模式等；主要数据来源于Bilibili API。具体功能如下所示:

- 适配深色模式
- Webview获取Cookie
- 使用Paging3分页加载网络内容
- Retrofit & Okhttp获取网络数据
- Hilt依赖注入
- 使用Room+Flow存储数据
- 模糊搜索 & 富文本
- 动画
- 导航
- and so on

## 视频

## 效果
### 推荐
![输入图片说明](img/recommend_light.png)
![输入图片说明](img/recommend_dark.png)
### 热门
![输入图片说明](img/hot_light.png)
![输入图片说明](img/hot_dark.png)
### 排行
![输入图片说明](img/rank_light.png)
![输入图片说明](img/rank_dark.png)
### 搜索
![输入图片说明](img/search_light.png)
![输入图片说明](img/search_dark.png)
### 模糊搜索
![输入图片说明](img/search_fuzzy_light.png)
![输入图片说明](img/search_fuzzy_dark.png)
### Dialog
![输入图片说明](img/clear_history_light.png)
![输入图片说明](img/clear_history_dark.png)
### 搜索结果
!![输入图片说明](img/search_result_light.png)
![输入图片说明](img/search_result_dark.png)
### 视频详情
![输入图片说明](img/play_light.png)
![输入图片说明](img/play_dark.png)
### 个人信息页
![输入图片说明](img/introduce_light.png)
![输入图片说明](img/introduce_dark.png)
### 登录页
![输入图片说明](img/login_light.png)
![输入图片说明](img/login_dark.png)