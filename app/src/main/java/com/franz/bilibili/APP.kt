package com.franz.bilibili

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.util.Log
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.franz.bilibili.navigation.NavigationAction
import com.franz.bilibili.navigation.NavigationGraph
import com.franz.bilibili.navigation.Screen
import com.franz.bilibili.param.CookieKey
import com.franz.bilibili.tools.SPUtil
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.insets.ui.BottomNavigation
import com.google.accompanist.insets.ui.Scaffold
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject


@HiltAndroidApp
class APP: Application(){
    companion object{
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context

        @JvmStatic
        fun getStartDestination(): String{
            val value = SPUtil.getValue(context = context, key = CookieKey,"") as String
            return if (value.isBlank())
                Screen.WebLoginPage.route
            else
                //Screen.LoginPage.route
                Screen.NavigationPage.route
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
    }
}

@Composable
fun BilibiliApp(){
    BilibiliTheme() {
        ProvideWindowInsets {
            val systemUiController = rememberSystemUiController()
            val navHostController = rememberNavController()
            SideEffect {
                systemUiController.setStatusBarColor(Color.Transparent,false)
            }

            Scaffold(
                bottomBar = {BottomNavbar(navHostController = navHostController)},
                modifier = Modifier.fillMaxSize()
            )
            {
                NavigationGraph(
                    navHostController = navHostController,
                    modifier = Modifier.padding(it),
                    startDestination = APP.getStartDestination()
                )
            }
        }
    }
}

@Composable
fun BottomNavbar(
    navHostController: NavHostController,
    elements: Array<NavElement> = NavElement.values()
){
    val navBackStackEntry by navHostController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry?.destination?.route
    val route = elements.map { it.route }
    if (currentRoute in route){
        BottomNavigation(
            Modifier.windowInsetsBottomHeight(WindowInsets.navigationBars.add(WindowInsets(bottom = 56.dp))),
            backgroundColor = BilibiliTheme.colors.bottomBar
        ) {
            elements.forEach {
                BottomNavigationItem(
                    selected = currentRoute == it.route,
                    icon = {
                        Icon(
                            painter = painterResource(id = it.icon),
                            contentDescription = it.route,
                            modifier = Modifier.size(20.dp)
                        )
                    },
                    label = { Text(text = stringResource(id = it.title), fontSize = 12.sp)},
                    alwaysShowLabel = true,
                    selectedContentColor = BilibiliTheme.colors.selectIcon,
                    unselectedContentColor = BilibiliTheme.colors.unselectIcon,
                    modifier = Modifier.navigationBarsPadding(),
                    onClick = {
                        navHostController.navigate(it.route){
                            // 当用户选择子项时在返回栈中弹出到导航图中的起始目的地
                            // 来避免太过臃肿的目的地堆栈
                            navHostController.graph.startDestinationRoute?.let { route->
                                popUpTo(route){saveState = true}
                            }
                            // 当重复选择相同项时避免相同目的地的多重拷贝
                            launchSingleTop = true
                            // 当重复选择之前已经选择的项时恢复状态
                            restoreState = true
                        }
                    }
                )
            }
        }
    }
}

enum class NavElement(
    @StringRes val title: Int,
    val route: String,
    @DrawableRes val icon: Int
){
    HomePage(R.string.HomePage,Screen.HomePage.route,R.drawable.icon_home),
    RankPage(R.string.RankPage,Screen.RankPage.route,R.drawable.icon_rank),
    SearchPage(R.string.SearchPage,Screen.SearchPage.route,R.drawable.icon_search),
    MinePage(R.string.MinePage,Screen.MinePage.route,R.drawable.icon_mine)
}