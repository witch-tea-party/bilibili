package com.franz.bilibili.bean

data class BaseResponse<T>(
    val code:Int,
    val message:String,
    val ttl:Int,
    val data:T
)
