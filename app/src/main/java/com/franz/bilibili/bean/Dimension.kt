package com.franz.bilibili.bean

data class Dimension(
    val height: Int,
    val rotate: Int,
    val width: Int
)