package com.franz.bilibili.bean

data class HotSearchBean(
    val hot_id: Int,
    val icon: String,
    val is_commercial: String,
    val keyword: String,
    val position: Int,
    val show_name: String,
    val word_type: Int
)