package com.franz.bilibili.bean

data class Owner(
    val face: String,
    val mid: Long,
    val name: String
)