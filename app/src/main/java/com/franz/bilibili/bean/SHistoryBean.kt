package com.franz.bilibili.bean

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SearchHistoryTable")
data class SHistoryBean(
    @PrimaryKey val key: String,
    val time: Long
)

class SHException(message: String): Exception(message)