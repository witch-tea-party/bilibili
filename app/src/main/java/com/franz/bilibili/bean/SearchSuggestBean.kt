package com.franz.bilibili.bean

data class SearchSuggestBean(
    val name: String,
    val ref: Int,
    val spid: Int,
    val term: String,
    val value: String
)