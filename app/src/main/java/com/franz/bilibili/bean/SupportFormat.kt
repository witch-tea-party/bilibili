package com.franz.bilibili.bean

data class SupportFormat(
    val codecs: Any,
    val display_desc: String,
    val format: String,
    val new_description: String,
    val quality: Int,
    val superscript: String
)