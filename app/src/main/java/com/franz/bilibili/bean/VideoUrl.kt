package com.franz.bilibili.bean

data class VideoUrl(
    val accept_description: List<String>,
    val accept_format: String,
    val accept_quality: List<Int>,
    val durl: List<Durl>,
    val format: String,
    val from: String,
    val high_format: Any,
    val last_play_cid: Int,
    val last_play_time: Int,
    val message: String,
    val quality: Int,
    val result: String,
    val seek_param: String,
    val seek_type: String,
    val support_formats: List<SupportFormat>,
    val timelength: Int,
    val video_codecid: Int
)