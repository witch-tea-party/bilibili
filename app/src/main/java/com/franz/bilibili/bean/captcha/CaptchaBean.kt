package com.franz.bilibili.bean.captcha

data class CaptchaBean(
    val code: Int,
    val `data`: Data,
    val message: String,
    val ttl: Int
)