package com.franz.bilibili.bean.captcha

data class Data(
    val geetest: Geetest,
    val tencent: Tencent,
    val token: String,
    val type: String
)