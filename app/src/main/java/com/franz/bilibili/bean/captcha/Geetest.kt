package com.franz.bilibili.bean.captcha

data class Geetest(
    val challenge: String,
    val gt: String
)