package com.franz.bilibili.bean.detail

import com.franz.bilibili.bean.Dimension
import com.franz.bilibili.bean.Rights
import com.franz.bilibili.bean.Stat

data class Arc(
    val aid: Int,
    val author: Author,
    val copyright: Int,
    val ctime: Int,
    val desc: String,
    val desc_v2: Any,
    val dimension: Dimension,
    val duration: Int,
    val `dynamic`: String,
    val is_blooper: Boolean,
    val is_chargeable_season: Boolean,
    val pic: String,
    val pubdate: Int,
    val rights: Rights,
    val stat: Stat,
    val state: Int,
    val title: String,
    val type_id: Int,
    val type_name: String,
    val videos: Int
)