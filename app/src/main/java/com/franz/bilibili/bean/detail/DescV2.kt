package com.franz.bilibili.bean.detail

data class DescV2(
    val biz_id: Int,
    val raw_text: String,
    val type: Int
)