package com.franz.bilibili.bean.detail

data class HonorDetail(
    val aid: Int,
    val desc: String,
    val type: Int,
    val weekly_recommend_num: Int
)