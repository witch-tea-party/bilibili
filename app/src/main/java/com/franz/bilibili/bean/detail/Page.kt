package com.franz.bilibili.bean.detail

import com.franz.bilibili.bean.Dimension

data class Page(
    val cid: Int,
    val dimension: Dimension,
    val duration: Int,
    val from: String,
    val page: Int,
    val part: String,
    val vid: String,
    val weblink: String
)