package com.franz.bilibili.bean.detail

data class Subtitle(
    val allow_submit: Boolean,
    val list: List<SubtitleBean>
)