package com.franz.bilibili.bean.detail

data class SubtitleBean(
    val ai_status: Int,
    val ai_type: Int,
    val author: Author,
    val id: Long,
    val id_str: String,
    val is_lock: Boolean,
    val lan: String,
    val lan_doc: String,
    val subtitle_url: String,
    val type: Int
)