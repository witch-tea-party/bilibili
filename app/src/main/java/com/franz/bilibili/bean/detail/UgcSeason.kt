package com.franz.bilibili.bean.detail

import com.franz.bilibili.bean.Stat

data class UgcSeason(
    val attribute: Int,
    val cover: String,
    val ep_count: Int,
    val id: Int,
    val intro: String,
    val is_pay_season: Boolean,
    val mid: Int,
    val season_type: Int,
    val sections: List<Section>,
    val sign_state: Int,
    val stat: Stat,
    val title: String
)