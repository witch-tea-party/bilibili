package com.franz.bilibili.bean.detail

import com.franz.bilibili.bean.Dimension
import com.franz.bilibili.bean.Owner
import com.franz.bilibili.bean.Rights
import com.franz.bilibili.bean.Stat

data class VideoDetail(
    val aid: Int,
    val bvid: String,
    val cid: Int,
    val copyright: Int,
    val ctime: Int,
    val desc: String,
    val desc_v2: List<DescV2>,
    val dimension: Dimension,
    val duration: Int,
    val `dynamic`: String,
    val honor_reply: HonorReply,
    val is_chargeable_season: Boolean,
    val is_season_display: Boolean,
    val is_story: Boolean,
    val like_icon: String,
    val mission_id: Int,
    val need_jump_bv: Boolean,
    val no_cache: Boolean,
    val owner: Owner,
    val pages: List<Page>,
    val pic: String,
    val premiere: Any,
    val pubdate: Int,
    val rights: Rights,
    val stat: Stat,
    val state: Int,
    val stein_guide_cid: Int,
    val subtitle: Subtitle,
    val teenage_mode: Int,
    val tid: Int,
    val title: String,
    val tname: String,
    val user_garb: UserGarb,
    val videos: Int,
    val ugc_season:UgcSeason
)