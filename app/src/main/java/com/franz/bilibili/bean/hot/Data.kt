package com.franz.bilibili.bean.hot

data class Data(
    val list: List<HotBean>,
    val no_more: Boolean
)