package com.franz.bilibili.bean.hot

data class HotModel(
    val code: Int,
    val `data`: Data,
    val message: String,
    val ttl: Int
)