package com.franz.bilibili.bean.hot

data class RcmdReason(
    val content: String,
    val corner_mark: Int
)