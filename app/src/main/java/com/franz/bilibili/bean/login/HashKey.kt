package com.franz.bilibili.bean.login

data class HashKey(
    val hash: String,
    val key: String
)