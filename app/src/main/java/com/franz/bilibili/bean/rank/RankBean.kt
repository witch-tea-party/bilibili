package com.franz.bilibili.bean.rank

import com.franz.bilibili.bean.Dimension
import com.franz.bilibili.bean.Owner
import com.franz.bilibili.bean.Rights
import com.franz.bilibili.bean.Stat

data class RankBean(
    val aid: Int,
    val bvid: String,
    val cid: Int,
    val copyright: Int,
    val ctime: Int,
    val desc: String,
    val dimension: Dimension,
    val duration: Int,
    val `dynamic`: String,
    val first_frame: String,
    val mission_id: Int,
    val owner: Owner,
    val pic: String,
    val pub_location: String,
    val pubdate: Int,
    val rights: Rights,
    val score: Int,
    val season_id: Int,
    val short_link: String,
    val short_link_v2: String,
    val stat: Stat,
    val state: Int,
    val tid: Int,
    val title: String,
    val tname: String,
    val videos: Int
)