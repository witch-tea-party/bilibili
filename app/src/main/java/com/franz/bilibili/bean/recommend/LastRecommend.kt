package com.franz.bilibili.bean.recommend

data class LastRecommend(
    val face: String,
    val mid: Int,
    val msg: String,
    val time: Int,
    val uname: String
)