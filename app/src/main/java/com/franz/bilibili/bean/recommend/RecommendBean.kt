package com.franz.bilibili.bean.recommend

import com.franz.bilibili.bean.Rights

data class RecommendBean(
    val aid: Int,
    val author: String,
    val bvid: String,
    val coins: Int,
    val create: String,
    val credit: Int,
    val description: String,
    val duration: String,
    val favorites: Int,
    val last_recommend: List<LastRecommend>,
    val like: Int,
    val mid: Int,
    val pic: String,
    val play: Int,
    val review: Int,
    val rights: Rights,
    val subtitle: String,
    val title: String,
    val typeid: Int,
    val typename: String,
    val video_review: Int
)