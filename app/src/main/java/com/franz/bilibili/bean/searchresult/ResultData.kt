package com.franz.bilibili.bean.searchresult

data class ResultData(
    val `data`: List<Any>,
    val result_type: String
)