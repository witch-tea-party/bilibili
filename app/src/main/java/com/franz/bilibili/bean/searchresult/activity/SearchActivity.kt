package com.franz.bilibili.bean.searchresult.activity

data class SearchActivity(
    val author: String,
    val card_type: Int,
    val card_value: String,
    val corner: String,
    val cover: String,
    val desc: String,
    val id: Int,
    val pos: Int,
    val position: Int,
    val state: Int,
    val status: Int,
    val title: String,
    val type: String,
    val url: String
)