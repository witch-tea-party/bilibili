package com.franz.bilibili.bean.searchresult.bilibili_user

data class BilibiliUser(
    val expand: Expand,
    val face_nft: Int,
    val face_nft_type: Int,
    val fans: Int,
    val gender: Int,
    val hit_columns: List<Any>,
    val is_live: Int,
    val is_senior_member: Int,
    val is_upuser: Int,
    val level: Int,
    val mid: Int,
    val official_verify: OfficialVerify,
    val res: List<Re>,
    val room_id: Int,
    val type: String,
    val uname: String,
    val upic: String,
    val usign: String,
    val verify_info: String,
    val videos: Int
)