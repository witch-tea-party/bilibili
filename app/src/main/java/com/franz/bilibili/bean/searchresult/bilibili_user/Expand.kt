package com.franz.bilibili.bean.searchresult.bilibili_user

data class Expand(
    val is_power_up: Boolean,
    val system_notice: Any
)