package com.franz.bilibili.bean.searchresult.bilibili_user

data class OfficialVerify(
    val desc: String,
    val type: Int
)