package com.franz.bilibili.bean.searchresult.bilibili_user

data class Re(
    val aid: Int,
    val arcurl: String,
    val bvid: String,
    val coin: Int,
    val desc: String,
    val dm: Int,
    val duration: String,
    val fav: Int,
    val is_pay: Int,
    val is_union_video: Int,
    val pic: String,
    val play: String,
    val pubdate: Int,
    val title: String
)