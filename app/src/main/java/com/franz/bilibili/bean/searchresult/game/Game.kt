package com.franz.bilibili.bean.searchresult.game

data class Game(
    val book_num: Int,
    val comment_num: Int,
    val download_num: Int,
    val game_base_id: Int,
    val game_icon: String,
    val game_link: String,
    val game_name: String,
    val game_status: Int,
    val game_tags: String,
    val grade: Double,
    val notice: String,
    val notice_title: String,
    val official_account: Int,
    val platform: String,
    val rank_info: RankInfo,
    val recommend_reason: String,
    val summary: String
)