package com.franz.bilibili.bean.searchresult.game

data class RankInfo(
    val game_rank: Int,
    val rank_content: String,
    val rank_link: String,
    val rank_type: Int,
    val search_bkg_day_color: String,
    val search_bkg_night_color: String,
    val search_day_icon_url: String,
    val search_font_day_color: String,
    val search_font_night_color: String,
    val search_night_icon_url: String
)