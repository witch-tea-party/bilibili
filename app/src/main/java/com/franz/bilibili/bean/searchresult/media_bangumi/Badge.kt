package com.franz.bilibili.bean.searchresult.media_bangumi

data class Badge(
    val bg_color: String,
    val bg_color_night: String,
    val bg_style: Int,
    val border_color: String,
    val border_color_night: String,
    val text: String,
    val text_color: String,
    val text_color_night: String
)