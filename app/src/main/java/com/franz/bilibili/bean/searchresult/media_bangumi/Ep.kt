package com.franz.bilibili.bean.searchresult.media_bangumi

data class Ep(
    val badges: List<Badge>,
    val cover: String,
    val id: Int,
    val index_title: String,
    val long_title: String,
    val release_date: String,
    val title: String,
    val url: String
)