package com.franz.bilibili.bean.searchresult.media_bangumi

data class MediaScore(
    val score: Double,
    val user_count: Int
)