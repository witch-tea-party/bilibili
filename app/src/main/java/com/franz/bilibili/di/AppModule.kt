package com.franz.bilibili.di

import android.app.Application
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.media3.common.Player
import androidx.media3.exoplayer.ExoPlayer
import androidx.room.Room
import androidx.room.RoomDatabase
import com.franz.bilibili.config.BilibiliConfig
import com.franz.bilibili.network.CollectService
import com.franz.bilibili.network.OfficialService
import com.franz.bilibili.param.CookieKey
import com.franz.bilibili.room.BilibiliDataBase
import com.franz.bilibili.room.SHistoryRepository
import com.franz.bilibili.room.SHistoryRepositoryImpl
import com.franz.bilibili.tools.SPUtil
import com.franz.bilibili.usecase.shUseCase.ClearSHistory
import com.franz.bilibili.usecase.shUseCase.GetSHistories
import com.franz.bilibili.usecase.shUseCase.InsertSHistory
import com.franz.bilibili.usecase.shUseCase.SHUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineExceptionHandler
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    /**
     * 总共有两个base接口，一个为官方提供，另一个为第三方收集的*/
    const val OFFICIAL_URL:String = "https://api.bilibili.com/"
    const val COLLECT_URL:String = "https://passport.bilibili.com/"

    const val OFFICIAL = "Official"
    const val COLLECT = "Collect"

    val USER_AGENT = """
            |${BilibiliConfig.getSystemUserAgent()} 
            |os/android model/${Build.MODEL} mobi_app/android 
            |build/${BilibiliConfig.build} channel/bili innerVer/${BilibiliConfig.build} 
            |osVer/${Build.VERSION.RELEASE} network/2
        """.trimMargin().replace("\n", "")

    /**
     *提供OkHttpClient对象,在其中添加拦截器，给header添加cookie，为未来需要cookie的接口做准备 */
    @Singleton
    @Provides
    fun providerClient(@ApplicationContext context: Context):OkHttpClient{
        val interceptorParams = Interceptor { chain ->
            val cookie = SPUtil.getValue(context, CookieKey,"") as String
            /**
             *
             (1)Mozilla/5.0 ：表示兼容Mozilla, 几乎所有的浏览器都有这个字符;
             (2) (Linux; Android 8.1.0; PACM00 Build/O11019; wv): 表示设备的操作系统版本，以及CPU信息；
            (3）AppleWebKit/537.36 (KHTML, like Gecko)：表示浏览器的内核；
            (4) Version/4.0 Chrome/62.0.3202.84 Mobile Safari/537.36: 表示浏览器的版本号。
            */
            val newBuilder = chain.request().newBuilder()
                .addHeader("Referer","https://www.bilibili.com/")
                .addHeader("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
                .addHeader("Origin","https://www.bilibili.com")
                .addHeader("Accept","*/*")
                .addHeader("Accept-Language","zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7")
                .addHeader("Cookie",cookie)
                .build()
            chain.proceed(newBuilder)
        }
        return OkHttpClient.Builder()
            .addInterceptor(interceptorParams)
            .connectTimeout(3000L,TimeUnit.MILLISECONDS)
            .writeTimeout(3000L,TimeUnit.MILLISECONDS)
            .retryOnConnectionFailure(true)
            .build()
    }

    /**
     * 在目录中使用Hilt提供多个相对类型的实例对象，通过"@Named"注解进行区分
     * 也可以通过"@Qualifier"限定符定义新的注解进行区分*/

    @Named(OFFICIAL)
    @Singleton
    @Provides
    fun providerOfficialRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(OFFICIAL_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Named(COLLECT)
    @Singleton
    @Provides
    fun providerCollectRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(COLLECT_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }


    @Singleton
    @Provides
    fun providerOfficialService(@Named(OFFICIAL) retrofit: Retrofit): OfficialService = retrofit.create(OfficialService::class.java)


    @Singleton
    @Provides
    fun providerCollectService(@Named(COLLECT) retrofit: Retrofit): CollectService = retrofit.create(CollectService::class.java)

    @Singleton
    @Provides
    fun providerDataBase(application: Application):BilibiliDataBase{
        return Room.databaseBuilder(
            application,
            BilibiliDataBase::class.java,
            BilibiliDataBase.DATABASE_NAME
        ).build()
    }

    @Singleton
    @Provides
    fun providerSHRepository(dataBase: BilibiliDataBase):SHistoryRepository = SHistoryRepositoryImpl(dataBase.sHistoryDao)

    @Singleton
    @Provides
    fun providerSHUseCase(repository: SHistoryRepository):SHUseCase{
        return SHUseCase(
            GetSHistories(repository),
            InsertSHistory(repository),
            ClearSHistory(repository)
        )
    }
}