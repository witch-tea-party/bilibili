package com.franz.bilibili.login

sealed class LoginEvent{
    data class ChangeEmail(val email: String): LoginEvent()
    data class ChangePassword(val password: String): LoginEvent()
    data class ChangeEyeState(val isShow: Boolean): LoginEvent()
    object Login: LoginEvent()
}
