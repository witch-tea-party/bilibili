package com.franz.bilibili.login

import android.annotation.SuppressLint
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.clipPath
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.*
import androidx.hilt.navigation.compose.hiltViewModel
import com.franz.bilibili.R
import com.franz.bilibili.ui.theme.*
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.insets.statusBarsPadding
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun LoginPage(
    modifier: Modifier = Modifier,
    viewModel: LoginViewModel = hiltViewModel(),
    onNavigation:()-> Unit
){
    val emailState = viewModel.emailState.value
    val pwdState = viewModel.pwdState.value
    val scaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()

    LaunchedEffect(viewModel.eventFlow){
        viewModel.eventFlow.collectLatest {
            when(it){
                is LoginUIEvent.LoginFailed->{
                    scaffoldState.snackbarHostState.showSnackbar(it.message)
                }
                is  LoginUIEvent.LoginSuccess->{
                    //scaffoldState.snackbarHostState.showSnackbar(it.message)
                    onNavigation()
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        modifier = modifier.navigationBarsPadding()
    ) {
        Box(
            modifier = modifier.fillMaxSize()
        ){
            Image(
                painter = painterResource(id = R.drawable.bg),
                contentDescription = "background",
                contentScale = ContentScale.Crop,
            )
            IrregularBg(Modifier.padding(20.dp))
            PwdContentView(
                emailState = emailState,
                loginState = pwdState,
                modifier = Modifier
                    .padding(20.dp)
                    .align(Alignment.Center),
                onChangeEmail = { viewModel.onEvent(LoginEvent.ChangeEmail(it)) },
                onChangePwd = { viewModel.onEvent(LoginEvent.ChangePassword(it)) },
                onChangeEyeState = { viewModel.onEvent(LoginEvent.ChangeEyeState(it)) },
                onLogin = {
                    scope.launch {
                        viewModel.onEvent(LoginEvent.Login)
                    }
                }
            )
        }
    }
}

@Composable
fun PwdContentView(
    emailState: LoginState,
    loginState: LoginState,
    startColor: Color = BilibiliTheme.colors.loginStart,
    endColor: Color = BilibiliTheme.colors.loginEnd,
    modifier: Modifier = Modifier,
    onChangeEmail: (String)-> Unit,
    onChangePwd: (String)-> Unit,
    onChangeEyeState: (Boolean)-> Unit,
    onLogin: ()-> Unit
){
   Column(
       horizontalAlignment = Alignment.CenterHorizontally,
       verticalArrangement = Arrangement.Top,
       modifier = modifier
           .fillMaxSize()
           .padding(10.dp)
           .statusBarsPadding()
   ) {
       /**logo*/
      Image(
          painter = painterResource(id = R.drawable.bilibili_logo),
          contentDescription = "bilibili",
          modifier = Modifier.padding(top = 40.dp)
      )

       Spacer(modifier = Modifier.height(50.dp))

       /**账号输入框*/
       TextField(
           value = emailState.text,
           onValueChange = onChangeEmail,
           leadingIcon = { Icon(
               painter = painterResource(id = R.drawable.icon_email),
               contentDescription = emailState.label,
               tint = BilibiliTheme.colors.defaultIcon,
               modifier = Modifier.size(24.dp)
           ) },
           singleLine = true,
           colors = TextFieldDefaults.textFieldColors(
               backgroundColor = Color.Transparent,
               disabledIndicatorColor = Color.Transparent,
               unfocusedIndicatorColor = BilibiliTheme.colors.defaultIcon,//没有焦点时，输入框下方的分割线颜色
               focusedIndicatorColor = BilibiliTheme.colors.defaultIcon,//拥有焦点时，输入框下方的分割线颜色
               errorIndicatorColor = Color.Transparent,
               cursorColor = BilibiliTheme.colors.defaultIcon,//光标颜色

           ),
           textStyle = TextStyle(color = BilibiliTheme.colors.textContent, fontSize = 12.sp),//输入框内的字体样式
           label = { Text(text = emailState.label, color = BilibiliTheme.colors.textTitle, style = MaterialTheme.typography.body1)},//上方提示字体样式
           placeholder = {Text(text = emailState.hint, color = BilibiliTheme.colors.textContent, style = MaterialTheme.typography.caption)},//hint的字体样式
           modifier = Modifier.fillMaxWidth()
       )
       
       Spacer(modifier = Modifier.height(30.dp))

       /**密码输入框*/
       TextField(
           value = loginState.text,
           onValueChange = onChangePwd,
           leadingIcon = { Icon(
               painter = painterResource(id = R.drawable.icon_key),
               contentDescription = loginState.label,
               tint =  BilibiliTheme.colors.defaultIcon,
               modifier = Modifier.size(24.dp)
           ) },
           trailingIcon = { Icon(
               painter =  painterResource(id = if (loginState.isShowPwd) R.drawable.icon_eye_show else R.drawable.icon_eye_hide),
               contentDescription = loginState.label,
               tint =  BilibiliTheme.colors.defaultIcon,
               modifier = Modifier
                   .size(24.dp)
                   .clickable { onChangeEyeState(loginState.isShowPwd) }
           ) },
           singleLine = true,
           colors = TextFieldDefaults.textFieldColors(
               backgroundColor = Color.Transparent,
               disabledIndicatorColor = Color.Transparent,
               unfocusedIndicatorColor =  BilibiliTheme.colors.defaultIcon,
               focusedIndicatorColor = BilibiliTheme.colors.defaultIcon,
               errorIndicatorColor = Color.Transparent,
               cursorColor =  BilibiliTheme.colors.defaultIcon,//光标颜色

           ),
           textStyle = TextStyle(color = BilibiliTheme.colors.textContent, fontSize = 12.sp),
           label = { Text(text = loginState.label, color = BilibiliTheme.colors.textTitle, style = MaterialTheme.typography.body1)},
           placeholder = {Text(text = loginState.hint, color = BilibiliTheme.colors.textContent, style = MaterialTheme.typography.caption)},
           keyboardOptions = KeyboardOptions(keyboardType = if (loginState.isShowPwd) KeyboardType.Text else KeyboardType.Password),
           visualTransformation = if (loginState.isShowPwd) VisualTransformation.None else PasswordVisualTransformation(),
           modifier = Modifier.fillMaxWidth()
       )

       Spacer(modifier = Modifier.height(20.dp))

       Text(
           text = "forget password?",
           color = BilibiliTheme.colors.textContent,
           style = MaterialTheme.typography.caption,
           textAlign = TextAlign.End,
           modifier = Modifier.fillMaxWidth()
       )

       Spacer(modifier = Modifier.height(50.dp))

       /**登录按钮*/
       TextButton(
           modifier = Modifier
               .background(
                   brush = Brush.horizontalGradient(listOf(startColor, endColor)),
                   shape = RoundedCornerShape(10.dp)
               )
               .size(250.dp, 40.dp),
           onClick = onLogin
       ) {
           Text(
               text = "Login",
               color = Color.White,
               textAlign = TextAlign.Center,
               modifier = Modifier.fillMaxSize()
           )
       }
   }
}
/**
 * 账号密码登录界面不规则背景*/
@Composable
fun IrregularBg(
    modifier: Modifier = Modifier,
    radius: Dp = 20.dp,
    leftBottom: Dp = 250.dp,
    rightBottom: Dp = 100.dp,
    leftTop: Dp = 150.dp,
    one: ImageBitmap = ImageBitmap.Companion.imageResource(id = R.drawable.icon_google),
    two: ImageBitmap = ImageBitmap.Companion.imageResource(id = R.drawable.icon_facebook),
    three: ImageBitmap = ImageBitmap.Companion.imageResource(id = R.drawable.icon_twitter),
    up: ImageBitmap = ImageBitmap.Companion.imageResource(id = R.drawable.icon_arrow_up),
    down: ImageBitmap = ImageBitmap.Companion.imageResource(id = R.drawable.icon_arrow_down),
    topColor: Color = BilibiliTheme.colors.backgroundTop,
    centerColor: Color = BilibiliTheme.colors.backgroundCenter,
    bottomColor: Color = BilibiliTheme.colors.backgroundBottom,
    endColor: Color = BilibiliTheme.colors.backgroundEnd
){
    Box(modifier = modifier
        .fillMaxSize()
        .shadow(10.dp, spotColor = Color.Gray)
        .statusBarsPadding()
        .navigationBarsPadding()
    ){
        Canvas(modifier = Modifier.matchParentSize()){
            /**
             * 通过描点进行绘制，每一个顶点，左上角顶点(0,0)可以省略
             * 上层不规则矩形*/
            val clipPathTop = Path().apply {
                lineTo(size.width, 0f)
                lineTo(size.width, size.height-rightBottom.toPx())
                lineTo(0f, size.height- leftBottom.toPx())
                close()
            }

            /**
             * 下层不规则矩形*/
            val clipPathBottom = Path().apply {
                lineTo(0f,size.height-leftTop.toPx())
                lineTo(size.width, size.height)
                lineTo(0f,size.height)
                close()
            }

            clipPath(clipPathTop) {
                drawRoundRect(
                    brush = Brush.verticalGradient(listOf(topColor,centerColor, bottomColor)),
                    size = size,
                    cornerRadius = CornerRadius(radius.toPx())
                )
            }

            clipPath(clipPathBottom){
                drawRoundRect(
                    brush = Brush.horizontalGradient(listOf(topColor,endColor, bottomColor)),
                    size = size,
                    cornerRadius = CornerRadius(radius.toPx())
                )
            }
            drawImage(
                image = one,
                dstOffset = IntOffset(50,(size.height-(leftBottom - 40.dp).toPx()).toInt()),
                dstSize = IntSize(192,192)
            )

            drawImage(
                image = two,
                dstOffset = IntOffset((size.width / 2 - 100).toInt(),(size.height-(leftTop / 2 + 74.dp).toPx()).toInt()),
                dstSize = IntSize(192,192)
            )
            drawImage(
                image = three,
                dstOffset = IntOffset(size.width.toInt() - 256,(size.height- rightBottom.toPx()).toInt()),
                dstSize = IntSize(224,224)//找不到相同尺寸，故调整大小，达到视觉一致
            )

            /**
             * 位于下层不规则背景左下方*/
            drawImage(
                image = up,
                dstOffset = IntOffset(20,(size.height - 120).toInt()),
                dstSize = IntSize(100,100)
            )

            /**
             * 位于上层不规则背景右下方*/
            drawImage(
                image = down,
                dstOffset = IntOffset(size.width.toInt() - 120,(size.height-(rightBottom + 50.dp).toPx()).toInt()),
                dstSize = IntSize(100,100)
            )
        }
    }
}