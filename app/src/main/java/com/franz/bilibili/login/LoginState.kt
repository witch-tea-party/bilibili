package com.franz.bilibili.login

data class LoginState(
    var label: String,
    var text: String,
    var hint: String,
    var isShowPwd: Boolean//只用于密码输入框
)
