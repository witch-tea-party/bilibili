package com.franz.bilibili.login

sealed class LoginUIEvent(val message:String){
    object LoginFailed:LoginUIEvent("账号或密码不能为空!")
    object LoginSuccess:LoginUIEvent("登录成功!")
}
