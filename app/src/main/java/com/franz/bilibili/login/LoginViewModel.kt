package com.franz.bilibili.login

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.franz.bilibili.APP
import com.franz.bilibili.bean.login.HashKey
import com.franz.bilibili.nav.search.SearchUIEvent
import com.franz.bilibili.network.CollectService
import com.franz.bilibili.param.CookieKey
import com.franz.bilibili.tools.SPUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import java.util.*
import javax.crypto.Cipher
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val service: CollectService): ViewModel() {
    private val _emailState = mutableStateOf(
        LoginState(
        text = "",
        label = "Email",
        hint = "Enter your email",
        isShowPwd = false)
    )

    val emailState:State<LoginState> = _emailState

    private val _pwdState = mutableStateOf(
        LoginState(
        text = "",
        label = "Password",
        hint = "Enter your password",
        isShowPwd = false)
    )

    val pwdState:State<LoginState> = _pwdState

    private val _eventFlow = MutableSharedFlow<LoginUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {

    }
    fun onEvent(event: LoginEvent){
        when(event){
            is LoginEvent.ChangeEmail -> {
                _emailState.value = emailState.value.copy(
                    text = event.email
                )
            }
            is LoginEvent.ChangePassword -> {
                _pwdState.value = pwdState.value.copy(
                    text = event.password
                )
            }
            is LoginEvent.Login -> {
                //login()
                viewModelScope.launch {
                    if (_emailState.value.text.isEmpty() || _pwdState.value.text.isEmpty()){
                        _eventFlow.emit(LoginUIEvent.LoginFailed)
                    }else{
                        _eventFlow.emit(LoginUIEvent.LoginSuccess)
                    }
                }
            }
            is LoginEvent.ChangeEyeState -> {
                _pwdState.value = pwdState.value.copy(
                    isShowPwd = !event.isShow
                )
            }
        }
    }

    private suspend fun getHashKey():HashKey?{
        val key = service.getHashKey()
        if (key.code == 0){
            return key.data
        }
        return null
    }
    
    private fun login(){
        viewModelScope.launch {
            //第一步，获取Hash和key
            val key = withContext(Dispatchers.IO){
                getHashKey()
            }
            key?.let {
                val (newHash,newKey) = key.let { data->
                    data.hash to data.key.split('\n').filterNot {
                        it.startsWith('-')
                    }.joinToString (separator = "")
                }
                //解析 RSA 公钥
                val publicKey = X509EncodedKeySpec(Base64.getDecoder().decode(newKey)).let {
                    KeyFactory.getInstance("RSA").generatePublic(it)
                }
                //加密密码
                //兼容 Android
                val cipheredPassword = Cipher.getInstance("RSA/ECB/PKCS1Padding").apply {
                    init(Cipher.ENCRYPT_MODE, publicKey)
                }.doFinal((newHash + _pwdState.value.text.toString()).toByteArray()).let {
                    Base64.getEncoder().encode(it)
                }.let {
                    String(it)
                }
                val login = service.login(username = _emailState.value.text.toString(), password = cipheredPassword)
                if (login.code == 0){
                }
            }
        }
    }
}
