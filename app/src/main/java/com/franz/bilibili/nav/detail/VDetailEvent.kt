package com.franz.bilibili.nav.detail

sealed class VDetailEvent{
    object Expand:VDetailEvent()
    object Agreement:VDetailEvent()
    object Dislike:VDetailEvent()
    object Favorite:VDetailEvent()
    object Epi:VDetailEvent()
}
