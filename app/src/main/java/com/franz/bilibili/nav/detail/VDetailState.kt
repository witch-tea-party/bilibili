package com.franz.bilibili.nav.detail

import com.franz.bilibili.bean.detail.UgcSeason
import com.franz.bilibili.bean.detail.VideoDetail
import com.franz.bilibili.bean.relate.RelateVideosBean

data class VDetailState(
    val aid:Int = 0,
    val cid:Int = 0,
    val videos:List<RelateVideosBean> = emptyList(),
    val url: String = "",
)

data class VInfoState(
    val grade:String = "无",
    var info: VideoDetail? = null
)

data class VShowState(
    val showRank:Boolean = false,
    val showContent:Boolean = false,
    val showLike:Boolean = false,
    val showDislike:Boolean = false,
    val showFavorite:Boolean = false
)

data class VCollectionState(
    val showSeason:Boolean = false,
    val showEpi:Boolean = false,
    val currentEpi:Int = 0,
    val totalEpi:Int = 0,
    val bean:UgcSeason? = null
)
