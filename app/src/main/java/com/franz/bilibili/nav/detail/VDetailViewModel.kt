package com.franz.bilibili.nav.detail

import android.net.Uri
import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import com.franz.bilibili.bean.relate.RelateVideosBean
import com.franz.bilibili.network.OfficialService
import com.franz.bilibili.tools.GsonFormat
import com.franz.bilibili.tools.responseBody
import com.google.gson.JsonObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class VDetailViewModel @Inject constructor(
    private val service: OfficialService,
    savedStateHandle: SavedStateHandle,
    val player: Player
):ViewModel() {
    private val _videoState = mutableStateOf(VDetailState())
    val videoState:State<VDetailState> = _videoState

    private val _infoState = mutableStateOf(VInfoState())
    val infoSate:State<VInfoState> = _infoState

    private val _showState = mutableStateOf(VShowState())
    val showState:State<VShowState> = _showState

    private val _collectionState = mutableStateOf(VCollectionState())
    val collectionState:State<VCollectionState> = _collectionState


    init {
        viewModelScope.launch(Dispatchers.Main) {
            try {
                savedStateHandle.get<Int>("aid")?.let {
                    if (it != 0){
                        _videoState.value = videoState.value.copy(
                            aid = it
                        )
                    }
                }
                savedStateHandle.get<Int>("cid")?.let {
                    if (it != 0){
                        _videoState.value = videoState.value.copy(
                            cid = it
                        )
                    }
                }
                /** 如果该视频没有携带cid，则通过获取视频详细信息获取cid
                * 然后通过aid and cid获取视频url*/
                withContext(Dispatchers.Main){
                    val cid = withContext(Dispatchers.IO){
                        getVideoDetail(_videoState.value.aid)
                    }
                    val url = withContext(Dispatchers.IO){
                        getVideoUrl(avid = _videoState.value.aid, cid = cid)
                    }
                    Log.d("videoUrl", "url=${url}")
                    if (url.isNotEmpty()){
                        play(url)
                    }
                    getRelatedVideos(_videoState.value.aid)
                }
            }catch (e:Exception){
                Log.d("VDetailViewModel",e.message.toString())
            }
        }
    }


    fun onEvent(event: VDetailEvent){
        when(event){
            is VDetailEvent.Expand->{
                _showState.value = showState.value.copy(
                    showContent = !_showState.value.showContent
                )
            }
            is VDetailEvent.Agreement->{
                _showState.value = showState.value.copy(
                    showLike = !_showState.value.showLike
                )
            }
            is VDetailEvent.Dislike->{
                _showState.value = showState.value.copy(
                    showDislike = !_showState.value.showDislike
                )
            }
            is VDetailEvent.Favorite->{
                _showState.value = showState.value.copy(
                    showFavorite = !_showState.value.showFavorite
                )
            }
            is VDetailEvent.Epi->{
                _collectionState.value = collectionState.value.copy(
                    showEpi = !_collectionState.value.showEpi
                )
            }
        }
    }

    private suspend fun play(url: String){
        val uri = Uri.parse(url)
        player.setMediaItem(MediaItem.fromUri(uri))
    }

    override fun onCleared() {
        super.onCleared()
        player.release()
    }

    /**
     * 获取视频详细信息
     * 重要的是获取其中的cid*/
    private suspend fun getVideoDetail(aid:Int):Int{
        val response = service.getVideoDetail(aid)
        if (response.code == 0){
                _infoState.value = infoSate.value.copy(
                    info = response.data,
                    grade = if (response.data.stat.evaluation.isBlank()) "无" else response.data.stat.evaluation
                )
                _videoState.value = videoState.value.copy(
                    cid = response.data.cid
                )
                if (response.data.honor_reply != null && response.data.honor_reply.honor != null){
                    _showState.value = showState.value.copy(
                        showRank = true
                    )
                }
                if (response.data.ugc_season != null){
                    val currentEpi:Int = response.data.ugc_season.sections[0].episodes.indexOfFirst {
                        it.aid == _videoState.value.aid
                    }
                    _collectionState.value = collectionState.value.copy(
                        showSeason = !_collectionState.value.showEpi,
                        currentEpi = currentEpi,
                        totalEpi = response.data.ugc_season.ep_count,
                        bean = response.data.ugc_season
                    )
                return response.data.cid
            }
        }else{
            Log.d("VDetailViewModel",response.message)
            return 0
        }
        return 0
    }

    /**
     * 获取视频URL
     * 默认获取64-720P 高清*/
    private suspend fun getVideoUrl(avid: Int,cid:Int,qn:Int = 16):String{
        val params = mutableMapOf(
            "avid" to "$avid",
            "cid" to "$cid",
            "qn" to "$qn",
//            "appkey" to BilibiliProperties.videoAppKey,
//            "device" to BilibiliProperties.platform,
//            "mobi_app" to BilibiliProperties.platform,
//            "platform" to BilibiliProperties.platform,
//            "otype" to "json",
//            "ts" to Instant.now().epochSecond.toString(),
//            "build" to BilibiliProperties.build,
//            "buvid" to BilibiliProperties.buildVersionId,
//            "force_host" to "0",
//            "fnval" to "16",
//            "npcybs" to "0",
//            "fnver" to "0"
        )
        val url = responseBody(params)
        val response = service.getVideoUrl(url)
        if (response.code == 0){
            _videoState.value = videoState.value.copy(
                url = response.data.durl[0].url
            )
            return response.data.durl[0].url
        }
        return ""
    }

    /**
     * 获取与此视频相关的视频*/
    private fun getRelatedVideos(aid:Int){
        service.getRelatedVideos(aid).enqueue(object :Callback<JsonObject>{
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val body = response.body().toString()
                if (body.isNotBlank()){
                    val json = JSONObject(body)
                    val code = json.getInt("code")
                    if (code == 0){
                        val videosJson = json.getJSONArray("data")
                        val videos = GsonFormat.fromListJson(videosJson.toString(),RelateVideosBean::class.java)
                        if (videos != null && videos.isNotEmpty()){
                            _videoState.value = videoState.value.copy(
                                videos = videos
                            )
                        }
                    }
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

            }
        })
    }
}