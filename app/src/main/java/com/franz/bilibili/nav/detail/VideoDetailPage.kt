package com.franz.bilibili.nav.detail


import android.util.Log
import androidx.annotation.DrawableRes
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateIntAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleObserver
import androidx.media3.common.Player
import androidx.media3.ui.PlayerView
import coil.compose.AsyncImage
import com.franz.bilibili.R
import com.franz.bilibili.bean.BilibiliID
import com.franz.bilibili.bean.Owner
import com.franz.bilibili.bean.detail.Episode
import com.franz.bilibili.bean.detail.UgcSeason
import com.franz.bilibili.bean.relate.RelateVideosBean
import com.franz.bilibili.tools.DateTransform
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.ui.theme.white
import com.franz.bilibili.widget.*
import com.google.accompanist.insets.statusBarsPadding

@Composable
fun VideoDetailPage(
     viewModel: VDetailViewModel = hiltViewModel()
){
    val videos = viewModel.videoState.value
    val infos = viewModel.infoSate.value
    val show = viewModel.showState.value
    val collection = viewModel.collectionState.value
    val lazyListState = rememberLazyListState()

    val test by animateIntAsState(
        targetValue = if (lazyListState.firstVisibleItemIndex in  0..1) 0 else 1,
        tween(500)
    )

    var lifecycle by remember { mutableStateOf(Lifecycle.Event.ON_CREATE) }
    val lifecycleOwner = LocalLifecycleOwner.current
    DisposableEffect(key1 = lifecycleOwner){
        val observer = LifecycleEventObserver{ _,event->
            lifecycle = event
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding()
            .navigationBarsPadding()
    ) {
        Column(modifier = Modifier
            .fillMaxSize()
            .background(BilibiliTheme.colors.bottomBar)
        ) {
            /**
             * 顶部标题栏*/
            AnimatedVisibility(visible = test == 1) {
                VideoTopAppBar(
                    onBack = {},
                    onPlay = {}
                )
            }

            LazyColumn(
                state = lazyListState,
                modifier = Modifier.weight(1f)
            ){
                item {
                    /**
                     * ExoPlayer*/
                    ExoPlayer(
                        player = viewModel.player,
                        lifecycle = lifecycle,
                        onCreate = {
                            viewModel.player.prepare()
                            Log.d("VDetailViewModel","onCreate")
                        },
                        onStart = {
                            viewModel.player.play()
                            Log.d("VDetailViewModel","onStart")
                        },
                        onPause = {
                             viewModel.player.pause()
                            Log.d("VDetailViewModel","onPause")
                        },
                        onResume = {
                             viewModel.player.play()
                            Log.d("VDetailViewModel","onResume")
                        },
                        onStop = {
                            viewModel.player.pause()
                            Log.d("VDetailViewModel","onStop")
                        },
                        onDestroy = {
                            viewModel.player.release()
                            Log.d("VDetailViewModel","onDestroy")
                        },
                        onAnother = {
                            Log.d("VDetailViewModel","onAnother")
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(250.dp)
                    )
                }
                /**
                 * 视频信息*/
                item {
                    AnimatedVisibility(infos.info != null) {
                        Column(modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .padding(10.dp)
                        ) {
                            UpInfo(owner = infos.info!!.owner, bvid = infos.info!!.bvid)
                            AnimatedVisibility(visible = show.showRank) {
                                AnimationRank(info = infos.info!!.honor_reply.honor[0].desc)
                            }
                            Spacer(modifier = Modifier.height(10.dp))
                            VideoTitle(title = infos.info!!.title, isExpand = show.showContent) {
                                viewModel.onEvent(VDetailEvent.Expand)
                            }
                            AnimatedVisibility(visible = show.showContent) {
                                AnimationContent(desc = infos.info!!.desc, bvid = infos.info!!.bvid)
                            }
                            Spacer(modifier = Modifier.height(10.dp))
                            InfoBar(
                                grade = infos.grade,
                                play = infos.info!!.stat.view,
                                comment = infos.info!!.stat.danmaku,
                                date = infos.info!!.pubdate,
                                pos = infos.info!!.stat.his_rank
                            )
                            Spacer(modifier = Modifier.height(20.dp))
                            StatBar(
                                like = infos.info!!.stat.like,
                                coin = infos.info!!.stat.coin,
                                favorite = infos.info!!.stat.favorite,
                                share = infos.info!!.stat.share,
                                showLike = show.showLike,
                                showDislike = show.showDislike,
                                showFavorite = show.showFavorite,
                                onClickLike = {
                                    viewModel.onEvent(VDetailEvent.Agreement)
                                },
                                onClickDislike = {
                                    viewModel.onEvent(VDetailEvent.Dislike)
                                },
                                onClickFavorite = {
                                    viewModel.onEvent(VDetailEvent.Favorite)
                                }
                            )
                        }
                    }
                }
                item {
                    AnimatedVisibility(
                        visible = collection.showSeason,
                    ) {
                        AnimationSeason(
                            title = collection.bean!!.title,
                            currentEpi = collection.currentEpi,
                            totalEpi = collection.totalEpi) {
                            viewModel.onEvent(VDetailEvent.Epi)
                        }
                    }
                }
                /**
                 * 相关视频列表*/
                items(videos.videos.size){
                    RelatedVideoItem(videos.videos[it]){

                    }
                    if (it < videos.videos.size - 1){
                        Divider(modifier = Modifier.padding(top = 10.dp, bottom = 10.dp))
                    }
                }
            }
        }
    }
    /**
     * 如果此视频拥有合集
     * 则显示视频合集的相关视频*/
        AnimatedVisibility(visible = collection.showEpi) {
            Box(modifier = Modifier
                .padding(top = 280.dp)
                .fillMaxSize()
                .background(BilibiliTheme.colors.bottomBar)
                .navigationBarsPadding()
            ){
                AnimationEpi(
                    ugcSeason = collection.bean!!,
                    currentEpi = collection.currentEpi,
                    onPlayVideo = {  },
                    onDismiss = { viewModel.onEvent(VDetailEvent.Epi) }
                )
            }
        }
}

/**
 * 相关视频list Item*/
@Composable
private fun RelatedVideoItem(
    bean: RelateVideosBean,
    imageHeight: Dp = 100.dp,
    onClickItem:(BilibiliID)->Unit
){
    ConstraintLayout(
        modifier = Modifier
            .padding(start = 10.dp, end = 10.dp)
            .fillMaxWidth()
            .height(imageHeight)
            .background(BilibiliTheme.colors.backgroundTop, shape = RoundedCornerShape(4.dp))
            .clickable { onClickItem(BilibiliID(aid = bean.aid, cid = bean.cid)) }
    ) {
        val (imageRef,viewRef,titleRef,authorRef,publishRef,durationRef,reasonRef) = createRefs()
        /**
         * 封面*/
        AsyncImage(
            model = bean.pic,
            contentDescription = bean.owner.name,
            contentScale = ContentScale.Crop,
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .fillMaxHeight()
                .clip(RoundedCornerShape(4.dp))
                .aspectRatio(1.7f)//aspectRatio:width/height
                .constrainAs(imageRef) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }
        )

        /**
         * 视频标题*/
        Text(
            text = bean.title,
            color = BilibiliTheme.colors.textTitle,
            style = MaterialTheme.typography.caption,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Start,
            modifier = Modifier
                .height(40.dp)
                .constrainAs(titleRef) {
                    start.linkTo(imageRef.end, 5.dp)
                    end.linkTo(parent.end, 5.dp)
                    top.linkTo(imageRef.top, 2.dp)
                    width = Dimension.fillToConstraints
                }
        )

        /**
         * 视频时长*/
        Text(
            text = doubleFormat(decimal = 2, number = bean.duration / 60.0),
            color = white,
            style = MaterialTheme.typography.overline,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .background(
                    color = Color.Black.copy(alpha = 0.4f),
                    shape = RoundedCornerShape(2.dp)
                )
                .padding(2.dp)
                .constrainAs(durationRef) {
                    top.linkTo(imageRef.bottom, ((-20).dp))
                    end.linkTo(imageRef.end, 6.dp)
                }
        )

        RecommendReason(
            content = bean.rcmd_reason,
            modifier = Modifier.constrainAs(reasonRef){
                bottom.linkTo(authorRef.top,2.dp)
                start.linkTo(viewRef.start)
            }
        )

        /**
         * 作者*/
        videoAuthor(
            bean.owner.name,
            modifier = Modifier
                .constrainAs(authorRef) {
                    bottom.linkTo(viewRef.top,2.dp)
                    start.linkTo(viewRef.start,(-5).dp)
                }
        )

        /**
         * 观看人数*/
        ItemRow(
            number = bean.stat.view,
            icon = R.drawable.icon_view,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(viewRef){
                bottom.linkTo(imageRef.bottom,2.dp)
                start.linkTo(imageRef.end,5.dp)
            }
        )

        /**
         * 发布时间*/
        Text(
            text = DateTransform.millToDate((bean.pubdate.toLong()*1000)),
            color = BilibiliTheme.colors.textContent,
            style = MaterialTheme.typography.overline,
            modifier = Modifier.constrainAs(publishRef){
                top.linkTo(viewRef.top)
                bottom.linkTo(viewRef.bottom)
                start.linkTo(viewRef.end,5.dp)
            }
        )
    }
}
/**
 * 播放页面顶部标题栏*/
@Composable
private fun VideoTopAppBar(
    backIcon:ImageVector = Icons.Default.ArrowBackIos,
    playIcon:ImageVector = Icons.Default.MovieFilter,
    moreIcon:ImageVector = Icons.Default.MoreVert,
    title: String = "立即播放",
    modifier: Modifier = Modifier,
    onBack:()->Unit,
    onPlay:()->Unit
){
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .background(BilibiliTheme.colors.highlightColor)
            .padding(15.dp)
    ) {
        Icon(
            imageVector = backIcon,
            contentDescription = "go back",
            tint = BilibiliTheme.colors.bottomBar,
            modifier = Modifier
                .size(24.dp)
                .clickable { onBack() }
        )
        Row(
            modifier = Modifier.weight(1f),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Icon(
                imageVector = playIcon,
                contentDescription = "immediately to play",
                tint = BilibiliTheme.colors.bottomBar,
                modifier = Modifier.size(24.dp)
            )
            Text(
                text = title,
                color = BilibiliTheme.colors.bottomBar,
                fontSize = 16.sp,
                modifier = Modifier.clickable { onPlay() }
            )
        }
        Icon(
            imageVector = moreIcon,
            contentDescription = "more",
            tint = BilibiliTheme.colors.bottomBar,
            modifier = Modifier.size(24.dp)
        )
    }
}
/**
 * 视频Up主图像、昵称等信息*/
@Composable
private fun UpInfo(owner: Owner,bvid:String,height:Dp = 48.dp){
    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        AsyncImage(
            model = owner.face,
            contentDescription = owner.face,
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .size(height)
                .clip(shape = CircleShape)
        )
        Spacer(modifier = Modifier.width(5.dp))
        Column(
            modifier = Modifier
                .height(height)
                .padding(top = 5.dp, bottom = 5.dp),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = owner.name,
                fontSize = 12.sp,
                color = BilibiliTheme.colors.textTitle
            )
            Text(
                text = bvid,
                fontSize = 10.sp,
                color = BilibiliTheme.colors.textContent
            )
        }
    }
}

/**
 * 显示视频标题*/
@Composable
private fun VideoTitle(
    title:String,
    isExpand:Boolean,
    onClick:()->Unit
){
    val icon by animateIntAsState(
        targetValue = if (isExpand){
            R.drawable.icon_expand_more
        }else{
            R.drawable.icon_expand_less
        }
    )

    Row(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = title,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = BilibiliTheme.colors.textTitle,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.weight(1f)
        )
        Icon(
            painter = painterResource(id = icon),
            contentDescription = "expand",
            tint = BilibiliTheme.colors.textContent,
            modifier = Modifier
                .size(24.dp)
                .clickable { onClick() }
        )
    }
}
/**
 * 显示视频desc介绍*/
@Composable
private fun AnimationContent(
    desc:String,
    bvid:String,
    banTip:String = "未经作者授权禁止转载",
    @DrawableRes icon:Int = R.drawable.icon_ban
){
    Column(
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            text = desc,
            fontSize = 10.sp,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(4.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = bvid,
                fontSize = 10.sp,
                color = BilibiliTheme.colors.textContent,
            )
            Spacer(modifier = Modifier.width(5.dp))
            Image(
                painter = painterResource(id = icon),
                contentDescription = "ban",
                modifier = Modifier.size(18.dp)
            )
            Text(
                text = banTip,
                fontSize = 10.sp,
                color = BilibiliTheme.colors.textContent,
            )
        }
    }
}
/**
 * 如果该视频拥在排行榜中拥有排名
 * 则显示相关排名信息*/
@Composable
private fun AnimationRank(
    info:String,
    tips:String = "当前排行榜",
    @DrawableRes icon:Int = R.drawable.icon_video_rank
){
    Row(
        modifier = Modifier
            .padding(top = 10.dp)
            .fillMaxWidth()
            .background(BilibiliTheme.colors.textContent, RoundedCornerShape(10.dp))
            .padding(5.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = "Video Rank",
            tint = BilibiliTheme.colors.highlightColor,
            modifier = Modifier.size(20.dp)
        )
        Text(
            text = info,
            fontSize = 14.sp,
            fontWeight = FontWeight.Bold,
            color = BilibiliTheme.colors.highlightColor,
        )
        Spacer(modifier = Modifier.weight(1f))
        Text(
            text = tips,
            fontSize = 12.sp,
            color = BilibiliTheme.colors.highlightColor,
        )
    }
}



/**
 * 如果此视频为合集
 * 则显示此视频的所有系列相关视频*/
@Composable
private fun AnimationSeason(
    title: String,
    currentEpi:Int,
    totalEpi:Int,
    moreIcon:ImageVector = Icons.Default.ArrowForwardIos,
    onShowEpi:()->Unit
){
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(start = 10.dp, end = 10.dp, top = 10.dp, bottom = 20.dp)
            .fillMaxWidth()
            .background(BilibiliTheme.colors.textContent, RoundedCornerShape(10.dp))
            .padding(5.dp)
            .clickable { onShowEpi() }
    ) {
        Text(
            text = "合集:".plus(title),
            color = BilibiliTheme.colors.textTitle,
            fontSize = 14.sp,
            fontWeight = FontWeight.Bold,
            overflow = TextOverflow.Ellipsis,
            maxLines = 1,
            modifier = Modifier.weight(1f)
        )
        Text(
            text = "$currentEpi/$totalEpi",
            color = BilibiliTheme.colors.textTitle.copy(alpha = 0.5f),
            fontSize = 12.sp
        )
        Icon(
            imageVector = moreIcon,
            contentDescription = "expand",
            tint = BilibiliTheme.colors.textTitle.copy(alpha = 0.5f),
            modifier = Modifier.size(16.dp)
        )
        
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun AnimationEpi(
    ugcSeason: UgcSeason,
    currentEpi: Int,
    closeIcon:ImageVector = Icons.Default.Close,
    modifier: Modifier = Modifier,
    onPlayVideo:()->Unit,
    onDismiss:()->Unit
){
    LazyColumn(
        modifier = modifier
            .fillMaxSize()
            .padding(10.dp),
        verticalArrangement = Arrangement.Center
    ){
        stickyHeader { 
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = "合集(${ugcSeason.ep_count})",
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold,
                    color = BilibiliTheme.colors.textTitle
                )
                Spacer(modifier = Modifier.weight(1f))
                Icon(
                    imageVector = closeIcon,
                    contentDescription = "close epi page",
                    tint = BilibiliTheme.colors.textTitle,
                    modifier = Modifier
                        .size(24.dp)
                        .clickable { onDismiss() }
                )
            }
        }
        item {
            Column(modifier = Modifier.fillMaxWidth()) {
                Divider(modifier = Modifier.padding(top = 10.dp, bottom = 10.dp))
                Text(
                    text = ugcSeason.title,
                    fontSize = 16.sp,
                    color = BilibiliTheme.colors.textTitle,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = ugcSeason.intro,
                    fontSize = 12.sp,
                    color = BilibiliTheme.colors.textContent,
                    maxLines = 5,
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(10.dp))
            }
        }
        items(ugcSeason.sections[0].episodes.size){
            EpiVideoItem(
                bean = ugcSeason.sections[0].episodes[it],
                currentEpi = it == currentEpi,
                onClickItem = {}
            )
            if (it < ugcSeason.sections[0].episodes.size - 1){
                Divider(modifier = Modifier.padding(top = 10.dp, bottom = 10.dp))
            }
        }
    }
}
@Composable
private fun EpiVideoItem(
    bean: Episode,
    imageHeight: Dp = 100.dp,
    currentEpi:Boolean = false,
    onClickItem:(BilibiliID)->Unit
){
    val color = animateColorAsState(
        targetValue = if (currentEpi)
            BilibiliTheme.colors.highlightColor
        else
            BilibiliTheme.colors.textTitle
    )

    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .height(imageHeight)
            .background(BilibiliTheme.colors.backgroundTop, shape = RoundedCornerShape(4.dp))
            .clickable { onClickItem(BilibiliID(aid = bean.aid, cid = bean.cid)) }
    ) {
        val (imageRef,viewRef,titleRef,descRef,publishRef,durationRef,replayRef,subtitleRef) = createRefs()
        /**
         * 封面*/
        AsyncImage(
            model = bean.arc.pic,
            contentDescription = bean.arc.pic,
            contentScale = ContentScale.Crop,
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .fillMaxHeight()
                .clip(RoundedCornerShape(4.dp))
                .aspectRatio(1.7f)//aspectRatio:width/height
                .constrainAs(imageRef) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }
        )

        /**
         * 视频标题*/
        Text(
            text = bean.arc.title,
            color = color.value,
            style = MaterialTheme.typography.subtitle2,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Start,
            modifier = Modifier
                .fillMaxWidth()
                .constrainAs(titleRef) {
                    start.linkTo(imageRef.end, 5.dp)
                    end.linkTo(parent.end, 5.dp)
                    top.linkTo(imageRef.top, 2.dp)
                    width = Dimension.fillToConstraints
                }
        )
        /**
         * 视频desc*/
        Text(
            text = bean.arc.desc,
            color = BilibiliTheme.colors.textContent,
            fontSize = 12.sp,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Start,
            modifier = Modifier
                .fillMaxWidth()
                .constrainAs(descRef) {
                    start.linkTo(titleRef.start)
                    end.linkTo(titleRef.end)
                    top.linkTo(titleRef.bottom, 2.dp)
                    width = Dimension.fillToConstraints
                }
        )

        /**
         * 视频时长*/
        Text(
            text = doubleFormat(decimal = 2, number = bean.arc.duration / 60.0),
            color = white,
            style = MaterialTheme.typography.overline,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .background(
                    color = Color.Black.copy(alpha = 0.4f),
                    shape = RoundedCornerShape(2.dp)
                )
                .padding(2.dp)
                .constrainAs(durationRef) {
                    top.linkTo(imageRef.bottom, ((-20).dp))
                    end.linkTo(imageRef.end, 6.dp)
                }
        )


        /**
         * 观看人数*/
        ItemRow(
            number = bean.arc.stat.view,
            icon = R.drawable.icon_view,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(viewRef){
                bottom.linkTo(imageRef.bottom,2.dp)
                start.linkTo(imageRef.end,5.dp)
            }
        )

        /**
         * 评论人数*/
        ItemRow(
            number = bean.arc.stat.reply,
            icon = R.drawable.icon_review,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(replayRef){
                top.linkTo(viewRef.top)
                start.linkTo(viewRef.end)
                end.linkTo(subtitleRef.start)
            }
        )

        /**
         * 弹幕人数*/
        ItemRow(
            number = bean.arc.stat.danmaku,
            icon = R.drawable.icon_subtitle,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(subtitleRef){
                top.linkTo(viewRef.top)
                end.linkTo(parent.end,5.dp)
            }
        )

        /**
         * 发布时间*/
        Text(
            text = DateTransform.millToDate((bean.arc.pubdate.toLong()*1000)),
            color = BilibiliTheme.colors.textContent,
            style = MaterialTheme.typography.overline,
            modifier = Modifier.constrainAs(publishRef){
                bottom.linkTo(viewRef.top,5.dp)
                start.linkTo(viewRef.start)
            }
        )
    }
}