package com.franz.bilibili.nav.home

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyGridItemScope
import androidx.compose.foundation.lazy.grid.LazyGridScope
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.lerp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.paging.compose.LazyPagingItems
import com.franz.bilibili.bean.BilibiliID
import com.franz.bilibili.nav.home.hot.HotPage
import com.franz.bilibili.nav.home.recommend.RecommendPage
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.ui.theme.white
import com.franz.bilibili.widget.TopAppBar
import com.google.accompanist.insets.statusBarsPadding
import com.google.accompanist.pager.*
import kotlinx.coroutines.launch
import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.roundToInt

/**
 * 首页内容
 * 使用Tab+Page滑动组件控制推荐和热门两个页面
 * 顶部含有搜索栏+Tab*/
@OptIn(ExperimentalPagerApi::class)
@Composable
fun HomePage(
    modifier: Modifier = Modifier,
    viewModel: HomeViewModel = hiltViewModel(),
    onClick: (String)->Unit,
    onClickItem:(BilibiliID)->Unit
) {
    //默认搜索关键词
    val key = viewModel.searchKey.value

    val pages = remember {
        mutableStateListOf("推荐", "热门")
    }
    val pageState = rememberPagerState(
        initialPage = 0
    )
    val scope = rememberCoroutineScope()

    /**
     * 用于联动"nestedScroll"进行滑动处理
     * 当上滑时，则可以相对应内容隐藏；
     * 当下滑时，顶部标题栏随机显示，不必要滑动到底部才显示*/
    val tabBarHeight = 32.dp
    val toolbarHeightPx = with(LocalDensity.current) { tabBarHeight.roundToPx().toFloat() }
    val toolbarOffsetHeightPx = remember { mutableStateOf(0f) }
    val nestedScrollConnection = remember {
        object : NestedScrollConnection {
            override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
                val delta = available.y
                val newOffset = toolbarOffsetHeightPx.value + delta
                toolbarOffsetHeightPx.value = newOffset.coerceIn(-toolbarHeightPx, 0f)
                return Offset.Zero
            }
        }
    }

    Column(
        modifier = modifier
            .fillMaxSize()
            .background(
                Brush.horizontalGradient(
                    listOf(
                        BilibiliTheme.colors.loginStart,
                        BilibiliTheme.colors.loginEnd
                    )
                )
            )
            .nestedScroll(nestedScrollConnection)
            .statusBarsPadding()
            .padding(10.dp)
    ) {
        /**
         * 顶部搜索栏*/
        TopAppBar(
            hint = key,
            height = tabBarHeight,
            modifier = Modifier.offset { IntOffset(0, (toolbarOffsetHeightPx.value * 2.5).roundToInt()) }
        ) {
            //点击了顶部搜索栏，然后获取顶部搜索栏默认搜索关键字，并跳转至搜索界面
            onClick(it)
        }

        /**
         * Tab*/
        TabRow(
            selectedTabIndex = pageState.currentPage,
            backgroundColor = Color.Transparent,
            indicator = { pos ->
                TabRowDefaults.Indicator(
                    color = BilibiliTheme.colors.tabSelect,
                    modifier = Modifier.customIndicatorOffset(
                        pagerState = pageState,
                        tabPositions = pos,
                        32.dp
                    )
                )
            },
            divider = {
                TabRowDefaults.Divider(color = Color.Transparent)
            },
            modifier = Modifier
                .height(tabBarHeight)
                .fillMaxWidth()
                .offset { IntOffset(0, (toolbarOffsetHeightPx.value * 1.5).roundToInt()) }
        ) {
            pages.forEachIndexed { index, title ->
                Tab(
                    text = { Text(title) },
                    selected = pageState.currentPage == index,
                    onClick = {
                        scope.launch {
                            pageState.scrollToPage(index)
                        }
                    },
                    enabled = true,
                    selectedContentColor = BilibiliTheme.colors.tabSelect,
                    unselectedContentColor = BilibiliTheme.colors.tabUnselect,
                )
            }
        }


        /**
         * ViewPager
         * 横向Viewpager，存放着推荐内容页面和热门内容页面*/
        HorizontalPager(
            count = pages.size,
            state = pageState,
            modifier = Modifier
                .fillMaxSize()
                .padding(5.dp)
                .offset { IntOffset(0, (toolbarOffsetHeightPx.value).roundToInt()) }
        ) { pos ->
            when (pos) {
                0 -> {
                    RecommendPage(onClickItem = onClickItem)
                }
                1 -> {
                    HotPage(onClickItem = onClickItem)
                }
            }
        }
    }
}

/**
 * 重写TabRow的pagerTabIndicatorOffset
 * 让TabRow 的indicator宽度可以进行自定义*/
@ExperimentalPagerApi
fun Modifier.customIndicatorOffset(
    pagerState: PagerState,
    tabPositions: List<TabPosition>,
    width: Dp
): Modifier = composed {
    if (pagerState.pageCount == 0) return@composed this

    val targetIndicatorOffset: Dp
    val indicatorWidth: Dp

    val currentTab = tabPositions[minOf(tabPositions.lastIndex, pagerState.currentPage)]
    val targetPage = pagerState.targetPage
    val targetTab = tabPositions.getOrNull(targetPage)

    if (targetTab != null) {
        val targetDistance = (targetPage - pagerState.currentPage).absoluteValue
        val fraction = (pagerState.currentPageOffset / max(targetDistance, 1)).absoluteValue

        targetIndicatorOffset = lerp(currentTab.left, targetTab.left, fraction)
        indicatorWidth = lerp(currentTab.width, targetTab.width, fraction).value.absoluteValue.dp
    } else {
        targetIndicatorOffset = currentTab.left
        indicatorWidth = currentTab.width
    }

    fillMaxWidth()
        .wrapContentSize(Alignment.BottomStart)
        .padding(horizontal = (indicatorWidth - width) / 2)
        .offset(x = targetIndicatorOffset)
        .width(width)
}

/**
 * LazyVerticalGrid还不支持Paging3分页管理库，通过此扩展函数可以实现*/
fun <T : Any> LazyGridScope.items(
    items: LazyPagingItems<T>,
    key: ((item: T) -> Any)? = null,
    span: ((item: T) -> GridItemSpan)? = null,
    contentType: ((item: T) -> Any)? = null,
    itemContent: @Composable LazyGridItemScope.(value: T?) -> Unit
) {
    items(
        count = items.itemCount,
        key = if (key == null) null else { index ->
            val item = items.peek(index)
            if (item == null) {
                //PagingPlaceholderKey(index)
            } else {
                key(item)
            }
        },
        span = if (span == null) null else { index ->
            val item = items.peek(index)
            if (item == null) {
                GridItemSpan(1)
            } else {
                span(item)
            }
        },
        contentType = if (contentType == null) {
            { null }
        } else { index ->
            val item = items.peek(index)
            if (item == null) {
                null
            } else {
                contentType(item)
            }
        }
    ) { index ->
        itemContent(items[index])
    }
}