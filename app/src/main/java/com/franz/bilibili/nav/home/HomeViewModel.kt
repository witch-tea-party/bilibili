package com.franz.bilibili.nav.home

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.franz.bilibili.network.OfficialService
import com.google.gson.JsonObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val service: OfficialService): ViewModel() {
    private val _searchKey = mutableStateOf("")
    val searchKey:State<String> = _searchKey

    init {
        viewModelScope.launch {
            getDefaultKey()
        }
    }

    /**
     * 当使用call<JsonObject>作为接口返回值时，不能在使用suspend作为接口标记
     * 否则会出现异常：Unable to invoke no-args constructor for retrofit2.Call
     *
     * 返回的类型必须是google.gson.JsonObject而不是org.json.JSONObject
     * 不然会出现解析内容为{}*/
    private fun getDefaultKey(){
        service.getDefaultSearchKey().enqueue(object: Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val body = response.body().toString()
                val jsonObject = JSONObject(body)
                if (jsonObject.has("code")){
                    val code = jsonObject.getInt("code")
                    //后端接口规定code=0为正常数据
                    if (code == 0){
                        val key = jsonObject.getJSONObject("data").get("name").toString()
                        _searchKey.value = key
                    }
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.d("HomeViewModel","error message=${t.message}")
            }
        })
    }
}