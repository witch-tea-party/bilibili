package com.franz.bilibili.nav.home.hot

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.franz.bilibili.bean.hot.HotBean
import com.franz.bilibili.network.OfficialService

class HotPagingSource(private val service: OfficialService):PagingSource<Int,HotBean>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, HotBean> {
       return try {
           val page = params.key ?: 1 //当前页，默认第一页
           val pageSize = params.loadSize //每页数据条数
           val repository = service.getHot(page,pageSize) //获取的数据源
           val repositoryItem = repository.data.list //获取的数据列表
           val previousPage = if (page > 1) page - 1 else null //前一页
           val nextPage = if (repositoryItem.isNotEmpty()) page+1 else null //下一页
           LoadResult.Page(repositoryItem,previousPage,nextPage)
       }catch (e:Exception){
           LoadResult.Error(e)
       }
    }

    override fun getRefreshKey(state: PagingState<Int, HotBean>): Int? = null
}