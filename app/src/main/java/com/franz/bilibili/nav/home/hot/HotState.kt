package com.franz.bilibili.nav.home.hot

import com.franz.bilibili.bean.hot.HotModel
import com.google.gson.annotations.SerializedName

data class HotState(
//    @SerializedName("data/list")
    val hots: List<HotModel> = emptyList()
)
