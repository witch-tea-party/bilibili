package com.franz.bilibili.nav.home.recommend

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import coil.compose.AsyncImage
import com.franz.bilibili.R
import com.franz.bilibili.bean.BilibiliID
import com.franz.bilibili.bean.recommend.RecommendBean
import com.franz.bilibili.nav.home.items
import com.franz.bilibili.param.recommend_banners
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.ui.theme.white
import com.franz.bilibili.widget.*


@Composable
fun RecommendPage(
    viewModel: RecommendViewModel = hiltViewModel(),
    onClickItem:(BilibiliID)->Unit
){
    val recommends = viewModel.getRecommendData().collectAsLazyPagingItems()
    Column(
        modifier = Modifier.fillMaxSize()
    ){
        LazyVerticalGrid(
            columns = GridCells.Fixed(2),
            horizontalArrangement = Arrangement.spacedBy(4.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp),
            modifier = Modifier.weight(1f),
        ){
            /**
             * 此操作可以根据起item数量改变其span数量*/
            item(
                span = { GridItemSpan(this.maxLineSpan) }
            ) {
                BilibiliBanner(images = recommend_banners)
                Spacer(modifier = Modifier.height(10.dp))
            }

            when(recommends.loadState.refresh){
                is LoadState.Loading-> {
                    item(span = {GridItemSpan(this.maxLineSpan)}) {
                    Loading()
                }}
                is LoadState.Error-> {
                   item(span = {GridItemSpan(this.maxLineSpan)}){
                       LargeLoadFailed(){recommends.retry()}
                   }
                }
                else ->{}
            }

            items(items = recommends){ item->
                if (item != null){
                    RecommendItem(bean = item, onClickItem = onClickItem)
                }
            }

            when(recommends.loadState.append){
                is LoadState.NotLoading-> {}
                is LoadState.Loading-> {}
                is LoadState.Error-> {
                    item(span = {GridItemSpan(this.maxLineSpan)}){
                        SmallLoadFailed(){recommends.retry()}
                    }
                }
            }
        }
    }
}

/**
 * GridView 2列x行时，水平Item布局组件*/
@Composable
fun RecommendItem(
    bean: RecommendBean,
    imageHeight:Dp = 120.dp,
    onClickItem:(BilibiliID)->Unit
){
    ConstraintLayout(
        modifier = Modifier
            .background(BilibiliTheme.colors.backgroundTop, shape = RoundedCornerShape(4.dp))
            .padding(bottom = 5.dp)
            .clickable { onClickItem(BilibiliID(aid = bean.aid, cid = 0)) }
    ) {
        val (imageRef,viewRef,titleRef,authorRef) = createRefs()
        /**
         * 封面*/
        AsyncImage(
            model = bean.pic,
            contentDescription = bean.author,
            contentScale = ContentScale.Crop,
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .height(imageHeight)
                .clip(RoundedCornerShape(topStart = 4.dp, topEnd = 4.dp))
                .constrainAs(imageRef) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    width = Dimension.fillToConstraints
                }
        )

        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.Black.copy(alpha = 0.4f))
                .padding(start = 5.dp, end = 5.dp)
                .constrainAs(viewRef) {
                    start.linkTo(parent.start)
                    top.linkTo(imageRef.bottom, (-20).dp)
                }
        ){
            /**
             * 观看人数*/
            ItemRow(
                number = bean.play,
                icon = R.drawable.icon_view
            )

            /**
             * 评论人数*/
            ItemRow(
                number = bean.review,
                icon = R.drawable.icon_review,
                modifier = Modifier
                    .weight(1f)
                    .align(Alignment.CenterVertically)
            )
            /**
             * 视频时长*/
            Text(
                text = bean.duration,
                color = white,
                style = MaterialTheme.typography.overline,
                textAlign = TextAlign.Center
            )
        }
        /**
         * 视频标题*/
        Text(
            text = bean.title,
            color = BilibiliTheme.colors.textTitle,
            style = MaterialTheme.typography.caption,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Start,
            modifier = Modifier
                .height(40.dp)
                .constrainAs(titleRef) {
                    start.linkTo(parent.start, 5.dp)
                    end.linkTo(parent.end, 5.dp)
                    top.linkTo(imageRef.bottom, 5.dp)
                    width = Dimension.fillToConstraints
                }
        )

        videoAuthor(
            bean.author,
            modifier = Modifier
                .constrainAs(authorRef) {
                    top.linkTo(titleRef.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
        )
    }
}




