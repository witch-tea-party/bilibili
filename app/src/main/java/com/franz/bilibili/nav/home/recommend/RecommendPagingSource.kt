package com.franz.bilibili.nav.home.recommend

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.franz.bilibili.bean.recommend.RecommendBean
import com.franz.bilibili.network.OfficialService

class RecommendPagingSource(private val service: OfficialService):PagingSource<Int,RecommendBean>() {
    override fun getRefreshKey(state: PagingState<Int, RecommendBean>): Int? = null

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, RecommendBean> {
       return try {
           val page = params.key ?: 1 //当前页，默认第一页
           val pageSize = params.loadSize //每页数据条数
           val repository = service.getRecommend(page,pageSize) //获取的数据源
           val repositoryItem = repository.recommends //获取的数据列表
           val previousPage = if (page > 1) page - 1 else null //前一页
           val nextPage = if (repositoryItem.isNotEmpty()) page+1 else null //下一页
           LoadResult.Page(repositoryItem,previousPage,nextPage)
       }catch (e:Exception){
           LoadResult.Error(e)
       }
    }
}