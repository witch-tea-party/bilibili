package com.franz.bilibili.nav.home.recommend

import com.franz.bilibili.bean.recommend.RecommendBean
import com.google.gson.annotations.SerializedName

data class RecommendState(
    @SerializedName("list")
    val recommends: List<RecommendBean> = emptyList()
)
