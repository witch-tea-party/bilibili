package com.franz.bilibili.nav.home.recommend

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.franz.bilibili.bean.recommend.RecommendBean
import com.franz.bilibili.network.OfficialService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class RecommendViewModel @Inject constructor(private val service:OfficialService):ViewModel() {

    private val PAGE_SIZE = 10


    fun getRecommendData(): Flow<PagingData<RecommendBean>> {
        return Pager(
            config = PagingConfig(pageSize = PAGE_SIZE, initialLoadSize = PAGE_SIZE),
            pagingSourceFactory = { RecommendPagingSource(service) }
        ).flow.cachedIn(viewModelScope)
    }
}