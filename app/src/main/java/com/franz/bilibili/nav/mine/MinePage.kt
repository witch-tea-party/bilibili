package com.franz.bilibili.nav.mine

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.progressSemantics
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.paint
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.*
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.franz.bilibili.R
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.google.accompanist.insets.statusBarsPadding
import kotlinx.coroutines.delay

@Composable
fun MinePage(
    modifier: Modifier = Modifier,
    name:String = "FranzLiszt",
    @DrawableRes bg:Int = R.drawable.user_profile_bg,
    @DrawableRes image:Int = R.drawable.user_profile_img,
    @StringRes introduction:Int = R.string.ProjectIntroduction
){
    ConstraintLayout(modifier = modifier
        .fillMaxSize()
        .paint(
            painterResource(id = bg),
            contentScale = ContentScale.Crop
        )
        .statusBarsPadding()
    ){
        val (alphaRef,imgRef,splitRef,nameRef,describeRef,progress) = createRefs()
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(400.dp)
                .background(
                    color = BilibiliTheme.colors.bottomBar.copy(alpha = 0.8f),
                    shape = RoundedCornerShape(topStart = 20.dp)
                )
                .constrainAs(alphaRef) {
                    start.linkTo(parent.start, 200.dp)
                    end.linkTo(parent.end)
                    bottom.linkTo(parent.bottom)
                }
        )

        Box(modifier = Modifier
            .size(64.dp)
            .background(shape = CircleShape, color = Color.Transparent)
            .constrainAs(imgRef) {
                end.linkTo(parent.end, 32.dp)
                top.linkTo(alphaRef.top, (-32).dp)
            }
        ){
            Image(
                painter = painterResource(id = image),
                contentDescription = "user image",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .clip(shape = CircleShape)
            )
        }
        circleProgress(
            color = BilibiliTheme.colors.highlightColor,
            size = 64.dp,
            modifier = Modifier.constrainAs(progress){
                end.linkTo(imgRef.end)
                top.linkTo(imgRef.top)
            }
        )

        Text(
            text = name,
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = BilibiliTheme.colors.textTitle.copy(alpha = 0.6f),
            modifier = Modifier
                .constrainAs(nameRef){
                    top.linkTo(alphaRef.top,50.dp)
                    start.linkTo(alphaRef.start,32.dp)
                }
        )

        Divider(
            color = BilibiliTheme.colors.textTitle.copy(alpha = 0.4f),
            modifier = Modifier
                .constrainAs(splitRef){
                    start.linkTo(alphaRef.start,32.dp)
                    end.linkTo(parent.end,32.dp)
                    top.linkTo(nameRef.bottom,32.dp)
                    width = Dimension.fillToConstraints
                }
        )

        Text(
            text = stringResource(id = introduction),
            fontSize = 14.sp,
            color = BilibiliTheme.colors.textTitle.copy(alpha = 0.5f),
            letterSpacing = 2.sp,
            lineHeight = 20.sp,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .constrainAs(describeRef){
                    top.linkTo(splitRef.bottom,32.dp)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(splitRef.start)
                    end.linkTo(parent.end,32.dp)
                    width = Dimension.fillToConstraints
                    height = Dimension.fillToConstraints
                }
        )
    }
}

@Composable
fun circleProgress(
    color: Color,
    size:Dp,
    modifier: Modifier = Modifier
){
    var progress by remember { mutableStateOf(0f) }
    LaunchedEffect(key1 = progress){
        if (progress >= 360f)  progress = 0f
        delay(64)
        progress += 1f
    }
    Canvas(modifier = modifier
        .size(size)
    ){
        drawArc(
            color = color,
            startAngle = 270f,
            sweepAngle = progress,
            useCenter = false,
            style = Stroke(width = 6.dp.value, cap = StrokeCap.Round)
        )
    }
}