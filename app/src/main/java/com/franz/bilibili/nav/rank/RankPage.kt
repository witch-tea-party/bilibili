package com.franz.bilibili.nav.rank

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.franz.bilibili.bean.BilibiliID
import com.franz.bilibili.nav.home.customIndicatorOffset
import com.franz.bilibili.param.rankValueList
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.google.accompanist.insets.statusBarsPadding
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.launch

@OptIn(ExperimentalPagerApi::class)
@Composable
fun RankPage(
    modifier: Modifier = Modifier,
    onClickItem:(BilibiliID)->Unit
){
    val pages = remember { rankValueList }
    val pageState = rememberPagerState(
        initialPage = 0
    )
    val scope = rememberCoroutineScope()

    Column(
        modifier = modifier
            .fillMaxSize()
            .background(
                Brush.horizontalGradient(
                    listOf(
                        BilibiliTheme.colors.loginStart,
                        BilibiliTheme.colors.loginEnd
                    )
                )
            )
            .statusBarsPadding()
            .padding(10.dp)
    ) {
        Text(
            text = "排行榜",
            fontSize = 16.sp,
            color = BilibiliTheme.colors.textTitle,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
        /**
         * Tab*/
        ScrollableTabRow(
            selectedTabIndex = pageState.currentPage,
            backgroundColor = Color.Transparent,
            indicator = { pos ->
                TabRowDefaults.Indicator(
                    color = BilibiliTheme.colors.tabSelect,
                    modifier = Modifier.customIndicatorOffset(
                        pagerState = pageState,
                        tabPositions = pos,
                        32.dp
                    )
                )
            },
            divider = {
                TabRowDefaults.Divider(color = Color.Transparent)
            },
            modifier = Modifier.wrapContentWidth(),
            edgePadding = 0.dp
        ) {
            pages.forEachIndexed { index, title ->
                Tab(
                    text = { Text(title) },
                    selected = pageState.currentPage == index,
                    onClick = {
                        scope.launch {
                            pageState.scrollToPage(index)
                        }
                    },
                    enabled = true,
                    selectedContentColor = BilibiliTheme.colors.tabSelect,
                    unselectedContentColor = BilibiliTheme.colors.tabUnselect,
                )
            }
        }


        HorizontalPager(
            count = pages.size,
            state = pageState,
            modifier = Modifier
                .fillMaxSize()
                .padding(5.dp)
        ) { pos ->
            when(pageState.currentPage){
                0->{RankSubPage(0, onClickItem = onClickItem)}
                1->{RankSubPage(1,onClickItem = onClickItem)}
                2->{RankSubPage(2,onClickItem = onClickItem)}
                3->{RankSubPage(3,onClickItem = onClickItem)}
                4->{RankSubPage(4,onClickItem = onClickItem)}
                5->{RankSubPage(5,onClickItem = onClickItem)}
                6->{RankSubPage(6,onClickItem = onClickItem)}
                7->{RankSubPage(7,onClickItem = onClickItem)}
                8->{RankSubPage(8,onClickItem = onClickItem)}
                9->{RankSubPage(9,onClickItem = onClickItem)}
                10->{RankSubPage(10,onClickItem = onClickItem)}
                11->{RankSubPage(11,onClickItem = onClickItem)}
                12->{RankSubPage(12,onClickItem = onClickItem)}
                13->{RankSubPage(13,onClickItem = onClickItem)}
                14->{RankSubPage(14,onClickItem = onClickItem)}
                15->{RankSubPage(15,onClickItem = onClickItem)}
            }
        }
    }
}