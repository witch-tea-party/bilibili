package com.franz.bilibili.nav.rank

import com.franz.bilibili.bean.rank.RankBean

data class RankState(
    var rid: Int = -1,
    val beans:List<RankBean> = emptyList()
)
