package com.franz.bilibili.nav.rank

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.franz.bilibili.R
import com.franz.bilibili.bean.BilibiliID
import com.franz.bilibili.bean.hot.HotBean
import com.franz.bilibili.bean.rank.RankBean
import com.franz.bilibili.tools.DateTransform
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.ui.theme.white
import com.franz.bilibili.widget.ItemRow
import com.franz.bilibili.widget.doubleFormat
import com.franz.bilibili.widget.videoAuthor
import dagger.hilt.android.lifecycle.HiltViewModel

@Composable
fun RankSubPage(
    index:Int,
    viewModel: RankViewModel = hiltViewModel(),
    beans: List<RankBean> = viewModel.rank.value.beans,
    onClickItem:(BilibiliID)->Unit
){
    val state = remember { mutableStateOf(true)}
    if (state.value){
        state.value = false
        viewModel.onEvent(index)
    }
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.spacedBy(6.dp),
    ){
        items(beans.size){
            RankItem(bean = beans[it], onClickItem = onClickItem)
        }
    }
}

@Composable
fun RankItem(
    bean: RankBean,
    imageHeight: Dp = 100.dp,
    onClickItem:(BilibiliID)->Unit
){
    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .height(imageHeight)
            .background(BilibiliTheme.colors.backgroundTop, shape = RoundedCornerShape(4.dp))
            .clickable { onClickItem(BilibiliID(aid = bean.aid, cid = bean.cid)) }
    ) {
        val (imageRef,viewRef,titleRef,authorRef,durationRef,replayRef,subTitleRef) = createRefs()
        /**
         * 封面*/
        AsyncImage(
            model = bean.pic,
            contentDescription = bean.owner.name,
            contentScale = ContentScale.Crop,
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .fillMaxHeight()
                .clip(RoundedCornerShape(4.dp))
                .aspectRatio(1.5f)//aspectRatio:width/height
                .constrainAs(imageRef) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }
        )

        /**
         * 视频标题*/
        Text(
            text = bean.title,
            color = BilibiliTheme.colors.textTitle,
            style = MaterialTheme.typography.caption,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Start,
            modifier = Modifier
                .height(40.dp)
                .constrainAs(titleRef) {
                    start.linkTo(imageRef.end, 5.dp)
                    end.linkTo(parent.end, 5.dp)
                    top.linkTo(imageRef.top, 2.dp)
                    width = Dimension.fillToConstraints
                }
        )

        /**
         * 视频时长*/
        Text(
            text = doubleFormat(decimal = 2, number = bean.duration / 60.0),
            color = white,
            style = MaterialTheme.typography.overline,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .background(
                    color = Color.Black.copy(alpha = 0.4f),
                    shape = RoundedCornerShape(2.dp)
                )
                .padding(2.dp)
                .constrainAs(durationRef) {
                    top.linkTo(imageRef.bottom, ((-20).dp))
                    end.linkTo(imageRef.end, 6.dp)
                }
        )

        /**
         * 作者*/
        videoAuthor(
            bean.owner.name,
            modifier = Modifier
                .constrainAs(authorRef) {
                    bottom.linkTo(viewRef.top,2.dp)
                    start.linkTo(viewRef.start,(-5).dp)
                }
        )

        /**
         * 观看人数*/
        ItemRow(
            number = bean.stat.view,
            icon = R.drawable.icon_view,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(viewRef){
                bottom.linkTo(imageRef.bottom,2.dp)
                start.linkTo(imageRef.end,5.dp)
            }
        )

        /**
         * 评论人数*/
        ItemRow(
            number = bean.stat.reply,
            icon = R.drawable.icon_review,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(replayRef){
                top.linkTo(viewRef.top)
                bottom.linkTo(viewRef.bottom)
                start.linkTo(viewRef.end)
                end.linkTo(subTitleRef.start)
            }
        )
        /**
         * 弹幕数量*/
        ItemRow(
            number = bean.stat.danmaku,
            icon = R.drawable.icon_subtitle,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(subTitleRef){
                top.linkTo(viewRef.top)
                bottom.linkTo(viewRef.bottom)
                end.linkTo(parent.end,5.dp)
            }
        )
    }
}