package com.franz.bilibili.nav.rank

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.franz.bilibili.bean.rank.RankBean
import com.franz.bilibili.network.OfficialService
import com.franz.bilibili.param.rankRidMap
import com.franz.bilibili.tools.GsonFormat
import com.google.gson.JsonObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class RankViewModel @Inject constructor(private val service: OfficialService) :ViewModel() {

    private val _rank = mutableStateOf(RankState())
    val rank:State<RankState> = _rank

    init {
        viewModelScope.launch {
            getRank(0)
        }
    }

    fun onEvent(index: Int){
        val rid = rankRidMap[index]
        if (rid != null){
           viewModelScope.launch {
               getRank(rid)
           }
        }
    }

    private fun getRank(rid:Int){
        service.getRank(rid).enqueue(object: Callback<JsonObject>{
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val body = response.body().toString()
                if (body.isNotBlank()){
                    val jsonObject = JSONObject(body)
                    val code = jsonObject.getInt("code")
                    if (code == 0){
                        val rankJson = jsonObject.getJSONObject("data").getJSONArray("list")
                        val rankBean = GsonFormat.fromListJson(rankJson.toString(),RankBean::class.java)
                        _rank.value = rank.value.copy(
                            rid = rid,
                            beans = rankBean
                        )
                    }
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.d("RankViewModel","error message=${t.message}")
            }
        })
    }
}