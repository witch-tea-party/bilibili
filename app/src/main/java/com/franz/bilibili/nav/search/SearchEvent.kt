package com.franz.bilibili.nav.search

sealed class SearchEvent{
    data class ChangeKey(val key:String): SearchEvent()
    data class Search(val key: String): SearchEvent()
    object HotExpand: SearchEvent()
    object HistoryExpand: SearchEvent()
    object ClearAll: SearchEvent()
}
