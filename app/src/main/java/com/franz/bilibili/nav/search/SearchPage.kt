package com.franz.bilibili.nav.search

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateIntAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBackIos
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.*
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.franz.bilibili.R
import com.franz.bilibili.bean.HotSearchBean
import com.franz.bilibili.bean.SHistoryBean
import com.franz.bilibili.bean.SearchSuggestBean
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.widget.CustomAlertDialog
import com.franz.bilibili.widget.CustomTextField
import com.franz.bilibili.widget.RichText
import com.google.accompanist.insets.statusBarsPadding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SearchPage(
    modifier: Modifier = Modifier,
    scaffoldState: ScaffoldState = rememberScaffoldState(),
    scope: CoroutineScope = rememberCoroutineScope(),
    viewModel: SearchViewModel = hiltViewModel(),
    onSearch:(EnterSearchState)->Unit,
){
    val key = viewModel.searchKey.value
    val hots = viewModel.hotSearch.value
    val histories = viewModel.historySearch.value
    val suggests = viewModel.searchSuggest.value

    val clearHistoryFlag = remember { mutableStateOf(false) }

    LaunchedEffect(scaffoldState.snackbarHostState){
        viewModel.eventFlow.collectLatest {
            when(it){
                is SearchUIEvent.InsertSuccess -> {
                   // scaffoldState.snackbarHostState.showSnackbar("搜索成功!")
                    onSearch(EnterSearchState(text = it.key, hint = it.hint))
                }
                is SearchUIEvent.InsertFailed -> {
                    scaffoldState.snackbarHostState.showSnackbar(it.message)
                }

                is SearchUIEvent.ClearAll-> {
                    scaffoldState.snackbarHostState.showSnackbar("清空成功!")
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        snackbarHost = {
            SnackbarHost(hostState = it){ data->
                Snackbar(
                    snackbarData = data,
                    modifier = Modifier.padding(bottom = 100.dp)
                )
            }
        }
    ) {
        Box(
            modifier = modifier
                .fillMaxSize()
                .background(
                    Brush.horizontalGradient(
                        listOf(
                            BilibiliTheme.colors.loginStart,
                            BilibiliTheme.colors.loginEnd
                        )
                    )
                )
                .statusBarsPadding()
                .padding(10.dp)
        )
        {
            Column(
                modifier = Modifier.fillMaxSize()
            ) {
                /**
                 * 搜索栏*/
                SearchBar(
                    text = key.text,
                    hint = key.hint,
                    onValueChange = {
                        viewModel.onEvent(SearchEvent.ChangeKey(it))
                    },
                    onSearch = {
                        viewModel.onEvent(SearchEvent.Search(key.text))
                    }
                )

                Spacer(modifier = Modifier.height(10.dp))

                /**
                 * 搜索热词*/
                HotSearchList(
                    hots = hots.hots,
                    isExpand = hots.isExpand,
                    size = hots.size,
                    onClickHot = {
                        viewModel.onEvent(SearchEvent.Search(it))
                    },
                    onExpand = {
                        viewModel.onEvent(SearchEvent.HotExpand)
                    }
                )

                Spacer(modifier = Modifier.height(20.dp))

                /**
                 * 搜索记录*/
                HistorySearchList(
                    histories = histories.histories,
                    isExpand = histories.isExpand,
                    size = histories.size,
                    visibility = histories.visibility,
                    modifier = Modifier.weight(1f),
                    onClickHistory = {
                        viewModel.onEvent(SearchEvent.Search(it))
                    },
                    onExpand = {
                        viewModel.onEvent(SearchEvent.HistoryExpand)
                    },
                    onDelete = {
                        //清空历史搜索记录
                        //通过有状态的变量榜单alert dialog
                        //改变其显示与关闭
                        clearHistoryFlag.value = true
                    }
                )
            }

            /**
             * 搜索建议词
             * 通过box覆盖上述内容*/
            AnimatedVisibility(
                visible = suggests.visibility,
                modifier = Modifier
                    .padding(top = 40.dp)
                    .fillMaxWidth()
                    .wrapContentHeight()
            ) {
                SuggestionSearchList(
                    key = key.text,
                    suggests = suggests.beans,
                    onClickSuggest = {
                        viewModel.onEvent(SearchEvent.Search(it))
                    })
            }

            CustomAlertDialog(
                visibility = clearHistoryFlag.value,
                title = "系统提示",
                content = "删除全部搜索历史?",
                confirmText = "删除",
                cancelText = "取消",
                onConfirm = {
                    scope.launch {
                        clearHistoryFlag.value = false
                        viewModel.onEvent(SearchEvent.ClearAll)
                        scaffoldState.snackbarHostState.showSnackbar("清除记录成功!")
                    }
                },
                onCancel = {
                    scope.launch {
                        clearHistoryFlag.value = false
                        scaffoldState.snackbarHostState.showSnackbar("取消删除记录!")
                    }
                },
                onDismiss = {
                    //点击dialog外的区域
                    scope.launch {
                        clearHistoryFlag.value = false
                        scaffoldState.snackbarHostState.showSnackbar("取消删除记录!")
                    }
                }
            )
        }
    }
}

@Composable
fun SearchBar(
    text: String = "",
    hint: String = "野原新之助",
    showBack:Boolean = false,
    onValueChange: (String) -> Unit,
    onSearch: ()->Unit,
    onBack:()->Unit = {}
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
    ) {
        AnimatedVisibility(visible = showBack) {
            Icon(
                imageVector = Icons.Default.ArrowBackIos,
                contentDescription = "back",
                tint = BilibiliTheme.colors.textTitle,
                modifier = Modifier
                    .padding(end = 5.dp)
                    .size(20.dp)
                    .clickable { onBack() }
            )
        }
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .weight(1f)
                .background(
                    color = BilibiliTheme.colors.searchBar,
                    shape = RoundedCornerShape(10.dp)
                )
                .padding(5.dp)
        ) {
            Icon(
                imageVector = Icons.Default.Search,
                contentDescription = "Search",
                tint = BilibiliTheme.colors.defaultIcon,
                modifier = Modifier.size(20.dp)
            )
            Spacer(modifier = Modifier.width(5.dp))
            CustomTextField(
                value = text,
                onValueChange = onValueChange,
                singleLine = true,
                textStyle = TextStyle(
                    color = BilibiliTheme.colors.textContent,
                    fontSize = 12.sp
                ),
                placeholderText = hint,
                keyboardActions = KeyboardActions(
                    //执行软键盘搜索点击事件
                    onSearch = { onSearch() }
                )
            )
        }

        Text(
            text = "搜索",
            style = MaterialTheme.typography.button,
            color = BilibiliTheme.colors.textTitle,
            modifier = Modifier
                .clickable { onSearch() }
                .padding(start = 5.dp)
        )
    }
}

@Composable
private fun HotSearchList(
    hots:List<HotSearchBean>,
    hotTitle:String = "bilibili热搜",
    columns: GridCells = GridCells.Fixed(2),
    modifier: Modifier = Modifier,
    isExpand: Boolean,
    size: Int,
    onClickHot: (String)->Unit,
    onExpand: ()->Unit
){
    LazyVerticalGrid(
        columns = columns,
        verticalArrangement = Arrangement.spacedBy(8.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp)
    ){
        item(span = {GridItemSpan(this.maxLineSpan)}){
            ExpandTitleBar(title = hotTitle, isExpand = isExpand, onExpand = onExpand)
            Spacer(modifier = Modifier.height(5.dp))
        }

        items(size){
            HotSearchItem(bean = hots[it], onClickHot = onClickHot)
        }
    }
}

@Composable
private fun HotSearchItem(
    bean: HotSearchBean,
    onClickHot: (String)->Unit
){
    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClickHot(bean.show_name) }
    ) {
        val (titleRef,iconRef) = createRefs()
        Text(
            text = bean.show_name,
            textAlign = TextAlign.Start,
            color = BilibiliTheme.colors.textTitle,
            fontSize = 14.sp,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.constrainAs(titleRef){
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                end.linkTo(iconRef.start)
                width = Dimension.fillToConstraints
            }
        )

        if (bean.icon != null){
            AsyncImage(
                model = bean.icon,
                contentDescription = "icon",
                modifier = Modifier
                    .size(16.dp)
                    .constrainAs(iconRef) {
                        top.linkTo(titleRef.top)
                        bottom.linkTo(titleRef.bottom)
                        start.linkTo(titleRef.end)
                        end.linkTo(parent.end)
                    }
            )
        }
    }
}


@Composable
private fun HistorySearchList(
    histories:List<SHistoryBean>,
    columns:GridCells = GridCells.Fixed(3),
    title: String = "搜索记录",
    isExpand: Boolean,
    visibility: Boolean,
    size: Int,
    modifier: Modifier = Modifier,
    onExpand: () -> Unit,
    onClickHistory: (String)->Unit,
    onDelete: () -> Unit
){
    LazyVerticalGrid(
        columns = columns,
        verticalArrangement = Arrangement.spacedBy(10.dp),
        horizontalArrangement = Arrangement.spacedBy(6.dp),
        modifier = modifier
    ){
        item(span = {GridItemSpan(this.maxLineSpan)}){
            AnimatedVisibility(visible = visibility) {
                ExpandTitleBar(
                    title = title,
                    isExpand = isExpand,
                    onExpand = onExpand
                )
                Spacer(modifier = Modifier.height(10.dp))
            }
        }

        items(size){
            LimitText(text = histories[it].key,onClickHistory = onClickHistory)
        }
        
        item(span = {GridItemSpan(this.maxLineSpan)}){
            Spacer(modifier = Modifier.height(15.dp))
            DeleteBar(visibility = visibility, onDelete = onDelete)
        }
    }
}

@Composable
private fun LimitText(
    text: String,
    onClickHistory: (String)->Unit
){
    val size = text.length
    if (size <= 8){
        Text(
            text = text,
            color = BilibiliTheme.colors.textTitle,
            fontSize = 12.sp,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxSize()
                .background(
                    color = BilibiliTheme.colors.searchBar,
                    shape = RoundedCornerShape(4.dp)
                )
                .padding(4.dp)
                .clickable { onClickHistory(text) }
        )
    }else{
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxSize()
                .background(
                    color = BilibiliTheme.colors.searchBar,
                    shape = RoundedCornerShape(4.dp)
                )
                .padding(4.dp)
                .clickable { onClickHistory(text) }
        ) {
            Text(
                text = text.substring(0,8),
                color = BilibiliTheme.colors.textTitle,
                fontSize = 12.sp,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                text = "...",
                color = BilibiliTheme.colors.textTitle,
                fontSize = 12.sp
            )
        }
    }
}

@Composable
private fun SuggestionSearchList(
    key:String,
    suggests: List<SearchSuggestBean>,
    modifier: Modifier = Modifier,
    onClickSuggest: (String) -> Unit
){
    LazyColumn(modifier = modifier
        .fillMaxWidth()
        .wrapContentHeight()
        .background(color = BilibiliTheme.colors.backgroundTop, shape = RoundedCornerShape(6.dp))
    ){
        items(suggests.size){
            SuggestionSearchItem(
                key,
                bean = suggests[it],
                onClickSuggest = onClickSuggest
            )
            if (it < suggests.size-1){
                Divider()
            }
        }
    }
}

@Composable
private fun SuggestionSearchItem(
    key:String,
    bean: SearchSuggestBean,
    onClickSuggest: (String) -> Unit
){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
            .clickable { onClickSuggest(bean.name) },
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            imageVector = Icons.Default.Search,
            contentDescription = "search",
            tint = BilibiliTheme.colors.defaultIcon,
            modifier = Modifier.size(16.dp)
        )

        Spacer(modifier = Modifier.width(10.dp))

        /**
         * 搜索栏富文本显示
         * 搜索内容存在于搜索建议内容之中的高亮显示，不存在则普通·显示*/
        RichText(
            selectColor = BilibiliTheme.colors.highlightColor,
            unselectColor = BilibiliTheme.colors.textTitle,
            searchValue = key,
            matchValue = bean.name,
            fontSize = 12.sp
        )
    }
}



@Composable
private fun ExpandTitleBar(
    title:String,
    isExpand: Boolean,
    onExpand: ()->Unit
){
    val icon by animateIntAsState(
        targetValue = if (isExpand){
            R.drawable.icon_expand_more
        }else{
            R.drawable.icon_expand_less
        }
    )

    val expand = if (isExpand) "折叠" else "展开"

    Row(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = title,
            fontSize = 14.sp,
            color = BilibiliTheme.colors.textTitle,
            fontWeight = FontWeight.Bold
        )

        Spacer(modifier = Modifier.weight(1f))

        Text(
            text = expand,
            fontSize = 12.sp,
            color = BilibiliTheme.colors.textTitle,
            modifier = Modifier.clickable { onExpand() }
        )
        Icon(
            painter = painterResource(id = icon),
            contentDescription = "expand",
            tint = BilibiliTheme.colors.textTitle,
            modifier = Modifier.size(16.dp)
        )
    }
}

@Composable
fun DeleteBar(
    title:String = "清空历史记录",
    visibility: Boolean,
    onDelete: ()->Unit
){
    AnimatedVisibility(visible = visibility) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clickable { onDelete() },
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                imageVector = Icons.Default.Delete,
                contentDescription = "delete history",
                tint = BilibiliTheme.colors.defaultIcon,
                modifier = Modifier.size(16.dp)
            )
            Spacer(modifier = Modifier.width(4.dp))
            Text(
                text = title,
                color = BilibiliTheme.colors.defaultIcon,
                fontSize = 12.sp
            )
        }
    }
}