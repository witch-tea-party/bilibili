package com.franz.bilibili.nav.search

import com.franz.bilibili.bean.HotSearchBean
import com.franz.bilibili.bean.SHistoryBean
import com.franz.bilibili.bean.SearchSuggestBean

data class EnterSearchState(
    val text:String = "",
    val hint:String = "野原新之助"
)
data class SearchState(
    val beans:List<SearchSuggestBean> = emptyList(),
    val visibility: Boolean = false,
)

data class HotSearchState(
    val hots:List<HotSearchBean> = emptyList(),
    val isExpand: Boolean = false,
    val size: Int = hots.size
)

data class HistorySearchState(
    val histories:List<SHistoryBean> = emptyList(),
    val isExpand: Boolean = false,
    val visibility: Boolean = true,
    val size: Int = histories.size
)
