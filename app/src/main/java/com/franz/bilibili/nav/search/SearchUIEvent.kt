package com.franz.bilibili.nav.search

sealed class SearchUIEvent{
    data class InsertFailed(val message: String):SearchUIEvent()
    data class InsertSuccess(val key:String,val hint:String):SearchUIEvent()
    data class OnFailed(val message: String):SearchUIEvent()
    object ClearAll:SearchUIEvent()
}
