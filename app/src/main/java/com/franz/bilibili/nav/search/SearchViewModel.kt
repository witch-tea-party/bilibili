package com.franz.bilibili.nav.search

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.franz.bilibili.bean.HotSearchBean
import com.franz.bilibili.bean.SHException
import com.franz.bilibili.bean.SHistoryBean
import com.franz.bilibili.bean.SearchSuggestBean
import com.franz.bilibili.network.OfficialService
import com.franz.bilibili.tools.GsonFormat
import com.franz.bilibili.usecase.shUseCase.SHUseCase
import com.google.gson.JsonObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val service: OfficialService,
    private val usecase: SHUseCase,
    private val savedStateHandle: SavedStateHandle
 ):ViewModel() {

    /**
     * 默认搜索关键词*/
    private val _searchKey = mutableStateOf(EnterSearchState())
    val searchKey:State<EnterSearchState> = _searchKey

    /**
     * 搜索建议词*/
    private val _searchSuggest = mutableStateOf(SearchState())
    val searchSuggest:State<SearchState> = _searchSuggest

    /**
     * 搜索热词*/
    private val hotSearchUrl = "https://app.bilibili.com/x/v2/search/trending/ranking"
    private val _hotSearch = mutableStateOf(HotSearchState())
    val hotSearch:State<HotSearchState> = _hotSearch

    /**
     * 历史搜索记录*/
    private val _historySearch = mutableStateOf(HistorySearchState())
    val historySearch: State<HistorySearchState> = _historySearch

    private var getNotesJob: Job? = null

    private val _eventFlow = MutableSharedFlow<SearchUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        /**
         * 获取从HomePage结点传过来搜索建议值*/
        viewModelScope.launch {
            savedStateHandle.get<String>("key")?.let {
                if (it.isNotBlank()){
                    _searchKey.value = searchKey.value.copy(
                        hint = it
                    )
                }
            }
            getHotSearch()
        }
        getHistories()
    }


    fun onEvent(event: SearchEvent){
        when(event){
            is SearchEvent.ChangeKey-> {
                //输入框字符变换
                viewModelScope.launch {
                    val key = event.key
                    _searchKey.value = searchKey.value.copy(
                        text = key
                    )
                    if (key.isNotBlank()){
                        getSearchList(key)
                    }else{
                        _searchSuggest.value = searchSuggest.value.copy(
                            visibility = false
                        )
                    }
                }
            }

            is SearchEvent.Search-> {
                //insert and navigation
                viewModelScope.launch {
                    try {
                        _searchKey.value = searchKey.value.copy(text = event.key)
                        usecase.insertSHistory(
                            SHistoryBean(
                                key = event.key,
                                time = System.currentTimeMillis()
                            )
                        )
                        _eventFlow.emit(SearchUIEvent.InsertSuccess(_searchKey.value.text,_searchKey.value.hint))
                    }catch (e: SHException){
                        _eventFlow.emit(SearchUIEvent.InsertFailed(e.message ?: "插入失败!"))
                    }
                }
            }

            is SearchEvent.ClearAll-> {
                //清空搜索历史记录
                viewModelScope.launch {
                    usecase.clearSHistory.invoke()
                    _historySearch.value = historySearch.value.copy(
                        visibility = false
                    )
                    _eventFlow.emit(SearchUIEvent.ClearAll)
                }
            }

            is SearchEvent.HotExpand-> {
                val flag = _hotSearch.value.isExpand
                _hotSearch.value = hotSearch.value.copy(
                    isExpand = !flag,
                    size = if (flag) _hotSearch.value.hots.size / 2 else _hotSearch.value.hots.size
                )
            }

            is SearchEvent.HistoryExpand-> {
                val flag = _historySearch.value.isExpand
                _historySearch.value = _historySearch.value.copy(
                    isExpand = !flag,
                    size = if (flag){
                        //less
                        if (_historySearch.value.histories.size <= 9) _historySearch.value.histories.size else 9
                    }else{
                        //more
                        _historySearch.value.histories.size
                    }
                )
            }
        }
    }

    /**
     因为搜索建议的接口返回结果如下，并不是数组形式，故需要一个个解析转为list类型
    {
    "0": {
    "value": "三体",
    "term": "三体",
    "ref": 0,
    "name": "三体",
    "spid": 5
    },
    "1": {
    "value": "三体电视剧",
    "term": "三体电视剧",
    "ref": 0,
    "name": "三体电视剧",
    "spid": 5
         }
    }
     */
    private fun getSearchList(key: String){
        service.getSearchSuggest(key).enqueue(object: Callback<JsonObject>{
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val body = response.body().toString()
                if (body.isNotBlank()) {
                    val jsonObject = JSONObject(body)
                    val list: MutableList<SearchSuggestBean> = ArrayList()
                    _searchSuggest.value = searchSuggest.value.copy(
                        beans = list.apply {
                            repeat(10) { index ->
                                if (jsonObject.has(index.toString())){
                                    val json = jsonObject.getJSONObject(index.toString())
                                    val bean = GsonFormat.fromJson(json.toString(), SearchSuggestBean::class.java)
                                    add(bean)
                                }
                            }
                        },
                        visibility = true
                    )
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                viewModelScope.launch {
                    _eventFlow.emit(SearchUIEvent.OnFailed(t.message ?: "网络请求失败"))
                    Log.d("SearchViewModel","error message=${t.message}")
                }
            }
        })
    }

    /**
     * 获取热搜关键词列表*/
    private fun getHotSearch(){
        service.getHotSearch(hotSearchUrl).enqueue(object: Callback<JsonObject>{
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val body = response.body().toString()
                if (body.isNotBlank()){
                    val jsonObject = JSONObject(body)
                    val code = jsonObject.getInt("code")
                    if (code == 0){
                        val hotsJson = jsonObject.getJSONObject("data").getJSONArray("list")
                        val hots = GsonFormat.fromListJson(hotsJson.toString(),HotSearchBean::class.java)
                        _hotSearch.value = hotSearch.value.copy(
                            hots = hots,
                            size = hots.size / 2,
                            isExpand = false
                        )
                    }
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                viewModelScope.launch {
                    _eventFlow.emit(SearchUIEvent.OnFailed(t.message ?: "网络请求失败"))
                    Log.d("SearchViewModel","error message=${t.message}")
                }
            }
        })
    }

    /**
     * 此处size需要根据isExpand状态进行判断
     * 因为当为true时，插入数据库，然后flow进行接收，会重新返回折叠状态*/
    private fun getHistories(){
        getNotesJob?.cancel()
        getNotesJob = usecase.getSHistories().onEach { histories->
            _historySearch.value = historySearch.value.copy(
                histories = histories,
                isExpand = _historySearch.value.isExpand,
                size = if (_historySearch.value.isExpand) {
                    histories.size
                } else {
                    if (histories.size <= 9){
                        histories.size
                    }else{
                        9
                    }
                },
                visibility = histories.isNotEmpty()
            )
        }.launchIn(viewModelScope)
    }
}