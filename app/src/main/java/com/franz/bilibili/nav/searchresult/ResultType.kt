package com.franz.bilibili.nav.searchresult

sealed class ResultType(val type:String){
    object Activity:ResultType("activity")
    object BilibiliUser:ResultType("bili_user")
    object Game:ResultType("web_game")
    object MediaBang:ResultType("media_bangumi")
    object MediaFt:ResultType("media_ft")
    object Video:ResultType("video")
}
