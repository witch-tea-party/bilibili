package com.franz.bilibili.nav.searchresult

import com.franz.bilibili.bean.searchresult.activity.SearchActivity
import com.franz.bilibili.bean.searchresult.bilibili_user.BilibiliUser
import com.franz.bilibili.bean.searchresult.game.Game
import com.franz.bilibili.bean.searchresult.media_bangumi.MediaBangumi
import com.franz.bilibili.bean.searchresult.video.SearchVideo

data class KeyWordState(
    val text:String = "",
    val hint:String = ""
)

data class ResultListState(
    val activities:List<SearchActivity> = emptyList(),//活动
    val bilibili_user:List<BilibiliUser> = emptyList(),//官方用户
    val games:List<Game> = emptyList(),//游戏
    val media_bangumi:List<MediaBangumi> = emptyList(),//番剧
    val media_ft:List<MediaBangumi> = emptyList(),//影视
    val searchVideos:List<SearchVideo> = emptyList()//视频
)

data class PagesState(val titles:List<String> = arrayListOf())

data class TotalPageState(
    val showFilter:Boolean = false,
    val filterType: TotalFilterType = TotalFilterType.DefaultSort
)
