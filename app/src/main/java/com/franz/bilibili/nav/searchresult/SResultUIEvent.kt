package com.franz.bilibili.nav.searchresult

sealed class SResultUIEvent{
    data class CodeError(val message:String): SResultUIEvent()
    data class BodyEmpty(val message:String): SResultUIEvent()
    data class OnFailure(val message:String): SResultUIEvent()
}
