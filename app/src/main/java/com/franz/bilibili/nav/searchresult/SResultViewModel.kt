package com.franz.bilibili.nav.searchresult

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.franz.bilibili.APP
import com.franz.bilibili.bean.searchresult.ResultData
import com.franz.bilibili.bean.searchresult.activity.SearchActivity
import com.franz.bilibili.bean.searchresult.bilibili_user.BilibiliUser
import com.franz.bilibili.bean.searchresult.game.Game
import com.franz.bilibili.bean.searchresult.media_bangumi.MediaBangumi
import com.franz.bilibili.bean.searchresult.video.SearchVideo
import com.franz.bilibili.network.OfficialService
import com.franz.bilibili.param.CookieKey
import com.franz.bilibili.tools.GsonFormat
import com.franz.bilibili.tools.SPUtil
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class SResultViewModel @Inject constructor(
    private val service: OfficialService,
    savedStateHandle: SavedStateHandle = SavedStateHandle()
    ):ViewModel() {

    /**
     * 搜索关键词*/
    private val _keyword = mutableStateOf(KeyWordState())
    val keyword:State<KeyWordState> = _keyword

    /**
     * 搜索结果*/
    private val _results = mutableStateOf(ResultListState())
    val results:State<ResultListState> = _results

    /**
     * page的标题数据集合*/
    private val _pages = mutableStateOf(PagesState())
    val pages:State<PagesState> = _pages

    private val _totalPageState = mutableStateOf(TotalPageState())
    val totalPageState:State<TotalPageState> = _totalPageState

    private val _eventFlow = MutableSharedFlow<SResultUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        viewModelScope.launch {
            savedStateHandle.get<String>("key")?.let {
                if (it.isNotBlank()) {
                    _keyword.value = keyword.value.copy(text = it)
                }
            }

            savedStateHandle.get<String>("hint")?.let {
                if (it.isNotBlank()) {
                    _keyword.value = keyword.value.copy(hint = it)
                }
            }

            if (_keyword.value.text.isNotBlank() && _keyword.value.hint.isNotBlank()){
                getSearchResult(_keyword.value.text)
            }
        }
    }

    fun onEvent(event: SearchResultEvent){
        when(event){
            is SearchResultEvent.Search-> {
                viewModelScope.launch {
                    if (_keyword.value.text.isNotEmpty()){
                        getSearchResult(_keyword.value.text)
                    }else{
                        getSearchResult(_keyword.value.hint)
                    }
                }
            }

            is SearchResultEvent.ChangeSearchKey->{
                _keyword.value = keyword.value.copy(
                    text = event.key
                )
            }
            is SearchResultEvent.ShowFilter->{
                /**
                 * 修改综合page筛选组件是否显示*/
                _totalPageState.value = totalPageState.value.copy(
                    showFilter = !_totalPageState.value.showFilter
                )
            }
            is SearchResultEvent.ChangeFilterType->{
                /**
                 * 修改综合page筛选组件类型
                 * 根据所选类别更改排序顺序*/
                when(event.type){
                    is TotalFilterType.DefaultSort->{
                        /**
                         * 默认排序*/
                        _totalPageState.value = totalPageState.value.copy(
                            filterType = event.type
                        )
                    }
                    is TotalFilterType.OddSort->{
                        /**
                         * 非正常排序*/
                        _totalPageState.value = totalPageState.value.copy(
                            filterType = event.type
                        )
                    }
                    is TotalFilterType.PlaySort->{
                        /**
                         * 播放量排序*/
                        _totalPageState.value = totalPageState.value.copy(
                            filterType = event.type
                        )
                    }
                    is TotalFilterType.CommentSort->{
                        /**
                         * 评论数排序*/
                        _totalPageState.value = totalPageState.value.copy(
                            filterType = event.type
                        )
                    }
                    is TotalFilterType.NewUpdateSort->{
                        /**
                         * 更新时间排序排序*/
                        _totalPageState.value = totalPageState.value.copy(
                            filterType = event.type
                        )
                    }
                }
            }
        }
    }
    /**
     * 根据搜索关键字获取相关搜索结果
     * 返回的数据包含不同类型，例如：用户、番剧、影视等
     * 所以通过封闭类结合when进行逐个解析*/
    private fun getSearchResult(keyword:String){
        service.getSearchResult(keyword).enqueue(object: Callback<JsonObject>{
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val body = response.body().toString()
                if (body.isNotBlank() && body != "null") {
                    val jsonObject = JSONObject(body)
                    val code = jsonObject.getInt("code")
                    val pageList = arrayListOf<String>()
                    when(code){
                        0->{
                            //默认Page Title
                            pageList.add("综合")
                            val resultJson = jsonObject.getJSONObject("data").getJSONArray("result")
                            val resultList = GsonFormat.fromListJson(resultJson.toString(),ResultData::class.java)
                            repeat(resultList.size){
                                when(resultList[it].result_type){
                                    ResultType.Activity.type ->{
                                        if (resultList[it].data.isNotEmpty()){
                                            val dataJson = Gson().toJson(resultList[it].data)
                                            val data = GsonFormat.fromListJson(dataJson.toString(),SearchActivity::class.java)
                                            _results.value = results.value.copy(
                                                activities = data
                                            )
                                            pageList.add("活动")
                                        }else{
                                            _results.value = results.value.copy(
                                                activities = emptyList()
                                            )
                                        }
                                    }
                                    ResultType.BilibiliUser.type->{
                                        if (resultList[it].data.isNotEmpty()){
                                            val dataJson = Gson().toJson(resultList[it].data)
                                            val data = GsonFormat.fromListJson(dataJson.toString(),BilibiliUser::class.java)
                                            _results.value = results.value.copy(
                                                bilibili_user = data
                                            )
                                            pageList.add("官方用户")
                                        }else{
                                            _results.value = results.value.copy(
                                                bilibili_user = emptyList()
                                            )
                                        }
                                    }
                                    ResultType.Game.type->{
                                        if (resultList[it].data.isNotEmpty()){
                                            val dataJson = Gson().toJson(resultList[it].data)
                                            val data = GsonFormat.fromListJson(dataJson.toString(),Game::class.java)
                                            _results.value = results.value.copy(
                                                games = data
                                            )
                                            pageList.add("游戏")
                                        }else{
                                            _results.value = results.value.copy(
                                                games = emptyList()
                                            )
                                        }
                                    }
                                    ResultType.MediaBang.type->{
                                        if (resultList[it].data.isNotEmpty()){
                                            val dataJson = Gson().toJson(resultList[it].data)
                                            val data = GsonFormat.fromListJson(dataJson.toString(),MediaBangumi::class.java)
                                            _results.value = results.value.copy(
                                                media_bangumi = data
                                            )
                                            pageList.add("番剧")
                                        }else{
                                            _results.value = results.value.copy(
                                                media_bangumi = emptyList()
                                            )
                                        }
                                    }
                                    ResultType.MediaFt.type->{
                                        if (resultList[it].data.isNotEmpty()){
                                            val dataJson = Gson().toJson(resultList[it].data)
                                            val data = GsonFormat.fromListJson(dataJson.toString(),MediaBangumi::class.java)
                                            _results.value = results.value.copy(
                                                media_ft = data
                                            )
                                            pageList.add("影视")
                                        }else{
                                            _results.value = results.value.copy(
                                                media_ft = emptyList()
                                            )
                                        }
                                    }
                                    ResultType.Video.type->{
                                        if (resultList[it].data.isNotEmpty()){
                                            val dataJson = Gson().toJson(resultList[it].data)
                                            val data = GsonFormat.fromListJson(dataJson.toString(),SearchVideo::class.java)
                                            _results.value = results.value.copy(
                                                searchVideos = data
                                            )
                                            pageList.add("视频")
                                        }else{
                                            _results.value = results.value.copy(
                                                searchVideos = emptyList()
                                            )
                                        }
                                    }
                                }
                            }
                        }
                        412->{
                            viewModelScope.launch {
                                _eventFlow.emit(SResultUIEvent.CodeError("code为412"))
                                SPUtil.putValue(APP.context, CookieKey,"")
                            }
                        }
                        else->{
                            viewModelScope.launch {
                                _eventFlow.emit(SResultUIEvent.CodeError("未知错误码"))
                                SPUtil.putValue(APP.context, CookieKey,"")
                            }
                        }
                    }
                    /**
                     * 获取需要绘制的Tab，通过返回的json数据进行动态绘制Tab和Page*/
                    if (pageList.isNotEmpty()){
                        _pages.value = pages.value.copy(
                            titles = pageList
                        )
                    }
                }else{
                    //如果数据为空或者返回code为412则可能请求被拦截，cookie失效或者ip被服务端封控
                    viewModelScope.launch {
                        _eventFlow.emit(SResultUIEvent.BodyEmpty("body为空"))
                        SPUtil.putValue(APP.context, CookieKey,"")
                    }
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                viewModelScope.launch {
                    _eventFlow.emit(SResultUIEvent.OnFailure(t.message ?: "网络请求失败"))
                }
            }
        })
    }
}