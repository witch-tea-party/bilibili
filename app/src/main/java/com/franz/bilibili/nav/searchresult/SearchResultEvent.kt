package com.franz.bilibili.nav.searchresult


sealed class SearchResultEvent{
    object Search:SearchResultEvent()
    object ShowFilter: SearchResultEvent()
    data class ChangeSearchKey(val key:String):SearchResultEvent()
    data class ChangeFilterType(val type: TotalFilterType): SearchResultEvent()
}

sealed class TotalFilterType{
    object DefaultSort: TotalFilterType()
    object OddSort: TotalFilterType()
    object NewUpdateSort: TotalFilterType()
    object PlaySort: TotalFilterType()
    object CommentSort: TotalFilterType()
}
