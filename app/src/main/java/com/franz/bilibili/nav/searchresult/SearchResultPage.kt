package com.franz.bilibili.nav.searchresult

import android.annotation.SuppressLint
import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.franz.bilibili.bean.BilibiliID
import com.franz.bilibili.nav.home.customIndicatorOffset
import com.franz.bilibili.nav.search.SearchBar
import com.franz.bilibili.nav.searchresult.result.activity.ResultActivityPage
import com.franz.bilibili.nav.searchresult.result.game.ResultGamePage
import com.franz.bilibili.nav.searchresult.result.media.ResultMediaPage
import com.franz.bilibili.nav.searchresult.result.official.ResultOfficialPage
import com.franz.bilibili.nav.searchresult.result.total.ResultTotalPage
import com.franz.bilibili.nav.searchresult.result.video.ResultVideoPage
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.google.accompanist.insets.statusBarsPadding
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@OptIn(ExperimentalPagerApi::class)
@Composable
fun SearchResultPage(
    modifier: Modifier = Modifier,
    viewModel: SResultViewModel = hiltViewModel(),
    onNavigation:()->Unit,
    onClickItem:(BilibiliID)->Unit
){
    val key = viewModel.keyword.value
    val pages = viewModel.pages.value
    val results = viewModel.results.value
    val totalPage = viewModel.totalPageState.value

    val pageState = rememberPagerState(initialPage = 0)
    val scope = rememberCoroutineScope()
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = scaffoldState.snackbarHostState){
        viewModel.eventFlow.collectLatest {
            when(it){
                is SResultUIEvent.CodeError->{
                    scaffoldState.snackbarHostState.showSnackbar(it.message)
                }
                is SResultUIEvent.BodyEmpty->{
                    scaffoldState.snackbarHostState.showSnackbar(it.message)
                }
                is SResultUIEvent.OnFailure->{
                    scaffoldState.snackbarHostState.showSnackbar(it.message)
                }
            }
        }
    }

    Scaffold(scaffoldState = scaffoldState) {
        Column(
            modifier = modifier
                .fillMaxSize()
                .background(
                    Brush.horizontalGradient(
                        listOf(
                            BilibiliTheme.colors.loginStart,
                            BilibiliTheme.colors.loginEnd
                        )
                    )
                )
                .statusBarsPadding()
                .padding(10.dp)
                .navigationBarsPadding()
        ) {
            SearchBar(
                text = key.text,
                hint = key.hint,
                showBack = true,
                onValueChange = {
                    viewModel.onEvent(SearchResultEvent.ChangeSearchKey(it))
                },
                onSearch = {
                    viewModel.onEvent(SearchResultEvent.Search)
                },
                onBack = {
                    onNavigation()
                }
            )
            /**
             * Tab*/
            if (pages.titles.isNotEmpty()){
                ScrollableTabRow(
                    selectedTabIndex = pageState.currentPage,
                    backgroundColor = Color.Transparent,
                    indicator = { pos ->
                        TabRowDefaults.Indicator(
                            color = BilibiliTheme.colors.tabSelect,
                            modifier = Modifier.customIndicatorOffset(
                                pagerState = pageState,
                                tabPositions = pos,
                                32.dp
                            )
                        )
                    },
                    divider = {
                        TabRowDefaults.Divider(color = Color.Transparent)
                    },
                    modifier = Modifier.wrapContentWidth(),
                    edgePadding = 0.dp
                ) {
                    pages.titles.forEachIndexed { index, title ->
                        Tab(
                            text = { Text(title) },
                            selected = pageState.currentPage == index,
                            onClick = {
                                scope.launch {
                                    pageState.scrollToPage(index)
                                }
                            },
                            enabled = true,
                            selectedContentColor = BilibiliTheme.colors.tabSelect,
                            unselectedContentColor = BilibiliTheme.colors.tabUnselect,
                        )
                    }
                }

                HorizontalPager(
                    count = pages.titles.size,
                    state = pageState,
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(5.dp)
                ) { pos ->
                    when(pages.titles[pageState.currentPage]){
                        "综合" -> {
                            ResultTotalPage(
                                resultListState = results,
                                isShowFilter = totalPage.showFilter,
                                filterType = totalPage.filterType,
                                onChangeType = {
                                    viewModel.onEvent(SearchResultEvent.ChangeFilterType(it))
                                },
                                onShowFilter = {
                                    viewModel.onEvent(SearchResultEvent.ShowFilter)
                                },
                                onClickItem = onClickItem
                            )
                        }
                        "活动" -> {
                            ResultActivityPage(activities = results.activities, enableScroll = true)
                        }
                        "官方用户" ->{
                            ResultOfficialPage(users = results.bilibili_user, enableScroll = true)
                        }
                        "游戏" ->{
                            ResultGamePage(games = results.games, enableScroll = true)
                        }
                        "番剧" ->{
                            ResultMediaPage(videos = results.media_bangumi,"追番", enableScroll = true)
                        }
                        "影视" ->{
                            ResultMediaPage(videos = results.media_ft,"追剧", enableScroll = true)
                        }
                        "视频" ->{
                            ResultVideoPage(videos = results.searchVideos, onClickItem = onClickItem, enableScroll = true)
                        }
                    }
                }
            }
        }
    }
}