package com.franz.bilibili.nav.searchresult.result.activity

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExtendedFloatingActionButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.AsyncImage
import com.franz.bilibili.R
import com.franz.bilibili.bean.searchresult.activity.SearchActivity
import com.franz.bilibili.ui.theme.BilibiliTheme

@Composable
fun ResultActivityPage(
    activities:List<SearchActivity>,
    modifier: Modifier = Modifier,
    screenHeight:Int = LocalConfiguration.current.screenHeightDp,
    enableScroll:Boolean = true
){
    Box(modifier = modifier.fillMaxSize()){
        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(10.dp),
            userScrollEnabled = enableScroll,
            modifier = Modifier
                .fillMaxWidth()
                .heightIn(max = screenHeight.dp)
                .align(Alignment.TopStart)
        ) {
            items(activities.size) {
                ResultActivityItem(bean = activities[it])
            }
        }
    }
}

@Composable
private fun ResultActivityItem(
    bean: SearchActivity,
    imageHeight: Dp = 100.dp
){
    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .height(imageHeight)
            .background(BilibiliTheme.colors.backgroundTop, shape = RoundedCornerShape(4.dp))
    ) {
        val (imageRef,titleRef,contentRef,detailRef) = createRefs()
        /**
         * 封面*/
        AsyncImage(
            model = bean.cover,
            contentDescription = bean.title,
            contentScale = ContentScale.Crop,
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .fillMaxHeight()
                .clip(RoundedCornerShape(4.dp))
                .aspectRatio(1.7f)//aspectRatio:width/height
                .constrainAs(imageRef) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }
        )

        /**
         * 活动标题*/
        Text(
            text = bean.title,
            color = BilibiliTheme.colors.textTitle,
            style = MaterialTheme.typography.subtitle2,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .constrainAs(titleRef) {
                    start.linkTo(imageRef.end, 5.dp)
                    end.linkTo(parent.end, 5.dp)
                    top.linkTo(imageRef.top, 5.dp)
                    width = Dimension.fillToConstraints
                }
        )

        /**
         * 活动描述*/
        Text(
            text = bean.desc,
            color = BilibiliTheme.colors.textContent,
            fontSize = 10.sp,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.constrainAs(contentRef) {
                    top.linkTo(titleRef.bottom,5.dp)
                    bottom.linkTo(detailRef.top)
                    start.linkTo(titleRef.start)
                    end.linkTo(titleRef.end)
                    width = Dimension.fillToConstraints
                    height = Dimension.fillToConstraints
                }
        )
        
        ExtendedFloatingActionButton(
            backgroundColor = BilibiliTheme.colors.highlightColor,
            shape = RoundedCornerShape(6.dp),
            text = { Text(text = "查看详情", color = Color.White, fontSize = 12.sp) },
            onClick = {  },
            modifier = Modifier
                .height(30.dp)
                .constrainAs(detailRef) {
                    start.linkTo(titleRef.start)
                    bottom.linkTo(parent.bottom, 5.dp)
                }
        )

    }
}