package com.franz.bilibili.nav.searchresult.result.game

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import coil.compose.AsyncImage
import com.franz.bilibili.R
import com.franz.bilibili.bean.searchresult.game.Game
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.widget.calculation

@Composable
fun ResultGamePage(
    games:List<Game>,
    modifier: Modifier = Modifier,
    screenHeight:Int = LocalConfiguration.current.screenHeightDp,
    enableScroll:Boolean = true
){
    Box(modifier = modifier.fillMaxSize()){
        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(10.dp),
            userScrollEnabled = enableScroll,
            modifier = Modifier
                .fillMaxWidth()
                .heightIn(max = screenHeight.dp)
                .align(Alignment.TopStart)
        ) {
            items(games.size) {
                ResultGameItem(bean = games[it])
            }
        }
    }
}

@Composable
private fun ResultGameItem(
    bean: Game
){
    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .background(BilibiliTheme.colors.backgroundTop, shape = RoundedCornerShape(4.dp))
            .padding(5.dp)
    ) {
        val (imageRef,nameRef,typeRef,tagRef,desRef,scoreRef,commentRef,officialRef) = createRefs()

        /**
         * 游戏图标*/
        AsyncImage(
            model = bean.game_icon,
            contentDescription = bean.game_name,
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .constrainAs(imageRef) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start,10.dp)
                }
        )

        /**
         * 游戏类别*/
        Text(
            text = "游戏",
            color = BilibiliTheme.colors.textContent,
            fontSize = 12.sp,
            modifier = Modifier
                .border(1.dp, BilibiliTheme.colors.textContent, RoundedCornerShape(4.dp))
                .padding(1.dp)
                .constrainAs(typeRef) {
                    start.linkTo(imageRef.end, 5.dp)
                    top.linkTo(imageRef.top)
                }
        )


        /**
         * 游戏名称*/
        Text(
            text = bean.game_name,
            color = BilibiliTheme.colors.textTitle,
            fontSize = 14.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.constrainAs(nameRef) {
                    start.linkTo(typeRef.end,5.dp)
                    top.linkTo(typeRef.top)
                }
        )

        /**
         * 游戏标签*/
        Text(
            text = bean.game_tags,
            color = BilibiliTheme.colors.textTitle,
            fontSize = 12.sp,
            modifier = Modifier.constrainAs(tagRef){
                top.linkTo(typeRef.bottom,5.dp)
                start.linkTo(typeRef.start)
            }
        )
        /**
         * 游戏简介*/
        Text(
            text = "简介:${bean.summary}",
            color = BilibiliTheme.colors.textContent,
            fontSize = 12.sp,
            modifier = Modifier.constrainAs(desRef){
                top.linkTo(tagRef.bottom,5.dp)
                start.linkTo(tagRef.start)
            }
        )
        /**
         * 游戏评分*/
        Text(
            text = "${bean.grade}分",
            color = BilibiliTheme.colors.highlightColor,
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.constrainAs(scoreRef){
                bottom.linkTo(officialRef.top,5.dp)
                start.linkTo(desRef.start)
            }
        )
        /**
         * 官方详情*/
        Text(
            text = "官方动态",
            color = BilibiliTheme.colors.bottomBar,
            fontSize = 14.sp,
            modifier = Modifier
                .background(BilibiliTheme.colors.highlightColor, RoundedCornerShape(4.dp))
                .padding(3.dp)
                .constrainAs(officialRef) {
                    bottom.linkTo(imageRef.bottom)
                    start.linkTo(scoreRef.start)
                }
        )
        /**
         * 游戏评论*/
        Text(
            text = "评论(${calculation(bean.comment_num)}条)",
            color = BilibiliTheme.colors.textTitle,
            fontSize = 14.sp,
            modifier = Modifier
                .border(1.dp, BilibiliTheme.colors.textContent, RoundedCornerShape(4.dp))
                .padding(3.dp)
                .constrainAs(commentRef) {
                    top.linkTo(officialRef.top)
                    start.linkTo(officialRef.end, 5.dp)
                }
        )
    }
}