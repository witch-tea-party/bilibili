package com.franz.bilibili.nav.searchresult.result.media

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.AsyncImage
import com.franz.bilibili.R
import com.franz.bilibili.bean.searchresult.media_bangumi.MediaBangumi
import com.franz.bilibili.tools.DateTransform
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.widget.calculation

@Composable
fun ResultMediaPage(
    videos:List<MediaBangumi>,
    name:String,
    modifier: Modifier = Modifier,
    screenHeight:Int = LocalConfiguration.current.screenHeightDp,
    enableScroll:Boolean = true
){
    Box(modifier = modifier.fillMaxSize()){
        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(6.dp),
            userScrollEnabled = enableScroll,
            modifier = Modifier
                .fillMaxWidth()
                .heightIn(max = screenHeight.dp)
                .align(Alignment.TopStart)
        ) {
            items(videos.size) {
                ResultMediaItem(bean = videos[it], name = name)
            }
        }
    }
}

@Composable
private fun ResultMediaItem(
    bean:MediaBangumi,
    name: String,
    itemHeight: Dp = 150.dp
){
    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .height(itemHeight)
            .background(BilibiliTheme.colors.backgroundTop, RoundedCornerShape(4.dp))
    ) {
        val (imgRef,typeRef,nameRef,infoRef,desRef,styleRef,scoreRef,fansRef,viewRef,followRef) = createRefs()
        /**
         * 封面*/
        AsyncImage(
            model = bean.cover,
            contentDescription = bean.cover,
            contentScale = ContentScale.Crop,
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .fillMaxHeight()
                .clip(RoundedCornerShape(4.dp))
                .aspectRatio(0.7f)
                .constrainAs(imgRef) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }
        )
        /**
         * 类别*/
        Text(
            text = bean.season_type_name,
            fontSize = 10.sp,
            color = BilibiliTheme.colors.bottomBar,
            modifier = Modifier
                .background(BilibiliTheme.colors.highlightColor, RoundedCornerShape(1.dp))
                .padding(1.dp)
                .constrainAs(typeRef) {
                    top.linkTo(imgRef.top, 2.dp)
                    end.linkTo(imgRef.end, 10.dp)
                }
        )
        /**
         * 视频名称*/
        Text(
            text = bean.title,
            fontSize = 14.sp,
            color = BilibiliTheme.colors.textTitle,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .constrainAs(nameRef){
                    top.linkTo(imgRef.top,2.dp)
                    start.linkTo(imgRef.end,5.dp)
                    end.linkTo(viewRef.start)
                    width = Dimension.fillToConstraints
                }
        )

        Row(modifier = Modifier.constrainAs(infoRef) {
            top.linkTo(nameRef.bottom, 5.dp)
            start.linkTo(nameRef.start)
        }) {
            /**
             * 徽章*/
            if (bean.badges != null){
                Text(
                    text = bean.badges[0].text,
                    fontSize = 10.sp,
                    color = BilibiliTheme.colors.highlightColor,
                    modifier = Modifier
                        .border(1.dp, BilibiliTheme.colors.highlightColor, RoundedCornerShape(1.dp))
                        .padding(1.dp)
                )
                Spacer(modifier = Modifier.width(5.dp))
            }

            /**
             * 时间｜区域*/
            Text(
                text = "${DateTransform.millToDate(bean.pubtime.toLong()*1000).substring(0,5)}｜${bean.areas}",
                fontSize = 12.sp,
                color = BilibiliTheme.colors.textContent,
                modifier = Modifier
            )
        }

        /**
         * tags*/
        Text(
            text = bean.styles,
            color = BilibiliTheme.colors.textContent,
            fontSize = 12.sp,
            modifier = Modifier.constrainAs(styleRef){
                top.linkTo(infoRef.bottom,5.dp)
                start.linkTo(nameRef.start)
            }
        )
        /**
         * 描述*/
        Text(
            text = bean.desc,
            fontSize = 10.sp,
            color = BilibiliTheme.colors.textContent,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .constrainAs(desRef){
                    top.linkTo(styleRef.bottom,5.dp)
                    bottom.linkTo(scoreRef.top)
                    start.linkTo(nameRef.start)
                    end.linkTo(followRef.start)
                    width = Dimension.fillToConstraints
                    height = Dimension.fillToConstraints
                }
        )

        /**
         * 评分*/
        Text(
            text = "${bean.media_score.score}分",
            color = BilibiliTheme.colors.highlightColor,
            style = MaterialTheme.typography.subtitle1,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.constrainAs(scoreRef){
                start.linkTo(nameRef.start)
                bottom.linkTo(imgRef.bottom,2.dp)
            }
        )
        /**
         * 参与人数*/
        Text(
            text = "${calculation(bean.media_score.user_count)}人参与",
            color = BilibiliTheme.colors.textContent,
            fontSize = 10.sp,
            modifier = Modifier.constrainAs(fansRef){
                start.linkTo(scoreRef.end,5.dp)
                bottom.linkTo(scoreRef.bottom)
            }
        )

        /**
         *立即观看*/
        ExtendedFloatingActionButton(
            backgroundColor = BilibiliTheme.colors.highlightColor,
            shape = RoundedCornerShape(6.dp),
            text = { Text(text = "立即观看", color = BilibiliTheme.colors.bottomBar, fontSize = 12.sp) },
            onClick = {  },
            modifier = Modifier
                .width(120.dp)
                .height(30.dp)
                .constrainAs(viewRef) {
                    top.linkTo(nameRef.top)
                    end.linkTo(parent.end, 5.dp)
                }
        )
        /**
         * 追剧*/
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .width(120.dp)
                .height(30.dp)
                .border(1.dp, BilibiliTheme.colors.highlightColor, RoundedCornerShape(4.dp))
                .padding(2.dp)
                .constrainAs(followRef) {
                    top.linkTo(viewRef.bottom, 10.dp)
                    end.linkTo(parent.end, 5.dp)
                }
        ) {
            Icon(
                imageVector = Icons.Default.Favorite,
                contentDescription = "follow",
                tint = BilibiliTheme.colors.highlightColor
            )
            Spacer(modifier = Modifier.width(2.dp))
            Text(
                text = name,
                color = BilibiliTheme.colors.highlightColor,
                fontSize = 12.sp
            )
        }
    }
}