package com.franz.bilibili.nav.searchresult.result.official

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExtendedFloatingActionButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.AsyncImage
import com.franz.bilibili.R
import com.franz.bilibili.bean.searchresult.bilibili_user.BilibiliUser
import com.franz.bilibili.bean.searchresult.bilibili_user.Re
import com.franz.bilibili.tools.DateTransform
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.ui.theme.white
import com.franz.bilibili.widget.ItemRow
import com.franz.bilibili.widget.calculation

@Composable
fun ResultOfficialPage(
    users:List<BilibiliUser>,
    modifier: Modifier = Modifier,
    screenHeight:Int = LocalConfiguration.current.screenHeightDp,
    enableScroll:Boolean = true
){
    Box(modifier = modifier.fillMaxSize()){
        LazyColumn(
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Top,
            userScrollEnabled = enableScroll,
            modifier = Modifier
                .fillMaxWidth()
                .heightIn(max = screenHeight.dp)
                .align(Alignment.TopStart)
        ) {
            items(users.size) {
                ResultUserItem(user = users[it])
                if (users[it].res.isNotEmpty()) {
                    RelateVideo(users[it].res)
                }
            }
        }
    }
}

@Composable
private fun ResultUserItem(user:BilibiliUser,modifier: Modifier = Modifier){
    ConstraintLayout(
        modifier = modifier
            .fillMaxWidth()
            .heightIn(max = 100.dp)
            .background(BilibiliTheme.colors.backgroundTop, shape = RoundedCornerShape(4.dp))
            .padding(5.dp)
    ) {
        val (imgRef,nameRef,fansRef,videoRef,levelRef,followRef) = createRefs()
        /**
         * 用户图像*/
        AsyncImage(
            model = "http:".plus(user.upic),
            contentDescription = "img",
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .size(48.dp)
                .clip(shape = CircleShape)
                .constrainAs(imgRef) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start, 5.dp)
                }
        )
        /**
         * 用户昵称*/
        Text(
            text = user.uname,
            color = BilibiliTheme.colors.textTitle,
            fontSize = 14.sp,
            modifier = Modifier.constrainAs(nameRef){
                top.linkTo(imgRef.top)
                start.linkTo(imgRef.end,5.dp)
            }
        )
        /**
         * 用户等级*/
        Text(
            text = "LV${user.level}",
            color = BilibiliTheme.colors.bottomBar,
            fontSize = 10.sp,
            modifier = Modifier
                .background(BilibiliTheme.colors.highlightColor, RoundedCornerShape(4.dp))
                .padding(1.dp)
                .constrainAs(levelRef) {
                    top.linkTo(nameRef.top)
                    start.linkTo(nameRef.end, 5.dp)
                }
        )
        /**
         * 用户粉丝数量*/
        Text(
            text = "${calculation(user.fans)}粉丝",
            color = BilibiliTheme.colors.textContent,
            fontSize = 12.sp,
            modifier = Modifier
                .constrainAs(fansRef){
                    bottom.linkTo(imgRef.bottom)
                    start.linkTo(nameRef.start)
                }
        )
        /**
         * 发布视频数量*/
        Text(
            text = "${calculation(user.videos)}个视频",
            color = BilibiliTheme.colors.textContent,
            fontSize = 12.sp,
            modifier = Modifier
                .constrainAs(videoRef){
                    top.linkTo(fansRef.top)
                    start.linkTo(fansRef.end,10.dp)
                }
        )
        /**
         * 关注按钮*/
        ExtendedFloatingActionButton(
            text = { Text(text = "+关注", color = BilibiliTheme.colors.bottomBar, fontSize = 14.sp) },
            onClick = {  },
            backgroundColor = BilibiliTheme.colors.highlightColor,
            shape = RoundedCornerShape(4.dp),
            modifier = Modifier
                .height(30.dp)
                .constrainAs(followRef) {
                    top.linkTo(imgRef.top)
                    bottom.linkTo(imgRef.bottom)
                    end.linkTo(parent.end, 5.dp)
                }
        )
    }
}

@Composable
private fun RelateVideo(videos: List<Re>,modifier: Modifier = Modifier){
    Spacer(modifier = Modifier.height(6.dp))
    repeat(videos.size){
        RelateVideoItem(bean = videos[it])
        if (it < videos.size - 1){
            Spacer(modifier = Modifier.height(6.dp))
        }
    }
}

@Composable
private fun RelateVideoItem(
    bean: Re,
    imageHeight: Dp = 100.dp
) {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .height(imageHeight)
            .background(BilibiliTheme.colors.backgroundTop, shape = RoundedCornerShape(4.dp))
    ) {
        val (imageRef, titleRef, publishRef, durationRef, playRef,commentRef,likeRef) = createRefs()

        /**
         * 封面*/
        AsyncImage(
            model = "http:".plus(bean.pic),
            contentDescription = bean.pic,
            contentScale = ContentScale.Crop,
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .fillMaxHeight()
                .clip(RoundedCornerShape(4.dp))
                .aspectRatio(1.7f)//aspectRatio:width/height
                .constrainAs(imageRef) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }
        )

        /**
         * 视频标题*/
        Text(
            text = bean.title,
            color = BilibiliTheme.colors.textTitle,
            style = MaterialTheme.typography.caption,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Start,
            modifier = Modifier
                .constrainAs(titleRef) {
                    start.linkTo(imageRef.end, 5.dp)
                    end.linkTo(parent.end, 5.dp)
                    top.linkTo(imageRef.top, 2.dp)
                    width = Dimension.fillToConstraints
                }
        )

        /**
         * 发布时间*/
        Text(
            text = DateTransform.millToDate((bean.pubdate.toLong() * 1000)),
            color = BilibiliTheme.colors.textContent,
            style = MaterialTheme.typography.overline,
            modifier = Modifier.constrainAs(publishRef) {
                bottom.linkTo(playRef.top,5.dp)
                start.linkTo(titleRef.start)
            }
        )

        /**
         * 视频时长*/
        Text(
            text = bean.duration,
            color = white,
            style = MaterialTheme.typography.overline,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .background(
                    color = Color.Black.copy(alpha = 0.4f),
                    shape = RoundedCornerShape(2.dp)
                )
                .padding(2.dp)
                .constrainAs(durationRef) {
                    top.linkTo(imageRef.bottom, ((-20).dp))
                    end.linkTo(imageRef.end, 6.dp)
                }
        )

        /**
         * 观看人数*/
        ItemRow(
            number = bean.play.toInt(),
            icon = R.drawable.icon_view,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(playRef) {
                bottom.linkTo(imageRef.bottom, 2.dp)
                start.linkTo(imageRef.end, 5.dp)
            }
        )

        /**
         * 评论人数*/
        ItemRow(
            number = bean.dm,
            icon = R.drawable.icon_review,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(commentRef) {
                top.linkTo(playRef.top)
                start.linkTo(playRef.end)
                end.linkTo(likeRef.start)
            }
        )

        /**
         * 收藏人数*/
        ItemRow(
            number = bean.fav,
            icon = R.drawable.icon_favorite,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(likeRef) {
                top.linkTo(playRef.top)
                end.linkTo(parent.end, 5.dp)
            }
        )
    }
}