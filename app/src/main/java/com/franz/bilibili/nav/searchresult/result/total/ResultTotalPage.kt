package com.franz.bilibili.nav.searchresult.result.total

import android.annotation.SuppressLint
import androidx.compose.animation.*
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronRight
import androidx.compose.material.icons.filled.Sort
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.*
import com.franz.bilibili.bean.BilibiliID
import com.franz.bilibili.nav.searchresult.ResultListState
import com.franz.bilibili.nav.searchresult.SearchResultEvent
import com.franz.bilibili.nav.searchresult.TotalFilterType
import com.franz.bilibili.nav.searchresult.result.activity.ResultActivityPage
import com.franz.bilibili.nav.searchresult.result.game.ResultGamePage
import com.franz.bilibili.nav.searchresult.result.media.ResultMediaPage
import com.franz.bilibili.nav.searchresult.result.official.ResultOfficialPage
import com.franz.bilibili.nav.searchresult.result.video.ResultVideoPage
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.widget.BilibiliRadioButton

@SuppressLint("SuspiciousIndentation", "UnrememberedMutableState")
@Composable
fun ResultTotalPage(
    modifier: Modifier = Modifier,
    filterType: TotalFilterType,
    isShowFilter:Boolean = false,
    resultListState: ResultListState,
    viewModel: TotalViewModel = TotalViewModel(resultListState),
    onShowFilter:()->Unit,
    onChangeType: (TotalFilterType) -> Unit,
    onClickItem:(BilibiliID)->Unit
){
    val state = viewModel.state.value
        LazyColumn(
            modifier = modifier.fillMaxSize()
        ){
            item {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Spacer(modifier = Modifier.weight(1f))
                    Icon(
                        imageVector = Icons.Default.Sort,
                        contentDescription = "filter",
                        tint = BilibiliTheme.colors.textTitle,
                        modifier = Modifier
                            .size(24.dp)
                            .clickable {
                                onShowFilter()
                            }
                    )
                }
                Spacer(modifier = Modifier.height(5.dp))
            }
            item{
                AnimatedVisibility(
                    visible = isShowFilter,
                    enter = fadeIn() + slideInVertically(),
                    exit = fadeOut() + slideOutVertically()
                ) {
                    AnimationFilter(type = filterType, onChangeType = onChangeType)
                }
                Spacer(modifier = Modifier.height(5.dp))
            }
            item{
                AnimatedVisibility(visible = state.showOfficial) {
                    Column(verticalArrangement = Arrangement.Center) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = "推荐用户",
                                style = MaterialTheme.typography.subtitle1,
                                color = BilibiliTheme.colors.textTitle,
                                fontWeight = FontWeight.Bold,
                            )
                            Spacer(modifier = Modifier.width(2.dp))
                            Icon(
                                imageVector = Icons.Default.ChevronRight,
                                contentDescription = "ChevronRight",
                                tint = BilibiliTheme.colors.textTitle,
                                modifier = Modifier.size(20.dp)
                            )
                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        ResultOfficialPage(users = state.values.bilibili_user, enableScroll = true)
                        Spacer(modifier = Modifier.height(10.dp))
                    }
                }
            }
            item {
                AnimatedVisibility(visible = state.showActivity) {
                    Column(verticalArrangement = Arrangement.Center) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = "推荐活动",
                                style = MaterialTheme.typography.subtitle1,
                                color = BilibiliTheme.colors.textTitle,
                                fontWeight = FontWeight.Bold,
                            )
                            Spacer(modifier = Modifier.width(2.dp))
                            Icon(
                                imageVector = Icons.Default.ChevronRight,
                                contentDescription = "ChevronRight",
                                tint = BilibiliTheme.colors.textTitle,
                                modifier = Modifier.size(20.dp)
                            )
                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        ResultActivityPage(activities = state.values.activities, enableScroll = true)
                        Spacer(modifier = Modifier.height(10.dp))
                    }
                }
            }
            item {
                AnimatedVisibility(visible = state.showGame) {
                    Column(verticalArrangement = Arrangement.Center) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = "推荐游戏",
                                style = MaterialTheme.typography.subtitle1,
                                color = BilibiliTheme.colors.textTitle,
                                fontWeight = FontWeight.Bold,
                            )
                            Spacer(modifier = Modifier.width(2.dp))
                            Icon(
                                imageVector = Icons.Default.ChevronRight,
                                contentDescription = "ChevronRight",
                                tint = BilibiliTheme.colors.textTitle,
                                modifier = Modifier.size(20.dp)
                            )
                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        ResultGamePage(games = state.values.games, enableScroll = true)
                        Spacer(modifier = Modifier.height(10.dp))
                    }
                }
            }
            item {
                AnimatedVisibility(visible = state.showAnime) {
                    Column(verticalArrangement = Arrangement.Center) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = "推荐番剧",
                                style = MaterialTheme.typography.subtitle1,
                                color = BilibiliTheme.colors.textTitle,
                                fontWeight = FontWeight.Bold,
                            )
                            Spacer(modifier = Modifier.width(2.dp))
                            Icon(
                                imageVector = Icons.Default.ChevronRight,
                                contentDescription = "ChevronRight",
                                tint = BilibiliTheme.colors.textTitle,
                                modifier = Modifier.size(20.dp)
                            )
                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        //番剧
                        ResultMediaPage(videos = state.values.media_bangumi, name = "追番", enableScroll = true)
                        Spacer(modifier = Modifier.height(10.dp))
                    }
                }
            }
            item {
                AnimatedVisibility(visible = state.showTv) {
                    Column(verticalArrangement = Arrangement.Center) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = "推荐影视",
                                style = MaterialTheme.typography.subtitle1,
                                color = BilibiliTheme.colors.textTitle,
                                fontWeight = FontWeight.Bold,
                            )
                            Spacer(modifier = Modifier.width(2.dp))
                            Icon(
                                imageVector = Icons.Default.ChevronRight,
                                contentDescription = "ChevronRight",
                                tint = BilibiliTheme.colors.textTitle,
                                modifier = Modifier.size(20.dp)
                            )
                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        //影视
                        ResultMediaPage(videos = state.values.media_ft,name = "追剧", enableScroll = true)
                        Spacer(modifier = Modifier.height(10.dp))
                    }
                }
            }
            item {
                AnimatedVisibility(visible = state.showVideo) {
                    Column(verticalArrangement = Arrangement.Center) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = "推荐视频",
                                style = MaterialTheme.typography.subtitle1,
                                color = BilibiliTheme.colors.textTitle,
                                fontWeight = FontWeight.Bold,
                            )
                            Spacer(modifier = Modifier.width(2.dp))
                            Icon(
                                imageVector = Icons.Default.ChevronRight,
                                contentDescription = "ChevronRight",
                                tint = BilibiliTheme.colors.textTitle,
                                modifier = Modifier.size(20.dp)
                            )
                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        ResultVideoPage(videos = state.values.searchVideos, onClickItem = onClickItem, enableScroll = true)
                        Spacer(modifier = Modifier.height(10.dp))
                    }
                }
            }
        }
}

@Composable
fun AnimationFilter(
    modifier: Modifier = Modifier,
    type: TotalFilterType,
    onChangeType:(TotalFilterType)->Unit
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(vertical = 4.dp)
    ) {
        Row(modifier = Modifier.fillMaxWidth()) {
            BilibiliRadioButton(
                text = "默认排序",
                selected = type == TotalFilterType.DefaultSort
            ) {
                onChangeType(TotalFilterType.DefaultSort)
            }
            Spacer(modifier = Modifier.width(8.dp))
            BilibiliRadioButton(
                text = "非正常排序",
                selected = type == TotalFilterType.OddSort
            ) {
                onChangeType(TotalFilterType.OddSort)
            }
        }
        Spacer(modifier = Modifier.height(10.dp))
        Row(modifier = Modifier.fillMaxWidth()) {
            BilibiliRadioButton(
                text = "新发布",
                selected = type == TotalFilterType.NewUpdateSort
            ) {
                onChangeType(TotalFilterType.NewUpdateSort)
            }
            Spacer(modifier = Modifier.width(8.dp))
            BilibiliRadioButton(
                text = "播放多",
                selected = type == TotalFilterType.PlaySort
            ) {
                onChangeType(TotalFilterType.PlaySort)
            }
            Spacer(modifier = Modifier.width(8.dp))
            BilibiliRadioButton(
                text = "弹幕多",
                selected = type == TotalFilterType.CommentSort
            ) {
                onChangeType(TotalFilterType.CommentSort)
            }
        }
    }
}