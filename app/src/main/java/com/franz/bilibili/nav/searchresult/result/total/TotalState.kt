package com.franz.bilibili.nav.searchresult.result.total

import com.franz.bilibili.nav.searchresult.ResultListState

data class TotalState(
    val values: ResultListState,
    val showOfficial:Boolean = false,
    val showActivity:Boolean = false,
    val showGame:Boolean = false,
    val showAnime:Boolean = false,
    val showTv:Boolean = false,
    val showVideo:Boolean = false
)
