package com.franz.bilibili.nav.searchresult.result.total

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.franz.bilibili.nav.searchresult.ResultListState
import kotlinx.coroutines.launch

class TotalViewModel(resultListState: ResultListState) :ViewModel() {
    private val _state = mutableStateOf(TotalState(values = resultListState))
    val state: State<TotalState> = _state
    init {
        viewModelScope.launch {
           checkEmpty()
        }
    }

    private fun checkEmpty(){
        /**
         * 判断官方用户是否存在内容*/
        if (_state.value.values.bilibili_user.isNotEmpty()){
            _state.value = state.value.copy(
                showOfficial = true
            )
        }else{
            _state.value = state.value.copy(
                showOfficial = false
            )
        }
        /**
         * 判断活动是否存在内容*/
        if (_state.value.values.activities.isNotEmpty()){
            _state.value = state.value.copy(
                showActivity = true
            )
        }else{
            _state.value = state.value.copy(
                showActivity = false
            )
        }
        /**
         * 判断游戏是否存在内容*/
        if (_state.value.values.games.isNotEmpty()){
            _state.value = state.value.copy(
                showGame = true
            )
        }else{
            _state.value = state.value.copy(
                showGame = false
            )
        }
        /**
         * 判断番剧是否存在内容*/
        if (_state.value.values.media_bangumi.isNotEmpty()){
            _state.value = state.value.copy(
                showAnime = true
            )
        }else{
            _state.value = state.value.copy(
                showAnime = false
            )
        }
        /**
         * 判断影视是否存在内容*/
        if (_state.value.values.media_ft.isNotEmpty()){
            _state.value = state.value.copy(
                showTv = true
            )
        }else{
            _state.value = state.value.copy(
                showTv = false
            )
        }
        /**
         * 判断视频是否存在内容*/
        if (_state.value.values.searchVideos.isNotEmpty()){
            _state.value = state.value.copy(
                showVideo = true
            )
        }else{
            _state.value = state.value.copy(
                showVideo = false
            )
        }
    }
}