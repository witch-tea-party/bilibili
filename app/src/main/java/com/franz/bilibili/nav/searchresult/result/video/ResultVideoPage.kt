package com.franz.bilibili.nav.searchresult.result.video

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.AsyncImage
import com.franz.bilibili.R
import com.franz.bilibili.bean.BilibiliID
import com.franz.bilibili.bean.searchresult.video.SearchVideo
import com.franz.bilibili.tools.DateTransform
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.ui.theme.white
import com.franz.bilibili.widget.ItemRow
import com.franz.bilibili.widget.videoAuthor

@Composable
fun ResultVideoPage(
    videos:List<SearchVideo>,
    modifier: Modifier = Modifier,
    enableScroll:Boolean = true,
    screenHeight:Int = LocalConfiguration.current.screenHeightDp,
    onClickItem:(BilibiliID)->Unit
){
    Box(modifier = modifier.fillMaxSize()){
        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(6.dp),
            userScrollEnabled = enableScroll,
            modifier = modifier
                .fillMaxWidth()
                .heightIn(max = screenHeight.dp)
                .align(Alignment.TopStart)
        ) {
            items(videos.size) {
                ResultVideoItem(bean = videos[it], onClickItem = onClickItem)
            }
        }
    }
}

@Composable
private fun ResultVideoItem(
    bean: SearchVideo,
    imageHeight: Dp = 100.dp,
    onClickItem:(BilibiliID)->Unit
) {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .height(imageHeight)
            .background(BilibiliTheme.colors.backgroundTop, shape = RoundedCornerShape(4.dp))
            .clickable { onClickItem(BilibiliID(aid = bean.aid, cid = 0)) }
    ) {
        val (imageRef, viewRef, titleRef, authorRef, publishRef, durationRef, reasonRef) = createRefs()

        /**
         * 封面*/
        AsyncImage(
            model = "http:".plus(bean.pic),
            contentDescription = bean.title,
            contentScale = ContentScale.Crop,
            placeholder = painterResource(id = R.drawable.bilibili_logo),
            modifier = Modifier
                .fillMaxHeight()
                .clip(RoundedCornerShape(4.dp))
                .aspectRatio(1.7f)//aspectRatio:width/height
                .constrainAs(imageRef) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }
        )

        /**
         * 视频标题*/
        Text(
            text = bean.title,
            color = BilibiliTheme.colors.textTitle,
            style = MaterialTheme.typography.caption,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Start,
            modifier = Modifier
                .height(40.dp)
                .constrainAs(titleRef) {
                    start.linkTo(imageRef.end, 5.dp)
                    end.linkTo(parent.end, 5.dp)
                    top.linkTo(imageRef.top, 2.dp)
                    width = Dimension.fillToConstraints
                }
        )

        /**
         * 视频时长*/
        Text(
            text = bean.duration,
            color = white,
            style = MaterialTheme.typography.overline,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .background(
                    color = Color.Black.copy(alpha = 0.4f),
                    shape = RoundedCornerShape(2.dp)
                )
                .padding(2.dp)
                .constrainAs(durationRef) {
                    top.linkTo(imageRef.bottom, ((-20).dp))
                    end.linkTo(imageRef.end, 6.dp)
                }
        )

        /**
         * 作者*/
        videoAuthor(
            bean.author,
            modifier = Modifier
                .constrainAs(authorRef) {
                    bottom.linkTo(viewRef.top, 2.dp)
                    start.linkTo(viewRef.start, (-5).dp)
                }
        )

        /**
         * 观看人数*/
        ItemRow(
            number = bean.play,
            icon = R.drawable.icon_view,
            color = BilibiliTheme.colors.textContent,
            modifier = Modifier.constrainAs(viewRef) {
                bottom.linkTo(imageRef.bottom, 2.dp)
                start.linkTo(imageRef.end, 5.dp)
            }
        )

        /**
         * 发布时间*/
        Text(
            text = DateTransform.millToDate((bean.pubdate.toLong() * 1000)),
            color = BilibiliTheme.colors.textContent,
            style = MaterialTheme.typography.overline,
            modifier = Modifier.constrainAs(publishRef) {
                top.linkTo(viewRef.top)
                bottom.linkTo(viewRef.bottom)
                start.linkTo(viewRef.end, 5.dp)
            }
        )
    }
}