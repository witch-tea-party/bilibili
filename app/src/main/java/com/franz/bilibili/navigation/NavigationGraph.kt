package com.franz.bilibili.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.*
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.franz.bilibili.APP
import com.franz.bilibili.bean.hot.HotBean
import com.franz.bilibili.login.LoginPage
import com.franz.bilibili.nav.detail.VideoDetailPage
import com.franz.bilibili.nav.home.HomePage
import com.franz.bilibili.nav.mine.MinePage
import com.franz.bilibili.nav.rank.RankPage
import com.franz.bilibili.nav.search.SearchPage
import com.franz.bilibili.nav.searchresult.SearchResultPage
import com.franz.bilibili.param.SearchKey
import com.franz.bilibili.tools.SPUtil
import com.franz.bilibili.weblogin.WebLoginPage

@Composable
fun NavigationGraph(
    navHostController: NavHostController,
    startDestination: String = Screen.WebLoginPage.route,
    modifier: Modifier = Modifier
){
    NavHost(navController = navHostController, startDestination = startDestination){
        composable(Screen.WebLoginPage.route){
            WebLoginPage(
                modifier = modifier,
                onNavigation = {
                    NavigationAction.toLogin(navHostController)
                },
                onBack = {
                    NavigationAction.onBack(navHostController)
                }
            )
        }

        composable(Screen.LoginPage.route){
            LoginPage(modifier = modifier){
                NavigationAction.toNavigation(navHostController)
            }
        }

        navigation(startDestination = Screen.HomePage.route, route = Screen.NavigationPage.route){
            bottomNavPage(navHostController,modifier)
        }

        composable(
            Screen.SearchResultPage.route.plus("?key={key}&hint={hint}"),
            arguments = listOf(
                navArgument(name = "key"){
                    type = NavType.StringType
                    defaultValue = ""
                },
                navArgument(name = "hint") {
                    type = NavType.StringType
                    defaultValue = ""
                }
            )
        ){
            SearchResultPage(modifier, onClickItem = {
                NavigationAction.toVideoDetail(navHostController,it.aid,it.cid)
            },
            onNavigation = {
                NavigationAction.onBack(navHostController)
            })
        }

        composable(
            Screen.VideoPlayPage.route.plus("?aid={aid}&cid={cid}"),
            arguments = listOf(
                navArgument("aid"){
                    type = NavType.IntType
                    defaultValue = 0
                },
                navArgument("cid"){
                    type = NavType.IntType
                    defaultValue = 0
                }
            )
        ){
            VideoDetailPage()
        }
    }
}

fun NavGraphBuilder.bottomNavPage(
    navHostController: NavHostController,
    modifier: Modifier = Modifier
){
    composable(Screen.HomePage.route){
        HomePage(modifier, onClickItem = {
            //跳转至视频播放页
            NavigationAction.toVideoDetail(navHostController,it.aid,it.cid)
        }, onClick = {
            //点击了顶部搜索栏，然后获取顶部搜索栏默认搜索关键字，并跳转至搜索界面
            //SPUtil.putValue(APP.context, SearchKey,it)
            NavigationAction.toSearch(navHostController,it)
        })
    }

    composable(Screen.RankPage.route){
        RankPage(modifier){
            //跳转至视频播放页
            NavigationAction.toVideoDetail(navHostController,it.aid,it.cid)
        }
    }

    composable(
        route = Screen.SearchPage.route,
        arguments = listOf(
            navArgument(name = "key"){
                type = NavType.StringType
                defaultValue = ""
            }
        )
    ){
        SearchPage(modifier = modifier){
            NavigationAction.toSearchResult(navHostController = navHostController, key = it.text, hint = it.hint)
        }
    }

    composable(Screen.MinePage.route){
        MinePage(modifier)
    }
}

class NavigationAction{
    companion object{
        fun onBack(navHostController: NavHostController){
            navHostController.navigateUp()
        }

        fun toLogin(navHostController: NavHostController){
            navHostController.navigate(Screen.LoginPage.route)
        }

        fun toNavigation(navHostController: NavHostController){
            navHostController.navigate(Screen.NavigationPage.route)
        }

        fun toSearch(navHostController: NavHostController,key:String){
            navHostController.navigate("SearchPage?key=$key"){
                navHostController.graph.startDestinationRoute?.let { route->
                    popUpTo(route){saveState = true}
                }
                launchSingleTop = true
                restoreState = true
            }
        }

        fun toSearchResult(navHostController: NavHostController,key: String,hint:String){
            navHostController.navigate(Screen.SearchResultPage.route.plus("?key=$key&hint=$hint"))
        }

        fun toVideoDetail(navHostController: NavHostController,aid:Int,cid:Int){
            navHostController.navigate(Screen.VideoPlayPage.route.plus("?aid=$aid&cid=$cid"))
        }
    }
}