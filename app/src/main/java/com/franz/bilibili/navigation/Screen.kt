package com.franz.bilibili.navigation

sealed class Screen(val route: String)
{
    object WebLoginPage: Screen("WebLoginPage")
    object LoginPage: Screen("PasswordLoginPage")
    object NavigationPage: Screen("NavigationPage")
    object SearchResultPage: Screen("SearchResultPage")
    object VideoPlayPage:Screen("VideoPlayPage")

    //bottom navigation bar route
    object HomePage: Screen("HomePage")
    object RankPage: Screen("RankPage")
    object SearchPage: Screen("SearchPage?key={key}")
    object MinePage: Screen("MinePage")
}
