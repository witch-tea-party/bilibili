package com.franz.bilibili.network

import com.franz.bilibili.bean.BaseResponse
import com.franz.bilibili.bean.captcha.CaptchaBean
import com.franz.bilibili.bean.login.HashKey
import com.franz.bilibili.bean.login.LoginBean
import com.franz.bilibili.nav.home.hot.HotState
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

/**
 * 登录步骤，使用web流程进行登录
 * 第一步：进行人机验证，访问captcha接口，获取token，challenge，gt
 * 登录功能暂且延迟*/
interface CollectService {

    /**
     * 第三方收集的提供的api接口*/

    @GET("x/passport-login/captcha?source=main_web")
    suspend fun getCaptcha():CaptchaBean

    //http://passport.bilibili.com/api/oauth2/getKey
    @GET("api/oauth2/getKey")
    suspend fun getHashKey():BaseResponse<HashKey>

    @POST("api/v3/oauth2/login")
    @FormUrlEncoded
    suspend fun login(
        @Field("username")username:String,
        @Field("password")password:String,
        @Field("challenge")challenge:String? =null,
        @Field("seccode")seccode:String? =null,
        @Field("validate")validate:String? =null,
    ):BaseResponse<LoginBean>
}