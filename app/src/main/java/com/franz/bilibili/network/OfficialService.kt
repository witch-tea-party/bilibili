package com.franz.bilibili.network

import com.franz.bilibili.bean.BaseResponse
import com.franz.bilibili.bean.VideoUrl
import com.franz.bilibili.bean.detail.VideoDetail
import com.franz.bilibili.bean.hot.HotModel
import com.franz.bilibili.nav.home.recommend.RecommendState
import com.google.gson.JsonObject
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query
import retrofit2.http.QueryMap
import retrofit2.http.Url

interface OfficialService {
    /**
     * 官方提供的api接口*/
    @GET("recommend")
    suspend fun getRecommend(@Query("page")page:Int = 1,@Query("pagesize")pagesize:Int = 10): RecommendState


    /**
     * 获取热门视频
     * pn:页码，默认1
     * ps:每页内容数量，默认20*/
    @GET("x/web-interface/popular")
    suspend fun getHot(@Query("pn")pn: Int = 1,@Query("ps")ps: Int = 10): HotModel

    /**
     * 获取默认搜索关键字*/
    @GET("x/web-interface/search/default")
    fun getDefaultSearchKey():Call<JsonObject>

    /**
     * 获取搜索建议*/
    @GET("suggest")
    fun getSearchSuggest(@Query("term")term:String):Call<JsonObject>

   /**
    * 热搜榜单，默认热搜前20个结果
    * 因为此接口与其他两大类接口baseurl不一致
    * 为了减少创建Retrofit对象以及接口管理
    * 故通过"@Url"关键字直接引入全部url*/
   @GET
    fun getHotSearch(@Url url: String):Call<JsonObject>

    /**
     * 根据关键字获取相关搜索结果*/
    /**
     * result_type:tips
     * result_type:esports
     * result_type:activity 活动
     * result_type:web_game 游戏
     * result_type:card
     * result_type:media_bangumi 番剧
     * result_type:media_ft 影视
     * result_type:bili_user 官方账号
     * result_type:user 普通用户账号
     * result_type:star
     * result_type:video 视频*/
    @GET("x/web-interface/search/all/v2")
    fun getSearchResult(@Query("keyword")keyword:String):Call<JsonObject>

    /**
     * 获取排行榜信息
     * rid：是分区id
     * 并不是每个分区id都有对应内容返回，code=0代表成功，code=-400为失败
     * 例如为0，代表全部内容
     * 1代表：动画
     * 3代表：音乐
     * 4代表:游戏
     * 5代表：娱乐
     * 11代表：电视剧
     * 13代表：番剧(排行榜不可用，最新可用)
     * 167代表：国漫(排行榜不可用，最新可用)
     * 23代表：电影
     * 36代表：知识
     * 119代表：鬼畜
     * 129代表：舞蹈
     * 155代表：时尚
     * 160代表：生活
     * 177代表：纪录片
     * 181代表：影视
     * 211代表：美食
     * 234代表：运动*/
    //https://api.bilibili.com/x/web-interface/ranking/v2
    @GET("x/web-interface/ranking/v2")
    fun getRank(@Query("rid")rid: Int):Call<JsonObject>

    /**
     * 获取视频的详细信息
     * 可以从此接口中获取cid
     * 传入aid or bvid*/
    //https://api.bilibili.com/x/web-interface/view?aid=648927961
    //VideoDetail
    @GET("x/web-interface/view")
    suspend fun getVideoDetail(@Query("aid")aid:Int):BaseResponse<VideoDetail>

    /**
     * 传入avid或者bvid获取相关推荐视频
     * 进入某个视频详情界面时，通过传入此视频的avid or bvid获取相关推荐视频*/
    //https://api.bilibili.com/x/web-interface/archive/related
    @GET("x/web-interface/archive/related")
    fun getRelatedVideos(@Query("aid")aid:Int):Call<JsonObject>

    /**
     * 获取视频URL播放地址*/
    //https://api.bilibili.com/x/player/playurl
    @Headers(
        "Referer:https://www.bilibili.com/",
        "User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
        "Origin:https://www.bilibili.com"
    )
    @GET("x/player/playurl")
    suspend fun getVideoUrl(@QueryMap map: Map<String,String>):BaseResponse<VideoUrl>
}