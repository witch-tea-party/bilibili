package com.franz.bilibili.param

import androidx.compose.runtime.mutableStateListOf


//@Qualifier
//@Retention(AnnotationRetention.BINARY)
//annotation class OfficialRetrofit
//
//@Qualifier
//@Retention(AnnotationRetention.BINARY)
//annotation class RecommendRetrofit

const val CookieKey: String = "CookieKey"
const val SearchKey: String = "SearchKey"

val recommend_banners = listOf(
    "https://i0.hdslb.com/bfs/banner/4817c6f52a736255714616c72ce48c6a28e7b30f.png",
    "https://i0.hdslb.com/bfs/banner/c266ca8303548aee19858a245346dd84f205b3a9.jpg",
    "https://i0.hdslb.com/bfs/banner/09ccba020039f60289213aa8bcc93089dd825851.jpg",
    "https://i0.hdslb.com/bfs/banner/9a1f078a584302b509f2b5cc1499777e0f55fc48.png"
)

val hot_banners = listOf(
    "https://i1.hdslb.com/bfs/archive/55ce9a4d1797ec56a0d4ed727f1a279b89ec3664.jpg",
    "https://i1.hdslb.com/bfs/archive/66faf0db298d9e61228283628064212e4c3152f8.jpg",
    "https://i0.hdslb.com/bfs/archive/d7a15ce7bccdaf3ff0a41e2b2c80feac1f9edc82.jpg",
    "https://i2.hdslb.com/bfs/archive/2f64c176864bced7835d375c47c2de700a107f93.jpg"
)

/**
 * 获取排行榜信息
 * rid：是分区id
 * 并不是每个分区id都有对应内容返回，code=0代表成功，code=-400为失败
 * 例如为0，代表全部内容
 * 1代表：动画
 * 3代表：音乐
 * 4代表:游戏
 * 5代表：娱乐
 * 11代表：电视剧
 * 13代表：番剧(排行榜不可用，最新可用)
 * 167代表：国漫(排行榜不可用，最新可用)
 * 23代表：电影
 * 36代表：知识
 * 119代表：鬼畜
 * 129代表：舞蹈
 * 155代表：时尚
 * 160代表：生活
 * 177代表：纪录片
 * 181代表：影视
 * 211代表：美食
 * 234代表：运动*/
val rankRidMap = mapOf<Int,Int>(
    0 to 0,
    1 to 1,
    2 to 3,
    3 to 4,
    4 to 5,
    5 to 11,
    6 to 23,
    7 to 36,
    8 to 119,
    9 to 129,
    10 to 155,
    11 to 160,
    12 to 177,
    13 to 181,
    14 to 211,
    15 to 234
)

val rankValueList = mutableStateListOf(
    "全区",
    "动画",
    "音乐",
    "游戏",
    "娱乐",
    "电视剧",
    "电影",
    "知识",
    "鬼畜",
    "舞蹈",
    "时尚",
    "生活",
    "纪录片",
    "影视",
    "美食",
    "运动"
)


