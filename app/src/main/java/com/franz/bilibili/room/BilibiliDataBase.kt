package com.franz.bilibili.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.franz.bilibili.bean.SHistoryBean

@Database(entities = [SHistoryBean::class],version = 1)
abstract class BilibiliDataBase: RoomDatabase() {
    abstract val sHistoryDao: SHistoryDao

    companion object{
        const val DATABASE_NAME = "Bilibili"
    }
}