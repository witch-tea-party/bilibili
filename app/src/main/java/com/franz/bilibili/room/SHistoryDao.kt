package com.franz.bilibili.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.franz.bilibili.bean.SHistoryBean
import kotlinx.coroutines.flow.Flow

@Dao
interface SHistoryDao {
    /**
     * 查询所以搜索历史记录*/
    @Query("SELECT *FROM SearchHistoryTable")
    fun queryAll(): Flow<List<SHistoryBean>>

    /**
     * 插入搜索记录*/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(bean: SHistoryBean)

    /**
     * 清空搜索记录*/
    @Query("DELETE FROM SearchHistoryTable")
    suspend fun deleteAll()
}