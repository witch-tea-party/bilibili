package com.franz.bilibili.room

import com.franz.bilibili.bean.SHistoryBean
import kotlinx.coroutines.flow.Flow

interface SHistoryRepository {
    /**
     * 查询所以搜索历史记录*/
    fun getHistories(): Flow<List<SHistoryBean>>

    /**
     * 插入搜索记录*/
    suspend fun insertHistory(bean: SHistoryBean)

    /**
     * 清空搜索记录*/
    suspend fun deleteAll()
}