package com.franz.bilibili.room

import com.franz.bilibili.bean.SHistoryBean
import kotlinx.coroutines.flow.Flow

class SHistoryRepositoryImpl(private val dao: SHistoryDao): SHistoryRepository {
    override fun getHistories(): Flow<List<SHistoryBean>> = dao.queryAll()

    override suspend fun insertHistory(bean: SHistoryBean) = dao.insert(bean)

    override suspend fun deleteAll() =  dao.deleteAll()
}