package com.franz.bilibili.tools

import android.util.Log
import java.net.URLEncoder
import java.security.MessageDigest

val appkey = "iVGUTjsxvpLeuDCf"
val appsec = "aHRmhWMLkdeMuILqORnYZocwMBpMEOdt"

fun appSign(params: MutableMap<String, String>, appkey: String, appsec: String): MutableMap<String, String> {
    // 为请求参数进行 api 签名
    params["appkey"] = appkey
    val sortedParams = params.toSortedMap(compareBy { it }) // 重排序参数 key
    val query = sortedParams.map { "${it.key}=${it.value}" }.joinToString("&") // 序列化参数
    val sign = md5(query + appsec) // 计算 api 签名
    params["sign"] = sign
    return params
}

fun md5(input: String): String {
    val md = MessageDigest.getInstance("MD5")
    val digest = md.digest(input.toByteArray(Charsets.UTF_8))
    return digest.joinToString("") {
        String.format("%02x", it)
    }
}

fun responseBody(params: MutableMap<String, String>):Map<String,String>{
      val signedParams = appSign(params, appkey, appsec)
      val sign = signedParams.map { "${it.key}=${URLEncoder.encode(it.value, "UTF-8")}" }.joinToString("&")
      return signedParams
}

//MD5
//private val md5Instance = MessageDigest.getInstance("MD5")
//
//fun String.md5() =
//    StringBuilder(32).apply {
//        //优化过的 md5 字符串生成算法
//        md5Instance.digest(toByteArray()).forEach {
//            val value = it.toInt() and 0xFF
//            val high = value / 16
//            val low = value % 16
//            append(if (high <= 9) '0' + high else 'a' - 10 + high)
//            append(if (low <= 9) '0' + low else 'a' - 10 + low)
//        }
//    }.toString()
//
///**
// * 签名算法为 "$排序后的参数字符串$appSecret".md5()
// */
//internal fun calculateSign(sortedQuery: String, appSecret: String) = (sortedQuery + appSecret).md5()
