package com.franz.bilibili.tools

import java.text.SimpleDateFormat
import java.util.*

class DateTransform {
    companion object{
         fun millToDate(time: Long): String{
            val date = Date(time)
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            return format.format(date)
        }
    }
}