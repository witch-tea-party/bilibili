package com.franz.bilibili.ui.theme

import androidx.compose.ui.graphics.Color


val white = Color(0xFFFFFFFF)
val grey = Color(0xFFC2C2C2)
val grey100 = Color(0xFFF2F2F2)
val black = Color(0xFF000000)
val black100 = Color(0xFF1A1A1A)
val black200 = Color(0xFF262626)
val black300 = Color(0xFF434343)

val blue100 = Color(0xFF009FFF)
val blue200 = Color(0xFFbfe9ff)
val red100 = Color(0xFFEC2F4B)
val red200 = Color(0xFFFC354C)
val green100 = Color(0xFF43C6AC)
val green200 = Color(0xFF0ABFBC)
val green300 = Color(0xFF134E5E)
val green400 = Color(0xFF71B280)
val green500 = Color(0xFF0f9b0f)
val yellow100 = Color(0xFFF8FFAE)
val pink100 = Color(0xFFff6e7f)
val pink200 = Color(0xFFd6A4A4)
val purple100 = Color(0xFFDAE2F8)
val purple200 = Color(0xFF3c1053)
val orange100 = Color(0xFFEE855E)
val orange200 = Color(0xFFFCF1ED)