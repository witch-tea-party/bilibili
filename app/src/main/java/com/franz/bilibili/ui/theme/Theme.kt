package com.franz.bilibili.ui.theme

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.TweenSpec
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = BilibiliColor(
    backgroundTop = black200,
    backgroundCenter = black100,
    backgroundBottom = black,
    backgroundEnd = black100,
    defaultIcon = grey,
    selectIcon = white,
    unselectIcon = grey,
    textTitle = white,
    textContent = grey,
    loginStart = black,
    loginEnd = black300,
    bottomBar = black,
    searchBar = black200,
    tabSelect = white,
    tabUnselect = grey,
    highlightColor = pink100
)

private val LightColorPalette = BilibiliColor(
    backgroundTop = white,
    backgroundCenter = white,
    backgroundBottom = pink100,
    backgroundEnd = pink200,
    defaultIcon = grey,
    selectIcon = pink100,
    unselectIcon = grey,
    textTitle = black,
    textContent = grey,
    loginStart = grey,
    loginEnd = pink100,
    bottomBar = white,
    searchBar = grey100,
    tabSelect = white,
    tabUnselect = black300,
    highlightColor = pink100
)

@Stable
class BilibiliColor(
    //由于是渐变色背景，所以背景由多个颜色组合而成
    backgroundTop: Color,//password page 上方背景颜色
    backgroundCenter: Color,//password page 中间背景颜色
    backgroundBottom: Color,//password page 底部背景颜色
    backgroundEnd: Color,
    defaultIcon: Color,// 默认图标填充颜色
    selectIcon: Color,// 图标被选中填充颜色
    unselectIcon: Color,//图标未选中填充颜色
    textTitle: Color,// 标题内容文字颜色
    textContent: Color,// 正文内容文字颜色
    loginStart: Color,//登录按钮前半部分背景颜色
    loginEnd: Color,//登录按钮后半部分背景颜色
    bottomBar: Color,
    searchBar: Color,
    tabSelect: Color,
    tabUnselect: Color,
    highlightColor: Color
){
    var backgroundTop: Color by mutableStateOf(backgroundTop)
        private set

    var backgroundCenter: Color by mutableStateOf(backgroundCenter)
        private set

    var backgroundBottom: Color by mutableStateOf(backgroundBottom)
        private set

    var backgroundEnd: Color by mutableStateOf(backgroundEnd)
        private set

    var defaultIcon: Color by mutableStateOf(defaultIcon)
        private set

    var selectIcon: Color by mutableStateOf(selectIcon)
        private set

    var unselectIcon: Color by mutableStateOf(unselectIcon)
        private set

    var textTitle: Color by mutableStateOf(textTitle)
        private set

    var textContent: Color by mutableStateOf(textContent)
        private set

    var loginStart: Color by mutableStateOf(loginStart)
        private set

    var loginEnd: Color by mutableStateOf(loginEnd)
        private set

    var bottomBar: Color by mutableStateOf(bottomBar)
        private set

    var searchBar: Color by mutableStateOf(searchBar)
        private set

    var tabSelect: Color by mutableStateOf(tabSelect)
        private set

    var tabUnselect: Color by mutableStateOf(tabUnselect)
        private set

    var highlightColor: Color by mutableStateOf(highlightColor)
        private set
}

private val LocalOwlColors = compositionLocalOf {
    LightColorPalette
}

object BilibiliTheme {
    val colors: BilibiliColor
        @Composable
        get() = LocalOwlColors.current
}

@Composable
fun BilibiliTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val targetColors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    val backgroundTop = animateColorAsState(targetColors.backgroundTop, TweenSpec(500))
    val backgroundCenter = animateColorAsState(targetColors.backgroundCenter, TweenSpec(500))
    val backgroundBottom = animateColorAsState(targetColors.backgroundBottom, TweenSpec(500))
    val backgroundEnd = animateColorAsState(targetColors.backgroundEnd, TweenSpec(500))
    val defaultIcon = animateColorAsState(targetColors.defaultIcon, TweenSpec(500))
    val selectIcon = animateColorAsState(targetColors.selectIcon, TweenSpec(500))
    val unselectIcon = animateColorAsState(targetColors.unselectIcon, TweenSpec(500))
    val textTitle = animateColorAsState(targetColors.textTitle, TweenSpec(500))
    val textContent = animateColorAsState(targetColors.textContent, TweenSpec(500))
    val loginStart = animateColorAsState(targetColors.loginStart, TweenSpec(500))
    val loginEnd = animateColorAsState(targetColors.loginEnd, TweenSpec(500))
    val bottomBar = animateColorAsState(targetColors.bottomBar, TweenSpec(500))
    val searchBar = animateColorAsState(targetColors.searchBar, TweenSpec(500))
    val tabSelect = animateColorAsState(targetColors.tabSelect, TweenSpec(500))
    val tabUnselect = animateColorAsState(targetColors.tabUnselect, TweenSpec(500))
    val highlightColor = animateColorAsState(targetColors.highlightColor, TweenSpec(500))

    val colors = BilibiliColor(
        backgroundTop = backgroundTop.value,
        backgroundCenter = backgroundCenter.value,
        backgroundBottom = backgroundBottom.value,
        backgroundEnd = backgroundEnd.value,
        defaultIcon = defaultIcon.value,
        selectIcon = selectIcon.value,
        unselectIcon = unselectIcon.value,
        textTitle = textTitle.value,
        textContent = textContent.value,
        loginStart = loginStart.value,
        loginEnd = loginEnd.value,
        bottomBar = bottomBar.value,
        searchBar = searchBar.value,
        tabSelect = tabSelect.value,
        tabUnselect = tabUnselect.value,
        highlightColor = highlightColor.value
    )

    CompositionLocalProvider(LocalOwlColors provides colors)
    {
        MaterialTheme(
            typography = Typography,
            shapes = Shapes,
            content = content
        )
    }
}