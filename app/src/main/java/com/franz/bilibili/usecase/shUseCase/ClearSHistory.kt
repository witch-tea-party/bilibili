package com.franz.bilibili.usecase.shUseCase

import com.franz.bilibili.room.SHistoryRepository

class ClearSHistory(private val repository: SHistoryRepository) {
    suspend operator fun invoke(){
        repository.deleteAll()
    }
}