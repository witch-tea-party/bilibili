package com.franz.bilibili.usecase.shUseCase

import com.franz.bilibili.bean.SHistoryBean
import com.franz.bilibili.room.SHistoryRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetSHistories(private val repository: SHistoryRepository) {
    operator fun invoke():Flow<List<SHistoryBean>>{
        return repository.getHistories().map { histories->
            histories.sortedByDescending { it.time }
        }
    }
}