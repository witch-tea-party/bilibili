package com.franz.bilibili.usecase.shUseCase

import com.franz.bilibili.bean.SHException
import com.franz.bilibili.bean.SHistoryBean
import com.franz.bilibili.room.SHistoryRepository

class InsertSHistory(private val repository: SHistoryRepository) {
    @Throws(SHException::class)
    suspend operator fun invoke(bean: SHistoryBean){
        if (bean.key.isBlank()){
            throw SHException("搜索内容不能为空")
        }
        repository.insertHistory(bean)
    }
}