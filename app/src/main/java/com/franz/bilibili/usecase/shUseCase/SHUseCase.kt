package com.franz.bilibili.usecase.shUseCase

data class SHUseCase(
    val getSHistories: GetSHistories,
    val insertSHistory: InsertSHistory,
    val clearSHistory: ClearSHistory
)
