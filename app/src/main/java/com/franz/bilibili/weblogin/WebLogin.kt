package com.franz.bilibili.weblogin

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.webkit.*
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.franz.bilibili.APP
import com.franz.bilibili.param.CookieKey
import com.franz.bilibili.tools.SPUtil
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.google.accompanist.insets.statusBarsPadding
import kotlinx.coroutines.launch

@SuppressLint("SetJavaScriptEnabled")
@Composable
fun WebLoginPage(
    modifier: Modifier = Modifier,
    onNavigation:()->Unit,
    onBack:()->Unit
){
    var progress by remember { mutableStateOf(-1) }
    Box(modifier = modifier
        .fillMaxSize()
        .statusBarsPadding()
    ){
        CustomWebView(
            modifier = Modifier.fillMaxSize(),
            url = "https://passport.bilibili.com/login",
            onBack = {
                if (it?.canGoBack() == true){
                    it.goBack()
                }else{
                    onBack()
                }
            },
            onProgressChange = {
                progress = it
            },
            initSettings = {
                it?.apply {
                    javaScriptEnabled = true
                }
            },
            onReceivedError = {
                Log.d("WebLoginPage","error message=${it?.description}")
            },
            onCookie = {
                Log.d("WebLoginPage","cookie=$it")
                SPUtil.putValue(APP.context, CookieKey,it)
                onNavigation()
            }
        )
        LinearProgressIndicator(
            progress = progress * 1.0F / 100F,
            color = BilibiliTheme.colors.highlightColor,
            modifier = Modifier
                .fillMaxWidth()
                .height(if (progress == 100) 0.dp else 5.dp)
        )
    }
}


@Composable
fun CustomWebView(
    modifier: Modifier = Modifier,
    url:String,
    onBack: (webView: WebView?) -> Unit,
    onProgressChange: (progress:Int)->Unit = {},
    initSettings: (webSettings: WebSettings?) -> Unit = {},
    onReceivedError: (error: WebResourceError?) -> Unit = {},
    onCookie:(String)->Unit = {}
){
    val webViewChromeClient = object: WebChromeClient(){
        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            //回调网页内容加载进度
            onProgressChange(newProgress)
            super.onProgressChanged(view, newProgress)
        }
    }
    val webViewClient = object: WebViewClient(){
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            onProgressChange(-1)
        }
        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            onProgressChange(100)
            //监听获取cookie
            val cookie = CookieManager.getInstance().getCookie(url)
            cookie?.let{ onCookie(cookie) }
        }
        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            if(null == request?.url) return false
            val showOverrideUrl = request.url.toString()
            try {
                if (!showOverrideUrl.startsWith("http://")
                    && !showOverrideUrl.startsWith("https://")) {
                    Intent(Intent.ACTION_VIEW, Uri.parse(showOverrideUrl)).apply {
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        view?.context?.applicationContext?.startActivity(this)
                    }
                    return true
                }
            }catch (e:Exception){
                return true
            }
            return super.shouldOverrideUrlLoading(view, request)
        }

        override fun onReceivedError(
            view: WebView?,
            request: WebResourceRequest?,
            error: WebResourceError?
        ) {
            super.onReceivedError(view, request, error)
            onReceivedError(error)
        }
    }
    var webView:WebView? = null
    val coroutineScope = rememberCoroutineScope()
    AndroidView(modifier = modifier,factory = { ctx ->
        WebView(ctx).apply {
            this.webViewClient = webViewClient
            this.webChromeClient = webViewChromeClient
            initSettings(this.settings)
            webView = this
            loadUrl(url)
        }
    })
    BackHandler {
        coroutineScope.launch {
            onBack(webView)
        }
    }
}