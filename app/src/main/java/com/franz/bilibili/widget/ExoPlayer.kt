package com.franz.bilibili.widget

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.media3.common.Player
import androidx.media3.ui.PlayerView

@Composable
fun ExoPlayer(
    modifier: Modifier = Modifier,
    player: Player,
    lifecycle: Lifecycle.Event,
    onCreate:()->Unit,
    onStart:()->Unit,
    onPause:()->Unit,
    onStop:()->Unit,
    onResume:()->Unit,
    onDestroy:()->Unit,
    onAnother:()->Unit
) {
    AndroidView(
        factory = { context ->
            PlayerView(context).also {
                it.player = player
            }
        },
        update = {
            when (lifecycle) {
                Lifecycle.Event.ON_CREATE -> {
                    //创建
                    onCreate()
                }
                Lifecycle.Event.ON_START -> {
                    //开始
                    onStart()
                }
                Lifecycle.Event.ON_PAUSE -> {
                    //暂停
                    onPause()
                }
                Lifecycle.Event.ON_RESUME -> {
                    //恢复
                    onResume()
                }
                Lifecycle.Event.ON_STOP -> {
                    //暂停
                    onStop()
                }
                Lifecycle.Event.ON_DESTROY -> {
                    //销毁
                    onDestroy()
                }
                else -> {
                    onAnother()
                }
            }
        },
        modifier = modifier
    )
}