package com.franz.bilibili.widget

import android.util.Log
import androidx.annotation.DrawableRes
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.*
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties
import androidx.compose.ui.window.SecureFlagPolicy
import com.franz.bilibili.R
import com.franz.bilibili.tools.DateTransform
import com.franz.bilibili.ui.theme.*

@Composable
fun TopAppBar(
    hint: String = "",
    height: Dp,
    modifier: Modifier = Modifier,
    onClick: (String) -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .height(height)
    ) {
        Image(
            painter = painterResource(id = R.drawable.bilibili_logo),
            contentDescription = "Bilibili",
            contentScale = ContentScale.Fit,
            modifier = Modifier.size(height)
        )

        Spacer(modifier = Modifier.width(10.dp))
        SearchBar(
            text = hint,
            modifier = Modifier
                .weight(1f)
                .height(32.dp)
        ) {
            onClick(it)
        }
        Spacer(modifier = Modifier.width(10.dp))

        Icon(
            painter = painterResource(id = R.drawable.icon_qrcode),
            contentDescription = "qrcode",
            tint = BilibiliTheme.colors.defaultIcon,
            modifier = Modifier.size(24.dp)
        )

    }
}

@Composable
fun SearchBar(
    text: String = "野原新之助",
    modifier: Modifier = Modifier,
    onClick: (String) -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .background(color = BilibiliTheme.colors.searchBar, shape = RoundedCornerShape(10.dp))
            .padding(5.dp)
            .clickable { onClick(text) }
    ) {
        Icon(
            imageVector = Icons.Default.Search,
            contentDescription = "Search",
            tint = BilibiliTheme.colors.defaultIcon,
            modifier = Modifier.size(20.dp)
        )
        Spacer(modifier = Modifier.width(5.dp))

        BasicTextField(
            value = text,
            onValueChange = {},
            enabled = false,
            textStyle = MaterialTheme.typography.caption.copy(color = BilibiliTheme.colors.defaultIcon)
        )
    }
}


@Composable
fun ItemRow(
    number: Int,
    @DrawableRes icon: Int,
    color:Color = white,
    modifier: Modifier = Modifier
) {
    val text = calculation(number)
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Icon(
            painterResource(id = icon),
            contentDescription = text,
            tint = color,
            modifier = Modifier.size(16.dp)
        )

        Spacer(modifier = Modifier.width(2.dp))

        Text(
            text = text,
            style = MaterialTheme.typography.overline,
            color = color
        )
    }
}

/**
 * 计算人数(Int)
 * 例如30263则返回3万,可保留一位小数
 * 如果小于10000，则直接返回*/
 fun calculation(number: Int): String {
    return if (number < 9999) {
        "$number"
    } else {
        "${doubleFormat(number = number / 10000.0)}万"
    }
}

/**
 * 格式化Double类型，设置保留小数位，并转化为String返回
 * @param decimal 需要保留的小数位
 * @param number 需要格式化的数字*/
fun doubleFormat(decimal: Int = 1, number: Double): String = "%.${decimal}f".format(number)


/**
 * 视频发布者*/
@Composable
fun videoAuthor(
    author: String,
    modifier: Modifier = Modifier
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .padding(start = 5.dp, end = 5.dp)
    ) {
        Text(
            text = "UP",
            color = BilibiliTheme.colors.defaultIcon,
            style = MaterialTheme.typography.overline.copy(fontSize = 8.sp),
            modifier = Modifier
                .border(
                    width = 1.dp,
                    color = BilibiliTheme.colors.defaultIcon,
                    shape = RoundedCornerShape(4.dp)
                )
                .padding(1.dp)
        )
        Spacer(modifier = Modifier.width(4.dp))
        Text(
            text = author,
            color = BilibiliTheme.colors.defaultIcon,
            style = MaterialTheme.typography.overline,
        )

        Spacer(modifier = Modifier.weight(1f))

        Icon(
            imageVector = Icons.Default.MoreVert,
            contentDescription = "more",
            tint = BilibiliTheme.colors.defaultIcon,
            modifier = Modifier.size(16.dp)
        )
    }
}

@Composable
fun RecommendReason(
    content: String,
    modifier: Modifier = Modifier
){
    if (content.isNotEmpty()){
        Text(
            text = content,
            color = orange100,
            style = MaterialTheme.typography.overline.copy(fontSize = 8.sp),
            modifier = modifier
                .background(color = orange200, shape = RoundedCornerShape(2.dp))
                .padding(2.dp)
        )
    }
}

@Composable
fun CustomTextField(
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions(),
    singleLine: Boolean = false,
    maxLines: Int = Int.MAX_VALUE,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    placeholderText: String = "",
    onTextLayout: (TextLayoutResult) -> Unit = {},
    cursorBrush: Brush = SolidColor(Color.Black),
) {
    BasicTextField(
        modifier = modifier.fillMaxWidth(),
        value = value,
        onValueChange = onValueChange,
        singleLine = singleLine,
        maxLines = maxLines,
        enabled = enabled,
        readOnly = readOnly,
        interactionSource = interactionSource,
        textStyle = textStyle,
        visualTransformation = visualTransformation,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        onTextLayout = onTextLayout,
        cursorBrush = cursorBrush,
        decorationBox = { innerTextField ->
            Row(
                modifier,
                verticalAlignment = Alignment.CenterVertically
            ) {
                if (leadingIcon != null) leadingIcon()
                Box(Modifier.weight(1f)) {
                    if (value.isEmpty())
                        Text(
                        placeholderText,
                        style = textStyle
                    )
                    innerTextField()
                }
                if (trailingIcon != null) trailingIcon()
            }
        }
    )
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun CustomAlertDialog(
    visibility: Boolean,
    title: String,
    content: String,
    confirmText: String,
    cancelText: String,
    onConfirm:()->Unit,
    onCancel:()->Unit,
    onDismiss:()->Unit
){
    if (visibility){
        AlertDialog(
            onDismissRequest = onDismiss,
            title = {
                Text(
                    text = title,
                    color = BilibiliTheme.colors.textTitle,
                    fontWeight = FontWeight.Bold,
                    fontSize = 14.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth()
            ) },
            text = {
                Text(
                    text = content,
                    color = BilibiliTheme.colors.textTitle,
                    fontSize = 12.sp
                )
            },
            shape = RoundedCornerShape(10.dp),
            backgroundColor = BilibiliTheme.colors.backgroundTop,
            confirmButton = {
                TextButton(onClick = onConfirm) {
                    Text(
                        text = confirmText,
                        color = BilibiliTheme.colors.loginEnd,
                        fontWeight = FontWeight.Bold,
                        fontSize = 12.sp
                    )
                }
            },
            dismissButton = {
                TextButton(onClick = onCancel) {
                    Text(
                        text = cancelText,
                        color = BilibiliTheme.colors.textContent,
                        fontWeight = FontWeight.Bold,
                        fontSize = 12.sp
                    )
                }
            },
            properties = DialogProperties(
                //是否可以通过按下后退按钮来关闭对话框。 如果为 true，按下后退按钮将调用 onDismissRequest。
                dismissOnBackPress=true,
                //是否可以通过在对话框边界外单击来关闭对话框。 如果为 true，单击对话框外将调用 onDismissRequest
                dismissOnClickOutside=true,
                //用于在对话框窗口上设置 WindowManager.LayoutParams.FLAG_SECURE 的策略。
                securePolicy= SecureFlagPolicy.Inherit,
                //对话框内容的宽度是否应限制为平台默认值，小于屏幕宽度。
                usePlatformDefaultWidth=true
            )
        )
    }
}

/**
 * 搜索栏富文本显示
 * 搜索内容存在于搜索建议内容之中的高亮显示，不存在则普通·显示*/
@Composable
fun RichText(
    selectColor: Color,
    unselectColor: Color,
    fontSize:TextUnit = TextUnit.Unspecified,
    searchValue: String,
    matchValue: String
){
    val richText = buildAnnotatedString {
        repeat(matchValue.length){
            val index = if (it < searchValue.length) matchValue.indexOf(searchValue[it]) else -1
            if (index == -1){
                withStyle(style = SpanStyle(
                    fontSize = fontSize,
                    color = unselectColor,
                )
                ){
                    append(matchValue[it])
                }
            }else{
                withStyle(style = SpanStyle(
                    fontSize = fontSize,
                    color = selectColor,
                )
                ){
                    append(matchValue[index])
                }
            }
        }
    }
    Text(
        text = richText,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        modifier = Modifier.fillMaxWidth(),
    )
}

/**
 * 排序选项*/
@Composable
 fun BilibiliRadioButton(
    text:String,
    selected:Boolean,
    modifier: Modifier = Modifier,
    onSelect:()->Unit){
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        RadioButton(
            selected = selected,
            colors = RadioButtonDefaults.colors(
                selectedColor = BilibiliTheme.colors.highlightColor,
                unselectedColor = BilibiliTheme.colors.textTitle
            ),
            onClick = onSelect
        )
        Text(text = text, style = MaterialTheme.typography.body1, color = BilibiliTheme.colors.textTitle)
    }
}

/**
 * 视频点赞、收藏、转发等数据*/
@Composable
 fun StatBar(
    like:Int = 0,
    dislike:String = "不喜欢",
    coin:Int = 0,
    favorite:Int = 0,
    share:Int = 0,
    showLike:Boolean = false,
    showDislike:Boolean = false,
    showFavorite:Boolean = false,
    onClickLike:()->Unit,
    onClickDislike:()->Unit,
    onClickFavorite:()->Unit
){

    val likeColor by animateColorAsState(
        targetValue = if (showLike){
            BilibiliTheme.colors.highlightColor
        }else{
            BilibiliTheme.colors.textTitle
        }
    )
    val dislikeColor by animateColorAsState(
        targetValue = if (showDislike){
            BilibiliTheme.colors.highlightColor
        }else{
            BilibiliTheme.colors.textTitle
        }
    )
    val favoriteColor by animateColorAsState(
        targetValue = if (showFavorite){
            BilibiliTheme.colors.highlightColor
        }else{
            BilibiliTheme.colors.textTitle
        }
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(BilibiliTheme.colors.textContent, RoundedCornerShape(10.dp))
            .padding(10.dp),
        verticalArrangement = Arrangement.Center
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Icon(
                painter = painterResource(id = R.drawable.icon_stat_like),
                contentDescription = "agreement",
                tint = likeColor,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
                    .clickable { onClickLike() }
            )
            Icon(
                painter = painterResource(id = R.drawable.icon_stat_dislike),
                contentDescription = "dislike",
                tint = dislikeColor,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
                    .clickable { onClickDislike() }
            )
            Icon(
                painter = painterResource(id = R.drawable.icon_stat_coin),
                contentDescription = "coin",
                tint = BilibiliTheme.colors.textTitle,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
            )
            Icon(
                painter = painterResource(id = R.drawable.icon_stat_favorite),
                contentDescription = "favorite",
                tint = favoriteColor,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
                    .clickable { onClickFavorite() }
            )
            Icon(
                painter = painterResource(id = R.drawable.icon_stat_share),
                contentDescription = "share",
                tint = BilibiliTheme.colors.textTitle,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
            )
        }
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = calculation(like),
                fontSize = 10.sp,
                color = likeColor.copy(alpha = 0.5f),
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = dislike,
                fontSize = 10.sp,
                color = dislikeColor.copy(alpha = 0.5f),
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = calculation(coin),
                fontSize = 10.sp,
                color = BilibiliTheme.colors.textTitle.copy(alpha = 0.5f),
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = calculation(favorite),
                fontSize = 10.sp,
                color = favoriteColor.copy(alpha = 0.5f),
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = calculation(share),
                fontSize = 10.sp,
                color = BilibiliTheme.colors.textTitle.copy(alpha = 0.5f),
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
        }
    }
}

/**
 * 视频播放量、评论等信息*/
@Composable
fun InfoBar(
    grade:String,
    play:Int,
    comment:Int,
    date:Int,
    pos:Int,
    iconColor: Color = BilibiliTheme.colors.highlightColor
){
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(BilibiliTheme.colors.textContent, RoundedCornerShape(10.dp))
            .padding(10.dp),
        verticalArrangement = Arrangement.Center
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Icon(
                painter = painterResource(id = R.drawable.icon_video_grade),
                contentDescription = "grade",
                tint = iconColor,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
            )
            Icon(
                painter = painterResource(id = R.drawable.icon_video_play),
                contentDescription = "view",
                tint = iconColor,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
            )
            Icon(
                painter = painterResource(id = R.drawable.icon_video_replay),
                contentDescription = "replay",
                tint = iconColor,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
            )
            Icon(
                painter = painterResource(id = R.drawable.icon_video_time),
                contentDescription = "date",
                tint = iconColor,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
            )
            Icon(
                painter = painterResource(id = R.drawable.icon_video_pos),
                contentDescription = "pos",
                tint = iconColor,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
            )
        }
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = if (grade.isNotBlank()) grade else "无",
                fontSize = 10.sp,
                color = BilibiliTheme.colors.textTitle.copy(alpha = 0.5f),
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = calculation(play),
                fontSize = 10.sp,
                color = BilibiliTheme.colors.textTitle.copy(alpha = 0.5f),
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = calculation(comment),
                fontSize = 10.sp,
                color = BilibiliTheme.colors.textTitle.copy(alpha = 0.5f),
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = DateTransform.millToDate(date.toLong()*1000),
                fontSize = 10.sp,
                color = BilibiliTheme.colors.textTitle.copy(alpha = 0.5f),
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = if (pos == 0) "无" else "$pos",
                fontSize = 10.sp,
                color = BilibiliTheme.colors.textTitle.copy(alpha = 0.5f),
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
        }
    }
}

