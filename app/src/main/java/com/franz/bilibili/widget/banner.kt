package com.franz.bilibili.widget

import android.util.Log
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Circle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.franz.bilibili.R
import com.franz.bilibili.ui.theme.BilibiliTheme
import com.franz.bilibili.ui.theme.white
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.delay

/**
 * banner*/
@OptIn(ExperimentalPagerApi::class)
@Composable
fun BilibiliBanner(
    bannerHeight: Dp = 200.dp,
    time:Long = 5000L,
    images:List<String>
) {
    val pagerState = rememberPagerState(initialPage = 0)
    /**
     * 自动轮播*/
    LaunchedEffect(pagerState.currentPage) {
        delay(time)
        if (pagerState.currentPage == images.size-1) {
            pagerState.scrollToPage(0)
        }else{
            pagerState.animateScrollToPage(pagerState.currentPage+1)
        }
    }

    Box(modifier = Modifier
        .fillMaxWidth()
        .height(bannerHeight)
    ){
        HorizontalPager(
            count = images.size,
            state = pagerState,
            contentPadding = PaddingValues(horizontal = 0.dp),
            modifier = Modifier
                .fillMaxSize()
                .clip(RoundedCornerShape(10.dp))
        ) { page ->
            repeat(images.size){
                AsyncImage(
                    model = images[pagerState.currentPage],
                    contentDescription = "banner",
                    contentScale = ContentScale.Crop,
                    placeholder = painterResource(id = R.drawable.bilibili_logo),
                    modifier = Modifier
                        .fillMaxSize()
                        .clip(RoundedCornerShape(10.dp))
                )
            }
        }

        /**
         * 指示器*/
        Row(modifier = Modifier
            .fillMaxWidth()
            .align(Alignment.BottomCenter),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            repeat(images.size){ pos->
                val size = remember { mutableStateOf(0.dp) }
                val color = remember { mutableStateOf(Color.Transparent) }
                if (pagerState.currentPage == pos){
                    size.value = 12.dp
                    color.value = BilibiliTheme.colors.selectIcon
                }else{
                    size.value = 8.dp
                    color.value = BilibiliTheme.colors.unselectIcon
                }
                Icon(
                    imageVector = Icons.Default.Circle,
                    contentDescription = "indicator",
                    tint = color.value,
                    modifier = Modifier
                        .size(size.value)
                        .animateContentSize()
                        .padding(end = 4.dp)
                )
            }
        }
    }
}