package com.franz.bilibili.widget

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.franz.bilibili.R
import com.franz.bilibili.ui.theme.BilibiliTheme

/**
 * 全屏加载失败提示*/
@Composable
fun LargeLoadFailed(onRetry:()->Unit){
    Box(
        modifier = Modifier
            .fillMaxSize()
            .clickable { onRetry() }
    ){
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.load_failed),
                contentDescription = "load failed",
                contentScale = ContentScale.Crop
            )
            Text(
                text = "加载失败，请点击重试!",
                color = BilibiliTheme.colors.textTitle,
                style = MaterialTheme.typography.caption
            )
        }
    }
}

/**
 * 底部加载失败提示*/
@Composable
fun SmallLoadFailed(onRetry:()->Unit){
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clickable { onRetry() }
    ) {
        Image(
            painter = painterResource(id = R.drawable.icon_load_failed),
            contentDescription = "load failed",
            modifier = Modifier.size(24.dp)
        )
        Spacer(modifier = Modifier.width(5.dp))

        Text(
            text = "加载失败，请点击重试!",
            color = BilibiliTheme.colors.textTitle,
            style = MaterialTheme.typography.caption
        )
    }
}

/**
 * 顶部圆圈加载*/
@Composable
fun Loading(){
    Box(modifier = Modifier
        .fillMaxWidth()
        .padding(5.dp)
    ){
        CircularProgressIndicator(
            modifier = Modifier
                .align(Alignment.Center)
                .size(24.dp),
            color = BilibiliTheme.colors.loginEnd
        )
    }
}